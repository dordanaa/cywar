<?php

use Illuminate\Http\Request;

Route::post('login', 'AuthController@login')->middleware(['throttle:80,3']);
Route::post('login-api', 'AuthController@apiLogin')->middleware('throttle:80,5');

Route::post('signup', 'AuthController@signup')->middleware('throttle:10,5');
Route::post('forgot-password', 'AuthController@forgotPass')->middleware('throttle:20,5');
Route::post('email-verify', 'AuthController@verifyEmail');
Route::post('getqrcode', 'AuthController@getQRCode');
Route::post('reset-password', 'AuthController@resetPassword')->middleware('throttle:20,5');
Route::get('signup', 'PagesController@signup');


// Route::post('login-jc', 'AuthController@apiLogin')->middleware('throttle:20,5'); // Redirect to login page

Route::get('news', 'PagesController@getNews'); 

// Outer Source Service
Route::get('pods', 'PodsController@getAllIPS');
Route::post('students/{token}', 'APIStudentController@index'); // Students Signup From Campus
Route::post('students/status/{token}', 'APIStudentController@updateStatus'); // Update student status From Campus
Route::get('structure/{token}','GamesController@GetGameStructure')->middleware('throttle:20,5'); //get data from ....
Route::post('users/create-jc', 'CMS\UserController@createJcUser')->middleware('throttle:20,5'); //Api For Jc to create new token


Route::group(['middleware' => 'auth:api'], function(){ // *************************************** LOGGED IN
    
    Route::get('cyberpedia', 'CyberpediaController@getAllTerms'); 
    Route::get('cyberpedia/terms/{term}', 'CyberpediaController@showTerm'); 
    Route::get('download', 'PagesController@storageIst'); 
    Route::post('logout', 'AuthController@logout'); 
    Route::post('hint', 'GamesController@getHint');
    Route::post('container', 'ContainerController@checkContainer');
    

    Route::group(['middleware' => 'jc'], function(){ // If user is from just code checks token, if token expired user will logged out

        Route::get('support', 'SupportController@index');
        Route::post('support', 'SupportController@create')->middleware('throttle:10,3');

        Route::get('hof', 'PagesController@HOF'); // Hall of Fame

        Route::prefix('/user')->group(function () {
            Route::get('', 'UserController@profile');
            Route::get('payment', 'PaypalController@getActivePlans');
            Route::get('edit', 'UserController@edit');
            Route::get('games', 'UserController@games');
            Route::get('details', 'UserController@details');
            Route::get('practice-arena', 'UserController@practiceArena');
            Route::post('', 'UserController@update');
            Route::post('refresh', 'AuthController@refreshToken')->middleware('throttle:5,30');
            // Route::post('payment/{id}', 'PaypalController@createAgreement');
            Route::post('payment', 'OrderController@setOrder');
            Route::post('execute-agreement', 'PaypalController@executeAgreement');
            Route::post('exec-order', 'PaypalController@execOrder');
        });
        
        Route::prefix('/practice')->group(function () {
            Route::get('', 'PracticeController@index');
            Route::get('pulse', 'PracticeController@pulse')->middleware('throttle:30,1');
            Route::get('{id}', 'PracticeController@show')->middleware('payment');
            Route::get('docker/{id}', 'PracticeController@runDocker');
            Route::post('test-task', 'PracticeController@testTask')->middleware('throttle:20,1');
            Route::post('download-file', 'PracticeController@downloadFile')->middleware('throttle:20,1');
            Route::post('reset-game/{id}', 'PracticeController@reset')->middleware('throttle:10,1');
            Route::get('solution/{id}', 'PracticeController@taskSolution');
        });

        Route::prefix('/games')->group(function () {
            Route::get('', 'GamesController@index');
            Route::get('{game}/hint', 'GamesController@hint');
            Route::post('reset-game/{id}', 'GamesController@reset')->middleware('throttle:10,1');
            Route::post('{game}', 'GamesController@submit')->middleware('throttle:50,1');
            Route::get('pulse', 'GamesController@pulse');
            Route::get('{name}', 'GamesController@game')->middleware('payment');
            Route::get('docker/{game}', 'GamesController@runDocker');
        });

    });

    Route::group(['middleware' => 'admin', 'prefix' => 'cms'], function(){ // *************************************** ADMIN
        
        Route::get('autocomplete', 'CMS\GeneralController@autocomplete')->middleware('throttle:100000,1');
        Route::post('uploadJson', 'CMS\PodController@initial');

        Route::prefix('dashboard')->group(function () {

            Route::get('basics', 'CMS\DashboardController@basics');
            Route::get('games', 'CMS\DashboardController@games');
            Route::get('students', 'CMS\DashboardController@students');
            Route::get('populars', 'CMS\DashboardController@populars');
            Route::get('hardest', 'CMS\DashboardController@hardest');

            Route::get('report', 'CMS\DashboardController@generateReport');
            
        });


        Route::prefix('modules')->group(function () {

            Route::post('', 'CMS\ModuleController@create');
            Route::get('show/{id}', 'CMS\ModuleController@show');
            Route::get('edit/{module}', 'CMS\ModuleController@edit');
            Route::post('multiple', 'CMS\ModuleController@mutiple');
            Route::post('update', 'CMS\ModuleController@update');
            Route::post('status', 'CMS\ModuleController@updateStatus');
            Route::post('delete', 'CMS\ModuleController@delete');
            Route::get('lessons/{id}/{pagination}', 'CMS\ModuleController@lessons');
            Route::get('{pagination}', 'CMS\ModuleController@index');

        });

        Route::prefix('lessons')->group(function () {

            Route::get('edit/{lesson}', 'CMS\LessonController@edit');
            Route::get('new/{id}', 'CMS\LessonController@new');
            Route::get('containers/{id}', 'CMS\LessonController@getContainers');
            Route::post('containers', 'CMS\LessonController@closeContainer');
            Route::post('', 'CMS\LessonController@create');
            Route::post('multiple', 'CMS\LessonController@mutiple');
            Route::post('update', 'CMS\LessonController@update');
            Route::post('missions/update', 'CMS\LessonController@updateMissions');
            Route::post('status', 'CMS\LessonController@updateStatus');
            Route::post('delete', 'CMS\LessonController@delete');
            Route::get('{pagination}', 'CMS\LessonController@index');

        });

        Route::prefix('categories')->group(function () {

            Route::post('', 'CMS\CategoryController@create');
            Route::get('show/{id}', 'CMS\CategoryController@show');
            Route::get('edit/{category}', 'CMS\CategoryController@edit');
            Route::post('multiple', 'CMS\CategoryController@mutiple');
            Route::post('update', 'CMS\CategoryController@update');
            Route::post('status', 'CMS\CategoryController@updateStatus');
            Route::post('delete', 'CMS\CategoryController@delete');
            Route::get('{pagination}', 'CMS\CategoryController@index');

        });

        Route::prefix('games')->group(function () {
            
            Route::get('new', 'CMS\GameController@new');
            Route::get('show/{id}', 'CMS\GameController@show');
            Route::get('edit/{id}', 'CMS\GameController@edit');
            Route::post('dockers', 'CMS\GameController@closeDocker');
            Route::post('create', 'CMS\GameController@create');
            Route::post('multiple', 'CMS\GameController@mutiple');
            Route::post('delete', 'CMS\GameController@delete');
            Route::post('status', 'CMS\GameController@updateStatus');
            Route::post('update', 'CMS\GameController@update');
            Route::post('file/delete', 'CMS\GameController@deleteFile');
            Route::post('file', 'CMS\GameController@file');
            Route::post('guide', 'CMS\GameController@guide');
            Route::post('image', 'CMS\GameController@image');
            
        });

        Route::prefix('colleges')->group(function () {

            Route::get('new', 'CMS\CollegeController@new');
            Route::get('show/{college}', 'CMS\CollegeController@show');
            Route::get('{pagination}', 'CMS\CollegeController@index');
            Route::get('classes/{id}/{pagination}', 'CMS\CollegeController@classes');
            Route::post('create', 'CMS\CollegeController@create');
            Route::post('multiple', 'CMS\CollegeController@mutiple');
            Route::post('update', 'CMS\CollegeController@update');
            Route::post('status', 'CMS\CollegeController@updateStatus');
            Route::post('delete', 'CMS\CollegeController@delete');

        });

        Route::prefix('businesses')->group(function () {

            Route::get('new', 'CMS\BusinessController@new');
            Route::get('show/{college}', 'CMS\BusinessController@show');
            Route::get('{pagination}', 'CMS\BusinessController@index');
            Route::get('classes/{id}/{pagination}', 'CMS\BusinessController@classes');
            Route::post('create', 'CMS\BusinessController@create');
            Route::post('multiple', 'CMS\BusinessController@mutiple');
            Route::post('update', 'CMS\BusinessController@update');
            Route::post('status', 'CMS\BusinessController@updateStatus');
            Route::post('delete', 'CMS\BusinessController@delete');

        });

        Route::prefix('countries')->group(function () {

            Route::get('new', 'CMS\CountryController@new');
            Route::get('show/{country}', 'CMS\CountryController@show');
            Route::post('create', 'CMS\CountryController@create');
            Route::post('multiple', 'CMS\CountryController@mutiple');
            Route::post('update', 'CMS\CountryController@update');
            Route::post('status', 'CMS\CountryController@updateStatus');
            Route::post('delete', 'CMS\CountryController@delete');
            Route::get('{pagination}', 'CMS\CountryController@index');
            Route::get('colleges/{id}/{pagination}', 'CMS\CountryController@colleges');

        });

        Route::prefix('tags')->group(function () {

            Route::get('new', 'CMS\TagsController@new');
            Route::get('edit/{tag}', 'CMS\TagsController@edit');
            Route::get('{pagination}', 'CMS\TagsController@index');
            Route::post('create', 'CMS\TagsController@create');
            Route::post('multiple', 'CMS\TagsController@mutiple');
            Route::post('update', 'CMS\TagsController@update');
            Route::post('status', 'CMS\TagsController@updateStatus');
            Route::post('delete', 'CMS\TagsController@delete');

        });

        Route::prefix('levels')->group(function () {

            Route::get('new', 'CMS\LevelController@new');
            Route::get('show/{level}', 'CMS\LevelController@show');
            Route::post('create', 'CMS\LevelController@create');
            Route::post('multiple', 'CMS\LevelController@mutiple');
            Route::post('update', 'CMS\LevelController@update');
            Route::post('status', 'CMS\LevelController@updateStatus');
            Route::post('delete', 'CMS\LevelController@delete');
            Route::get('{pagination}', 'CMS\LevelController@index');

        });

        Route::prefix('avatars')->group(function () {

            Route::get('new', 'CMS\AvatarController@new');
            Route::get('show/{level}', 'CMS\AvatarController@show');
            Route::post('create', 'CMS\AvatarController@create');
            Route::post('multiple', 'CMS\AvatarController@mutiple');
            Route::post('update', 'CMS\AvatarController@update');
            Route::post('status', 'CMS\AvatarController@updateStatus');
            Route::post('delete', 'CMS\AvatarController@delete');
            Route::get('{pagination}', 'CMS\AvatarController@index');

        });

        Route::prefix('tnc')->group(function () {

            Route::get('{pagination}', 'CMS\TncController@index');
            Route::get('show/{tnc}', 'CMS\TncController@show');
            Route::post('create', 'CMS\TncController@create');
            Route::post('update', 'CMS\TncController@update');
            Route::post('delete', 'CMS\TncController@delete');

        });

        Route::prefix('cookies')->group(function () {

            Route::get('{pagination}', 'CMS\CookieController@index');
            Route::get('show/{cookie}', 'CMS\CookieController@show');
            Route::post('create', 'CMS\CookieController@create');
            Route::post('update', 'CMS\CookieController@update');
            Route::post('delete', 'CMS\CookieController@delete');

        });

        Route::prefix('privacy')->group(function () {

            Route::get('{pagination}', 'CMS\PrivacyController@index');
            Route::get('show/{privacy}', 'CMS\PrivacyController@show');
            Route::post('create', 'CMS\PrivacyController@create');
            Route::post('update', 'CMS\PrivacyController@update');
            Route::post('delete', 'CMS\PrivacyController@delete');

        });

        Route::prefix('subscriptions')->group(function () {

            Route::get('{pagination}', 'CMS\SubscriptionController@index');
            Route::get('show/{id}', 'CMS\SubscriptionController@show');
            Route::get('edit/{subscription}', 'CMS\SubscriptionController@edit');
            Route::post('', 'CMS\SubscriptionController@create');
            Route::post('multiple', 'CMS\SubscriptionController@mutiple');
            Route::post('update', 'CMS\SubscriptionController@update');
            Route::post('status', 'CMS\SubscriptionController@updateStatus');
            Route::post('delete', 'CMS\SubscriptionController@delete');

        });

        Route::prefix('orders')->group(function () {

            Route::get('{pagination}', 'CMS\OrderController@index');
            Route::get('show/{id}', 'CMS\OrderController@show');
            Route::get('edit/{subscription}', 'CMS\OrderController@edit');
            Route::post('', 'CMS\OrderController@create');
            Route::post('multiple', 'CMS\OrderController@mutiple');
            Route::post('update', 'CMS\OrderController@update');
            Route::post('status', 'CMS\OrderController@updateStatus');
            Route::post('delete', 'CMS\OrderController@delete');

        });

        Route::prefix('plans')->group(function () {
            
            Route::get('new', 'PaypalController@newPlan');
            Route::get('list-plans', 'PaypalController@getPlans');
            Route::get('show/{plan}', 'PaypalController@showPlan');
            Route::post('create', 'PaypalController@createPlan');
            Route::post('update', 'PaypalController@updatePlan');
            Route::post('delete/{plan}', 'PaypalController@deletePlan');
            Route::post('status', 'PaypalController@changeStatus');
            Route::post('activate/{plan}', 'PaypalController@activatePlan');
            // Route::get('agreement/{agreement}', 'PaypalController@getAgreement');
            // Route::get('transactions/{agreement}', 'PaypalController@getTransc');
            // Route::post('agreement/update/{agreement}', 'PaypalController@updateAgreement');
            // Route::post('agreement/reactive/{agreement}', 'PaypalController@reactiveAgreement');
            // Route::post('agreement/stop/{agreement}', 'PaypalController@stopAgreement');
            // Route::post('execute-agreement', 'PaypalController@executeAgreement');
            // Route::post('create-agreement/{plan}', 'PaypalController@createAgreement');
            
        });

        Route::prefix('notification-types')->group(function () {
            
            Route::get('{pagination}', 'CMS\NotificationTypesController@index');
            Route::get('edit/{type}', 'CMS\NotificationTypesController@edit');
            Route::post('create', 'CMS\NotificationTypesController@create');
            Route::post('update', 'CMS\NotificationTypesController@update');
            Route::post('delete', 'CMS\NotificationTypesController@delete');
            Route::post('multiple', 'CMS\NotificationTypesController@mutiple');
            Route::post('status', 'CMS\NotificationTypesController@status');
            
        });
       
    });

    Route::group(['middleware' => 'editor', 'prefix' => 'cms'], function(){ // *************************************** MASTER AND ABOVE

        Route::prefix('cyberpedia')->group(function () {

            Route::prefix('categories')->group(function () {

                Route::get('show/{id}', 'CMS\CyberpediaCategoryController@show');
                Route::get('edit/{category}', 'CMS\CyberpediaCategoryController@edit');
                Route::post('', 'CMS\CyberpediaCategoryController@create');
                Route::post('multiple', 'CMS\CyberpediaCategoryController@mutiple');
                Route::post('update', 'CMS\CyberpediaCategoryController@update');
                Route::post('status', 'CMS\CyberpediaCategoryController@updateStatus');
                Route::post('delete', 'CMS\CyberpediaCategoryController@delete');
                Route::get('lessons/{id}/{pagination}', 'CMS\CyberpediaCategoryController@lessons');
                Route::get('{pagination}', 'CMS\CyberpediaCategoryController@index');

            });

            Route::prefix('terms')->group(function () {

                Route::get('new', 'CMS\CyberpediaTermController@new');
                Route::get('show/{id}', 'CMS\CyberpediaTermController@show');
                Route::get('edit/{id}', 'CMS\CyberpediaTermController@edit');
                Route::post('', 'CMS\CyberpediaTermController@create');
                Route::post('multiple', 'CMS\CyberpediaTermController@mutiple');
                Route::post('update', 'CMS\CyberpediaTermController@update');
                Route::post('status', 'CMS\CyberpediaTermController@updateStatus');
                Route::post('delete', 'CMS\CyberpediaTermController@delete');
                Route::get('lessons/{id}/{pagination}', 'CMS\CyberpediaTermController@lessons');
                Route::get('{pagination}', 'CMS\CyberpediaTermController@index');
                
            });

        });

    });

    
    Route::group(['middleware' => 'teacher', 'prefix' => 'cms'], function(){ // *************************************** TEACHER AND ABOVE

        Route::get('alldata', 'CMS\CMSController@getData');
        Route::get('full-search/{cat}/{search}', 'CMS\DashboardController@fullSearch');
        Route::get('users/show/{id}', 'CMS\UserController@show');
        Route::post('users/profile', 'CMS\UserController@updateProfile');
        Route::get('users/{id}/dockers', 'CMS\UserController@getDockers');
        Route::post('users/{id}/dockers', 'CMS\UserController@closeDockers');
        Route::get('/games/{status}/{pagination}', 'CMS\GameController@index');

        Route::prefix('students')->group(function () {
            
            Route::get('', 'CMS\StudentController@all');
            Route::get('{id}/classes-history', 'CMS\StudentController@classesHistory');
            Route::get('{id}/dockers', 'CMS\StudentController@getDockers');
            Route::get('{id}/statistics', 'CMS\StudentController@getStatistics');
            Route::get('show/{id}', 'CMS\StudentController@show');
            Route::get('{status}/{pagination}', 'CMS\StudentController@index');
            Route::post('{id}/dockers', 'CMS\StudentController@closeDockers');
            Route::post('filter', 'CMS\StudentController@filter');
            Route::post('multiple', 'CMS\StudentController@mutiple');
            Route::post('delete', 'CMS\StudentController@delete');
            Route::post('status', 'CMS\StudentController@updateStatus');
            Route::post('profile', 'CMS\StudentController@updateProfile');
            Route::post('password', 'CMS\StudentController@updatePassword');
            Route::post('active-mfa', 'CMS\StudentController@updateMFA');
            Route::post('new-qrcode/{id}', 'CMS\StudentController@newQRCode');
            Route::post('lessons', 'CMS\StudentController@updateLessons');
            Route::post('reset-activities', 'CMS\StudentController@resetActivities');
            Route::post('class', 'CMS\StudentController@updateClass');
            Route::post('class/remove', 'CMS\StudentController@removeClass');
            Route::post('game/locked', 'CMS\StudentController@gameLocked');
            
        });
        
        Route::prefix('notifications')->group(function () {
            
            Route::get('new', 'CMS\NotificationController@new');
            Route::get('edit/{type}', 'CMS\NotificationController@edit');
            Route::post('create', 'CMS\NotificationController@create');
            Route::post('update', 'CMS\NotificationController@update');
            Route::post('delete', 'CMS\NotificationController@delete');
            Route::post('multiple', 'CMS\NotificationController@mutiple');
            Route::post('status', 'CMS\NotificationController@status');
            Route::get('{pagination}', 'CMS\NotificationController@index');
            
        });
        
        Route::prefix('classes')->group(function () {
    
            Route::get('new', 'CMS\ClassController@new');
            Route::get('show/{class}', 'CMS\ClassController@show');
            Route::get('{id}/dockers', 'CMS\ClassController@getDockers');
            Route::get('{id}/statistics', 'CMS\ClassController@getStatistics');
            Route::post('{id}/dockers', 'CMS\ClassController@closeDockers');
            Route::post('create', 'CMS\ClassController@create');
            Route::post('multiple', 'CMS\ClassController@mutiple');
            Route::post('update', 'CMS\ClassController@update');
            Route::post('update-passwords/{id}', 'CMS\ClassController@updatePass');
            Route::post('update-mfa/{id}', 'CMS\ClassController@updateMFA');
            Route::post('new-qrcode/{id}', 'CMS\ClassController@newQRCode');
            Route::post('status', 'CMS\ClassController@updateStatus');
            Route::post('delete', 'CMS\ClassController@delete');
            Route::post('lessons', 'CMS\ClassController@lessons');
            Route::post('games', 'CMS\ClassController@games');
            Route::get('{pagination}', 'CMS\ClassController@index');
            Route::get('students/{id}/{pagination}', 'CMS\ClassController@getStudents');
            Route::get('users/{id}/{pagination}', 'CMS\ClassController@getUsers');
    
        });
        
        Route::prefix('lessons')->group(function () {

            Route::get('{pagination}', 'CMS\LessonController@index');

        });

        
        Route::prefix('support')->group(function () {

            Route::get('count', 'CMS\SupportController@count');
            Route::get('{pagination}', 'CMS\SupportController@index');
            Route::post('delete', 'CMS\SupportController@delete');
            Route::post('multiple', 'CMS\SupportController@mutiple');
            Route::post('status', 'CMS\SupportController@status');

        });
        
    });

    Route::group(['middleware' => 'master', 'prefix' => 'cms'], function(){ // *************************************** MASTER AND ABOVE
    
        Route::prefix('users')->group(function () {
            
            Route::get('new', 'CMS\UserController@new');
            Route::get('roles', 'CMS\UserController@roles');
            Route::get('{id}/classes-history', 'CMS\UserController@classesHistory');
            Route::get('{id}/statistics', 'CMS\UserController@getStatistics');
            Route::post('create', 'CMS\UserController@create');
            Route::post('games-allowed', 'CMS\UserController@gamesAllowed');
            Route::post('multiple', 'CMS\UserController@mutiple');
            Route::post('delete', 'CMS\UserController@delete');
            Route::post('new-qrcode/{id}', 'CMS\UserController@newQRCode');
            Route::post('status', 'CMS\UserController@updateStatus');
            Route::post('class', 'CMS\UserController@updateClass');
            Route::post('password', 'CMS\UserController@updatePassword');
            Route::post('active-mfa', 'CMS\UserController@updateMFA');
            Route::get('{status}/{pagination}', 'CMS\UserController@index');
            
        });
       
    });
    
});


