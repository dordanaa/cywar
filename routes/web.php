<?php

// Unite Databases ******************************
// Route::get('/unite-roles', 'PagesController@changeRoles');
// Route::get('/unite-headmasters', 'PagesController@fixMasters');
// Route::get('/unite-students', 'PagesController@uniteStudents');
// Route::get('/show-games-won', 'PagesController@gamesWon');
// Route::get('/deletestudents', 'PagesController@deleteStudents');

// Monthly Report
// Route::get('/checkExpirationOfClasses', 'CMS\GeneralController@checkExpires');
// Route::get('/agreement/{id}', 'PaypalController@updateAgreement');
// Route::get('/agreement/{id}', 'PaypalController@getAgreement');
// Route::post('google2fa/authenticate', 'AuthController@verifyGoogleOTP');
// Route::get('/monthly-report', 'CMS\DashboardController@generateReport');

Route::get('/{any?}', function (){
    return view('home');
  })->where('any', '^(?!api\/)[\/\w\.-]*');
  