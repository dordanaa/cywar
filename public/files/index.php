<?php
        require_once("../../helpers/includes/php_header.php");
        define("GID", 146);
        checkLevel($server, GID);
        
        $scenario = isset($_SESSION["game_start"]["games"]) ? json_decode($_SESSION["game_start"]["games"][GID][2]) : "0";
        //Add files for download
        $files = "RAR20files.zip";
        
        if($_SERVER["REQUEST_METHOD"] == "GET"  && !$robot && isset($_GET["docker"])){
            getDocker("https://GAME-URL", $server);
        }
        
        if($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["hint"])){
            $hint = getHint($server, GID);
            echo $hint;
            die;
        }
        
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !$robot) {
            // Here is the code after submit
            $flag = filter_input(INPUT_POST, "flag");
            if(isset($flag)){ // Check if form as been filled | Example: 
                if($flag == "b9a2cdd3209f734325483bca366b356a892b95f745e7bbb005f4f012df8112df"){ // Check form validation | Example: 
                    // If the form is correct
                    
                    $points = 100;
                    
                    // Calc points for multi inputs
                    // $points = calcGrade($answers);

                    $success = true;
                    $alertMsg = "You finished the game successfully";
                }else{
                    // If the form is Incorrect

                    $success = false;
                    $alertMsg = "You failed the game, please try again";
                }
            }else{
                //If the form is not filled correctly
                $success = false;
                $alertMsg = "You havent filled the form";
            }
        
        checkSession(GID, $points, $server); // API CALL come from include file
            
        // Only if you have multiple inputs ( Calc %)
        // function calcGrade($rightAnswers){
        //    $count = 0;
        //    foreach ($_POST as $value) {
        //        if (in_array($value, $rightAnswers)) {
        //            $count += 1;
        //        }
        //    }
        //    return (100 / count($rightAnswers) * $count);
        //}

        }
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("../../helpers/includes/header.php"); ?>
</head>
<body>
<?php include("../../helpers/includes/alerts.php"); ?>
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-white" href="https://cywar.hackeru.com/start">Home</a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <h1 class="text-white nav-link font-weight-light my-0 py-0"><?=  GAME ?></h1>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item pointer font-weight-bold text-warning" onclick="scenarioStart()" style="cursor:pointer">
                <i class="fas fa-info-circle"></i> <span class="d-none d-md-inline-block">Mission</span>
            </li>
        </ul>
    </div>
</nav>
<div class="container-fluid mx-0 px-0 d-flex">
    <!-- Sidebar -->
    <?php include("../../helpers/includes/side_nav.php"); ?>
    <!-- End Sidebar -->
    <div class="col-12 px-0 d-flex align-items-center main_wrapper">
        <div id="particles-js"></div>
        <div class="col-11 col-md-5 mx-auto mt-5 form_wrapper p-3">
            <h1 class="font-weight-light text-white text-center mb-3">What is my hash?</h1>
            <!-- Form -->
            <form class="col-12 col-md-8 mx-auto" id="form" role="form" method="POST" autocomplete="off">
                <input type="hidden" name="recaptcha_response" id="g-recaptcha-response" class="form-control" placeholder="capcha" />
                <!-- Can duplicate for multiple inputs -->
                <div class="form-group mt-3">
                    <label for="exampleInputPassword1" class="sr-only">What my hash? </label>
                    <input type="IP" class="form-control" name="flag" id="exampleInputPassword1" placeholder="hash" required>
                    </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-outline-warning btn-block">Submit</button>
                </div>
            </form>
            <!-- End Form -->
        </div>
    </div>
</div>

<!-- Modal Rules -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title text-white" id="exampleModalCenterTitle">Instruction Of The Game</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <ul class="list-group">
                <?php if($scenario): ?> <li class="list-group-item text-left text-warning" onclick="scenarioStart()" style="cursor:pointer">Watch Scenario</li> <?php endif; ?> 
                <!-- Add the link to the file -->
                <li class="list-group-item text-left">Stage 1: <a class="text-warning" href="RAR20files.zip" download>Download the file </a></li> 
                <li class="list-group-item text-left">Stage 2: Analyze it</li>
                <li class="list-group-item text-left">Stage 3: Submit the data</li>
            </ul>
        </div>
    </div>
</div>
<?php include("../../helpers/includes/story.php"); ?>
<script>
<?php if(isset($success) && $success): ?>
    setTimeout(function(){
        window.location.href = "https://cywar.hackeru.com/student-start";
    }, 3000);
<?php endif; ?>
    grecaptcha.ready(function() {
        grecaptcha.execute("<?= SITE_KEY ?>", {action: "homepage"}).then(function(token) {
            document.querySelector("#g-recaptcha-response").value = token;});
    });
</script>
<?php include("../../helpers/includes/footer.php"); ?>
</body>
</html>
    