<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145883124-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-145883124-1');
    </script>
    <!-- <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>  -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Orbitron|Francois+One|Staatliches|Anton|Yanone+Kaffeesatz&display=swap" rel="stylesheet">
    <title>HackerU Solutions</title>
    <style>
        html { overflow-y: auto }
        #startVideo {
            position: fixed;
            right: 0;
            bottom: 0;
            min-width: 100%; 
            min-height: 100%;
            z-index: 1;
        }

        body{
            background-color: #000;
        }
    </style>
</head>
<body id="body">

    <div id="app">
        <v-app>
            <app-home />
        </v-app>
    </div>

    <script src="{{ asset('js/app.js') }}" async defer></script>
    <script src="https://www.paypal.com/sdk/js?client-id=ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V&vault=true"></script>
    <!-- <script src="https://www.paypalobjects.com/api/checkout.js"></script> -->
</body>
</html>