<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Amaranth|Baloo+Chettan&display=swap" rel="stylesheet">
    <title>Document</title>

    <style>

    body{
        background-color: #ddd;   
    }

    #wrapper{
        width: 50%;
        background-color: #fff;
        margin: auto;
        padding: 100px 15px 100px 15px;
    }
       
    header, section{
        width: 100%;
        text-align: center;
    }

    *{
        font-family: 'Amaranth', sans-serif;
    }

    div#top{
        color: #fff;
        height: 100px;
        background: linear-gradient(90deg, #6ab8dc, #2d5ab3)
    }
    #top h1{
        padding-top: 30px;
        font-size: 2em;
        letter-spacing: 3px;
    }

    p, a{
        font-size: 1.5em;
    }

    a{
        background-color: #6ab8dc;
        padding: 10px 20px;
        color: #fff !important;
        text-decoration: none;
    }
    a:hover{
        background-color: #2d5ab3;
        color: #fff !important;
    }

    div#center{
        text-align: left;
    }

    table{
        width: 100%;
        text-align: left;
    }
    
    thead{
        overflow-x: scroll;
    }

    @media screen and (max-width: 992px) {
        
        #wrapper{
            width: 100%;
            background-color: #fff;
            margin: auto;
            padding: 100px 15px 100px 15px;
        }

    }
    
    </style>
</head>
<body>
    <div id="wrapper">
        <header>
            <img class="logo" src="https://cywar.hackeru.com/images/general/logo.png" alt="">
        </header>
        <section>
            <div id="top">
                <h1>Cywar - {{$data['date']}} Report</h1>
            </div>
            <div id="center">
                <div class="section">
                    <h2>Total Students Around The World</h2>
                    <table border="1">
                        <thead>
                            <tr>
                                @foreach($data['students'] AS $item)
                                <th>{{$item->country}}</th>
                                @endforeach
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @php $all = 0 @endphp
                                @foreach($data['students'] AS $item)
                                @php $all += $item->students_count @endphp
                                <td>{{$item->students_count}}</td>
                                @endforeach
                                <td>{{$all}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="section">
                    <h2>Challenges</h2>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Games</th>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $all = 0 @endphp
                            @foreach($data['cats'] AS $item)
                            @php $all += $item->games_count @endphp
                            <tr>
                                <td>{{$item->games_count}}</td>
                                <td>{{$item->name}}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>{{$all}}</td>
                                <td>Total</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="section">
                    <h2>Other Information</h2>
                    <p>Total Challenges Added: <u>{{$data['games_added']}}</u></p>
                    <p>Total Challenges Played: <u>{{$data['games_played']}}</u></p>
                    <p>Total Challenges Finished Successfully: <u>{{$data['games_finished']}}</u></p>
                </div>
            </div>
        </section>
        <footer dir="ltr">
            <p>
                Hackeru © {{date('Y')}}
            </p>
        </footer>
    </div>
</body>
</html>
