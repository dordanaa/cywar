<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet">
  <title>Document</title>

  <style>
    *{
      padding: 0;
      margin: 0px;
      color: #fff;
      font-family: 'Quicksand', sans-serif;
      font-weight: 100;
    }

    body {
      background-color: #ddd;
    }

    table{
      width: calc(50% - 100px);
      background: linear-gradient(rgb(6, 1, 17), rgb(28, 4, 37), rgb(7, 3, 20));
      background-color: rgb(28, 4, 37);
      padding: 50px;
      margin: auto;
      direction: ltr;
    }

    .divider {
      height: 1px;
      width: 25%;
      max-width: 75px;
      background-color: #fff;
    }

    h1 {
      padding-top: 30px;
      font-size: 2.5em;
      letter-spacing: 3px;
      margin-bottom: 15px;
    }
    
    a {
      background-color: #fff;
      padding: 10px 40px;
      color: rgb(28, 4, 37) !important;
      border-radius: 25px;
      text-decoration: none;
    }

    a:hover {
      background-color: rgb(73, 59, 78);
      border: 1px solid #fff;
      color: #fff !important;
    }

    @media screen and (max-width: 992px) {

      #table {
        width: 100%;
        background-color: #fff;
        margin: auto;
        padding: 100px 15px 100px 15px;
      }

    }

   </style>
</head>

<body>
  <table align="center">
    <thead>
        <tr>
          <td align="center">
              <img class="logo" src="https://cywar.hackeru.com/images/general/logo-white.png" alt="HackerU Logo">
          </td>
        </tr>
        <tr>
          <td>
            <h1>
              CLASS IS ABOUT TO EXPIRED,
              <br>
              {{$class->name}}
            </h1>
          </td>
        </tr>
      </thead>
      <tbody>
          <tr>
              <td>
                  <p>
                    The class is about to be expired in <strong><u>7 days</u></strong>.
                    <br>
                    Please check if the expiration date of the class is the correct.
                    <br>
                      If not, please update it as soon as you can.
                      <br>
                      Thank You!
                  </p>
              </td>
          </tr>
          <tr height="50">
          </tr>
          <tr>
            <td align="center">
                <a class="button"
                target="_blank"
                href="https://cywar.hackeru.com/CMS/settings/classes/{{$class->id}}">UPDATE CLASS</a>
            </td>
          </tr>
          <tr height="30">
          </tr>
          <tr>
            <td align="center">
                <p>
                  Hackeru © 2019
                </p>
            </td>
          </tr>
      </tbody>
  </table>
</body>

</html>