class Pagination {

    pages(data) // Set the pages
    {
        const pages = [];
        for(let i = 1; i <= data.last_page; i++)
        {
            if(data.last_page > 10){
                if(i < 10) pages.push(i)
                if(i == data.last_page) pages.push(i)
            }else{
                pages.push(i)
            }
        }
        return pages;
    }

    changed(page, max) // Return Pages After Change
    {   
        const pages = [1];
        if(page > 5){
            pages.push(page - 3);
            pages.push(page - 2);
            pages.push(page - 1);
            for(let i = 1; i <= max; i++){
                if(i != 1 && i >= page && i <= (page + 5) && ((page + i) >= (page + 5))){
                    pages.push(i)
                }else if(i == max){
                    pages.push(i)
                }
            }
            for(let i = 1; i < 10 - pages.length; i++){
                if(pages.length < 10){
                    pages.splice(i, 0, max - pages.length + 1)
                }
            }
        }else{
            if(max >= 10){
                pages.push(2)
                pages.push(3)
                pages.push(4)
                pages.push(5)
                pages.push(6)
                pages.push(7)
                pages.push(8)
                pages.push(9)
                pages.push(max)
            }else{
                for(let i = 2; i <= max; i++){
                    pages.push(i);
                }
            }
        }
        return pages;
    }

}

export default Pagination = new Pagination()
