import * as jwt_decode from "jwt-decode";

class Auth {

    s(token, refresh) { // set the data
        let {
            exp,
            sub
        } = jwt_decode(token.token);

        function addMinutes(date, minutes) {
            return new Date(date.getTime() + minutes * 60000);
        }
        let exptime = addMinutes(new Date, token.time); 
        
        const cred = [token.token, sub, token.name, token.role, exptime.valueOf()]
        let data = this.e(cred);

        // if(this.getCookie('pytoken')) this.logout();
        // document.cookie = `pytoken=${data};expires=${exptime}`;
        localStorage.setItem('pytoken', data)

        let game = token.url ? token.url : '';
        if(refresh)
        {
            if(game) window.location.href = `/challenges/${game}`;
            else window.location.href = '/';
        }
    }

    g() { // get the data
        const data = this.d();
        if (data.exp > Date.now()) return data;
        else this.logout(); // Token has been expired
    }

    logged() { // check if the user is logged
        return this.g() ? true : false;
    }

    logout() { // logout, delete token
        localStorage.removeItem('pytoken');
        // document.cookie = "pytoken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }

    id() { // get the user id
        if(this.g()) return this.g().subject;
    }

    t() { // get the user token
        const data = this.d();
        return data.token;
    }

    name() { // get the user name
        if(this.g()) return this.g().name;
    }

    admin() { // check if the user is an admin
        if(this.g()) return this.g().role == 'admin' ? true : false;
    }

    student() { // check if the user is an student
        if(this.g()) return this.g().role == 'student' ? true : false;
    }

    candidate() { // check if the user is an candidate
        if(this.g()) return this.g().role == 'candidate' ? true : false;
    }

    teacher() { // check if the user is an teacher
        if(this.g()) return this.g().role == 'teacher' ? true : false;
    }

    master() { // check if the user is an master
        if(this.g()) return this.g().role == 'master' ? true : false;
    }

    guest() { // check if the user is an guest
        if(this.g()) return this.g().role == 'guest' ? true : false;
    }

    role() { // check if the user role
        if(this.g()) return this.g().role;
        else return 'guest';
    }

    user() { // check if the user is just a user
        if(this.g()) return this.g().role == 'user' ? true : false;
    }

    exp() { // check if expired
        const data = this.d();
        return data.exp;
    }

    e(cred) { // encode
        let baseData = btoa(unescape(encodeURIComponent(cred.toString())));
        return baseData.slice(0, 4) + 'E' + baseData.slice(4);
    }

    getCookie(cname) { // get the cookie
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(";");
        for (let i = 0; i < ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == " ") {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    d() { //decode tok
        // const cred = this.getCookie('pytoken');
        const cred = localStorage.getItem('pytoken');
        
        let deco = [];
        if (cred) deco = decodeURIComponent(escape(atob(cred.slice(0, 4) + cred.slice(5)))).split(',');
        return {
            token: deco[0] || null,
            subject: deco[1] || null,
            name: deco[2] || null,
            role: deco[3] || null,
            exp: deco[4] || null
        };
    };

}

export default Auth = new Auth()
