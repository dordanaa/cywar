class Loading {

    on() { // Turn on the loading
        return EventBus.$emit('loading', true)
    }

    off() { // Turn off the loading
        return EventBus.$emit('loading', false)
    }

}

export default Loading = new Loading()
