class Alert {

    on(data){
        EventBus.$emit('alert', data);
    }

    onApp(data){
        EventBus.$emit('alertApp', data);
    }

}

export default Alert = new Alert()
