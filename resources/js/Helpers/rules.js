class Rules {

    check(type, value){
        let res = [];

        for(let i = 0; i < type.length; i++){
            
            if(type[i] == 'name') res.name = this.name(value[i]);
            if(type[i] == 'phone') res.phone = this.phone(value[i]);
            if(type[i] == 'class') res.class = this.class(value[i]);
            if(type[i] == 'email') res.email = this.email(value[i]);
            if(type[i] == 'email_verification') res.email_verification = this.email_verification(value[i], value[i - 1]);
            if(type[i] == 'password') res.password = this.password(value[i]);
            if(type[i] == 'password_confirmation') res.password_confirmation = this.password_confirmation(value[i], value[i - 1]);
            if(type[i] == 'image') res.image = this.image(value[i]);
            if(type[i] == 'category') res.category = this.category(value[i]);
            if(type[i] == 'content') res.content = this.content(value[i]);
            
        }

        

        if(!res.email) delete res.email;
        if(!res.email_verification) delete res.email_verification;
        if(!res.name) delete res.name;
        if(!res.class) delete res.class;
        if(!res.password) delete res.password;
        if(!res.password_confirmation) delete res.password_confirmation;
        if(!res.phone) delete res.phone;
        if(!res.image) delete res.image;
        if(!res.category) delete res.category;
        if(!res.content) delete res.content;

        return res;
    }

    category(v){
        if(v == null || v == ''){
            return 'Category is required'
        }else if(v && typeof v != 'number'){
            return 'Category must be a number'
        }
    }

    content(v){
        if(v == null || v == ''){
            return 'Content is required'
        }
    }

    class(v){
        if(v && v.length < 1 || v == null || v == ''){
            return 'Promo Code is required'
        }
    }

    phone(v){
        if(v && v.length < 1 || v == null || v == ''){
            return 'Phone is required'
        }
    }

    image(v){
        if(v == null || v == ''){
            return 'Image is Required'
        }
    }

    name(v){
        if(v == null || v == ''){
            return 'Name is Required'
        }else if(v && v.length < 2){
            return 'Name must be at least 2 characters'
        }
    }

    email(v){
        let str = /.+@.+\..+/;
        if(v == null || v == ''){
            return 'Email is Required'
        }else if(! str.test(v)){
            return 'Please Enter a valid email'
        }
    }

    email_verification(v, email){
        if(v == null || v == ''){
            return 'Email Verification is Required'
        }else if(v && email && email != v){
            return 'Emails does not match'
        }
    }

    password(v){
        if(v == null || v == ''){
            return 'Password is Required'
        }else if(v && v.length < 8){
            return 'Password must be at least 9 chars & contain at least one lowercase, one uppercase and one numeric'
        }
    }

    password_confirmation(v, pass){
        if(v == null || v == ''){
            return 'Password Confirmation is Required'
        }else if(v && pass && pass != v){
            return 'Passwords does not match'
        }
    }

}

export default Rules = new Rules()
