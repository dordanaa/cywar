import Auth from "./auth";

class Guard {

    admin(to, from, next) {// Admin Alone
        Auth.g() && Auth.g().role == 'admin' ? next() : next('/')
    }
    
    candidate(to, from, next) { // Candidate Alone
        Auth.g() && Auth.g().role == 'candidate' ? next() : next('/')
    }
    
    master(to, from, next) { // Head Master And Admin
        Auth.g() && Auth.g().role == 'master' || Auth.g().role == 'admin' ? next() : next('/')
    }
    
    teacher(to, from, next) { // Teacher, Head Master And Admin
        Auth.g() && Auth.g().role == 'teacher' || Auth.g().role == 'master' || Auth.g().role == 'admin' ? next() : next('/')
    }
    
    editor(to, from, next) { // Editor And Admin
        Auth.g() && Auth.g().role == 'editor' || Auth.g().role == 'admin' ? next() : next('/')
    }
    
    worker(to, from, next) { // Authroize to access CMS
        Auth.g() &  Auth.g().role == 'developer' || Auth.g().role == 'editor' || Auth.g().role == 'teacher' || Auth.g().role == 'master' || Auth.g().role == 'admin' ? next() : next('/')
    }
    
    developer(to, from, next) { // Developer And Admin
        Auth.g() && Auth.g().role == 'developer' || Auth.g().role == 'admin' ? next() : next('/')
    }

    user(to, from, next) { // All Users
        Auth.logged() ? next() : next('/login')
    }

    guest(to, from, next) { // Not Logged
        !Auth.logged() ? next() : next('/')
    }

}

export default Guard = new Guard()
