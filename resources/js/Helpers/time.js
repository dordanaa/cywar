class Time {

    set(date){
        date = new Date(date);        
        let minutes = new Date().getTimezoneOffset() * -1;
        let time = new Date(date.getTime() + minutes * 60000);
        return `${time.getDate()}/${time.getMonth() + 1}/${time.getFullYear()}  ${time.getHours()}:${time.getMinutes()}:${time.getSeconds()}`;
    }

}

export default Time = new Time()
