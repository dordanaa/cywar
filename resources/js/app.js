require("./bootstrap");

window.Vue = require("vue");
window.$ = require("jquery");

import Vue from "vue";
import Vuetify from "vuetify";

import "@mdi/font/css/materialdesignicons.css";

Vue.use(Vuetify);

import VueParticles from "vue-particles";
Vue.use(VueParticles);

import Auth from "./Helpers/auth";
window.Auth = Auth; // Token and User

import Time from "./Helpers/time";
window.Time = Time; // GMT Time

import Pagination from "./Helpers/pagination";
window.Pagination = Pagination; // Token and User

import Loading from "./Helpers/loading";
window.Load = Loading; // Token and User

import Alert from "./Helpers/alert";
window.Alert = Alert; // Token and User

import Rules from "./Helpers/rules";
window.R = Rules; // Rules

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper, /* { default global options } */)

window.URL = window.location.origin; // website url
window.IMG = window.location.origin + "/images/"; // images folder url

window.EventBus = new Vue();

window.addEventListener("unhandledrejection", function(event) {
    if (
        event.reason.response.status == 401 &&
        event.reason.response.data.message == "Unauthenticated."
    ) {
        Auth.logout();
        window.location.reload();
    }
});

if (Auth.g()) axios.defaults.headers.common["Authorization"] = `Bearer ${Auth.t()}`; // pass token for every request

Vue.component("AppHome", require("./components/AppHome.vue").default);

import router from "./Router/router.js";

const app = new Vue({
    el: "#app",
    router,
    rtl: true,
    vuetify: new Vuetify()
});
