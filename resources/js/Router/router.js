import Vue from "vue";
import VueRouter from "vue-router";
import Guard from "../Helpers/guards";


Vue.use(VueRouter);

// User
import Login from "../components/User/login";
import LoginCampus from "../components/User/login_campus";
import LoginJC from "../components/User/login_jc";
import Signup from "../components/User/signup";
import Logout from "../components/User/logout";
import Payment from "../components/User/user/payment";
import Agreement from "../components/User/user/agreement";
import Waiting from "../components/Pages/waiting";
import EmailVerify from "../components/Pages/email_verify";
import NewQR from "../components/Pages/newqr";
import User from "../components/User/user";
import UserProfile from "../components/User/user/profile";
import UserEdit from "../components/User/user/edit";
import UserGamesHistory from "../components/User/user/games_history";
import UserArenaHistory from "../components/User/user/arena_history";
import UserSettings from "../components/User/user/settings";
import ChangePass from "../components/User/change_pass";
import ForgotPass from "../components/User/forgot_pass";
import CMSSearch from "../components/CMS/General/search";

// Pages
import Home from "../components/Pages/home";
import About from "../components/Pages/about";
import News from "../components/Pages/news";
import HallOfFame from "../components/Pages/hof";
import Cyberpedia from "../components/Pages/cyberpedia";
import Suppport from "../components/Pages/support";
import Pricing from "../components/Pages/pricing";
import Policies from "../components/Pages/policies";

// Games
import Challenges from "../components/Games/main";
import ChallengesGames from "../components/Games/games";
import ChallengesCats from "../components/Games/cats";
import Game from "../components/Games/game";

import GameV2 from "../components/Games1/game";

// Practice
import Practice from "../components/Practice/main";
import PracticeModules from "../components/Practice/modules";
import PracticeCats from "../components/Practice/cats";
import PracticeLessons from "../components/Practice/lessons";
import Lesson from "../components/Practice/lesson";

// Cyber Range
import CyberRange from "../components/Pages/cyber_range";

// CMS
import Dashboard from "../components/CMS/dashboard";
// Students
import StudentActive from "../components/CMS/Students/active";
import StudentInactive from "../components/CMS/Students/inactive";
import StudentWaiting from "../components/CMS/Students/waiting";
import StudentsAll from "../components/CMS/Students/all";
import StudentShow from "../components/CMS/Students/show";
// Practice Arena
import CMSModule from "../components/CMS/Practice/main";
import CMSLesson from "../components/CMS/Practice/lessons/main";
import CMSModuleNew from "../components/CMS/Practice/new";
import CMSModuleEdit from "../components/CMS/Practice/edit";
import CMSModuleLessonEdit from "../components/CMS/Practice/lessons/edit";
import CMSModuleLessonNew from "../components/CMS/Practice/lessons/new";
// Cyberpedia
import CyberpediaCategories
  from "../components/Pages/Cyberpedia_parts/categories";
import CyberpediaTerms from "../components/Pages/Cyberpedia_parts/terms";
import CyberpediaTerm from "../components/Pages/Cyberpedia_parts/term";
import CMSCyberpediaCategories
  from "../components/CMS/Cyberpedia/Categories/main";
import CMSCyberpediaCategoriesNew
  from "../components/CMS/Cyberpedia/Categories/new";
import CMSCyberpediaCategoriesEdit
  from "../components/CMS/Cyberpedia/Categories/edit";
import CMSTerms from "../components/CMS/Cyberpedia/Terms/main";
import CMSTermsNew from "../components/CMS/Cyberpedia/Terms/new";
import CMSTermsEdit from "../components/CMS/Cyberpedia/Terms/edit";
// Categories
import Categories from "../components/CMS/Categories/main";
import CategoriesNew from "../components/CMS/Categories/new";
import CategoriesEdit from "../components/CMS/Categories/edit";
// Games
import GamesActive from "../components/CMS/Games/active";
import GamesInactive from "../components/CMS/Games/inactive";
import GamesNew from "../components/CMS/Games/new";
import GamesShow from "../components/CMS/Games/show";
import GamesEdit from "../components/CMS/Games/edit";
// Users
import UsersActive from "../components/CMS/Users/active";
import UsersInactive from "../components/CMS/Users/inactive";
import UsersNew from "../components/CMS/Users/new";
import UsersShow from "../components/CMS/Users/show";
// Subscriptions
import Subscriptions from "../components/CMS/Subscriptions/main";
import SubscriptionsNew from "../components/CMS/Subscriptions/new";
import SubscriptionsShow from "../components/CMS/Subscriptions/show";
// Plans
import Plans from "../components/CMS/Plans/main";
import PlansShow from "../components/CMS/Plans/show";
import PlansNew from "../components/CMS/Plans/new";
// Orders
import Orders from "../components/CMS/Orders/main";
import OrdersShow from "../components/CMS/Orders/show";
// ****** Settings
//Avatars
import Avatars from "../components/CMS/Settings/Avatars/main";
//Tags
import Tags from "../components/CMS/Settings/Tags/main";
import TagsNew from "../components/CMS/Settings/Tags/new";
import TagsEdit from "../components/CMS/Settings/Tags/edit";
//Levels
import Levels from "../components/CMS/Settings/Levels/main";
import LevelsNew from "../components/CMS/Settings/Levels/new";
import LevelsShow from "../components/CMS/Settings/Levels/show";
//Pod Descriptor
import PodDescriptor from "../components/CMS/Settings/Pod-descriptor/main";
// ****** Institutions
//Classes
import Classes from "../components/CMS/Institutions/Classes/main";
import ClassesShow from "../components/CMS/Institutions/Classes/show";
import ClassesNew from "../components/CMS/Institutions/Classes/new";
//Colleges
import Colleges from "../components/CMS/Institutions/Colleges/main";
import CollegesNew from "../components/CMS/Institutions/Colleges/new";
import CollegeShow from "../components/CMS/Institutions/Colleges/show";
//Businesses
import Businesses from "../components/CMS/Institutions/Businesses/main";
import BusinessesNew from "../components/CMS/Institutions/Businesses/new";
import BusinessesShow from "../components/CMS/Institutions/Businesses/show";
//Countries
import Countries from "../components/CMS/Institutions/Countries/main";
import CountryNew from "../components/CMS/Institutions/Countries/new";
import CountryShow from "../components/CMS/Institutions/Countries/show";
// Policies
// Tnc
import TNC from "../components/CMS/Policies/Tnc/main";
import TNCNew from "../components/CMS/Policies/Tnc/new";
import TNCShow from "../components/CMS/Policies/Tnc/show";
// Privacy
import Privacy from "../components/CMS/Policies/Privacy/main";
import PrivacyNew from "../components/CMS/Policies/Privacy/new";
import PrivacyShow from "../components/CMS/Policies/Privacy/show";
// Cookies
import Cookies from "../components/CMS/Policies/Cookies/main";
import CookiesNew from "../components/CMS/Policies/Cookies/new";
import CookiesShow from "../components/CMS/Policies/Cookies/show";
// Supports
import Support from "../components/CMS/Support/main";
// Notifications
import Notifications from "../components/CMS/Notifications/main";
import NotificationNew from "../components/CMS/Notifications/new";
import NotificationTypes from "../components/CMS/Notifications/Types/main";
import NotificationTypeNew from "../components/CMS/Notifications/Types/new";
import NotificationTypeEdit from "../components/CMS/Notifications/Types/edit";


// Errors
import NotFound from "../components/Errors/notfound";


const routes = [
  {
    path: "/",
    component: Home
  },
  {
    path: "/about",
    component: About
  },
  {
    path: "/news",
    component: News,
    beforeEnter: Guard.user
  },
  {
    path: "/login",
    component: Login,
    beforeEnter: Guard.guest
  },
  {
    path: "/login-campus",
    component: LoginCampus
    // beforeEnter: Guard.guest
  },
  {
    path: "/login-jc",
    component: LoginJC,
    beforeEnter: Guard.guest
  },
  {
    path: "/waiting",
    component: Waiting,
    beforeEnter: Guard.guest
  },
  {
    path: "/verify-email",
    component: EmailVerify,
    beforeEnter: Guard.guest
  },
  {
    path: "/newqr",
    component: NewQR,
    beforeEnter: Guard.guest
  },
  {
    path: "/signup",
    component: Signup,
    beforeEnter: Guard.guest
  },
  {
    path: "/logout",
    component: Logout,
    beforeEnter: Guard.user
  },
  {
    path: "/reset-password",
    component: ChangePass,
    beforeEnter: Guard.guest
  },
  {
    path: "/cyber-range",
    component: CyberRange,
    beforeEnter: Guard.user
  },
  {
    path: "/payment",
    component: Payment,
    beforeEnter: Guard.user
  },
  {
    path: "/agreement",
    component: Agreement,
    beforeEnter: Guard.user
  },
  {
    path: "/profile",
    component: User,
    beforeEnter: Guard.user,
    children: [
      {
        path: "",
        component: UserProfile
      },
      {
        path: "edit",
        component: UserEdit
      },
      {
        path: "challenges",
        component: UserGamesHistory
      },
      {
        path: "practice-arena",
        component: UserArenaHistory
      },
      {
        path: "settings",
        component: UserSettings
      }
    ]
  },
  {
    path: "/support",
    component: Suppport,
    beforeEnter: Guard.user
  },
  {
    path: "/pricing",
    component: Pricing,
    beforeEnter: Guard.user
  },
  {
    path: "/policies",
    component: Policies
  },
  {
    path: "/forgot-password",
    component: ForgotPass,
    beforeEnter: Guard.guest
  },
  { // ***************************** GAMES
    path: "/challenges",
    component: Challenges,
    beforeEnter: Guard.user,
    children: [
      {
        path: "",
        component: ChallengesCats
      },
      {
        path: ":cat",
        component: ChallengesGames
      }
    ]
  },
  {
    path: "/challenges/:cat/:game",
    component: Game,
    beforeEnter: Guard.user
  },
  // {
  //   path: "/challenges/:cat/:game/v2",
  //   component: GameV2,
  //   beforeEnter: Guard.user
  // },
  { // ***************************** PRACTICE ARENA
    path: "/practice-arena",
    component: Practice,
    beforeEnter: Guard.user
  },
  {
    path: "/practice-arena/:type",
    component: PracticeModules,
    beforeEnter: Guard.user,
    children: [
      {
        path: "",
        component: PracticeCats
      },
      {
        path: ":cat",
        component: PracticeLessons
      }
    ]
  },
  {
    path: "/practice-arena/:type/:module/:lesson",
    component: Lesson,
    beforeEnter: Guard.user
  },
  {
    path: "/hall-of-fame",
    component: HallOfFame,
    beforeEnter: Guard.user
  },
  {
    path: "/cyberpedia",
    component: Cyberpedia,
    beforeEnter: Guard.user,
    children: [
      {
        path: "",
        component: CyberpediaCategories
      },
      {
        path: ":cat",
        component: CyberpediaTerms
      },
      {
        path: ":cat/:term",
        component: CyberpediaTerm
      }
    ]
  },
  {// ***************************** CMS DASHBOARD
    path: "/CMS",
    component: Dashboard,
    beforeEnter: Guard.worker
  },
  { // ***************************** CMS STUDENTS ROUTES
    path: "/CMS/students/all",
    component: StudentsAll,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/students/active",
    component: StudentActive,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/students/inactive",
    component: StudentInactive,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/students/waiting",
    component: StudentWaiting,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/students/:slug",
    component: StudentShow,
    beforeEnter: Guard.teacher
  },
  { // ***************************** CMS PRACTICE ARENA
    path: "/CMS/modules",
    component: CMSModule,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/modules/new",
    component: CMSModuleNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/modules/edit/:slug",
    component: CMSModuleEdit,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/lessons",
    component: CMSLesson,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/lessons/new/:slug",
    component: CMSModuleLessonNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/lessons/:slug",
    component: CMSModuleLessonEdit,
    beforeEnter: Guard.admin
  },
  { // ***************************** CMS CYBERPEDIA
    path: "/CMS/cyberpedia/categories",
    component: CMSCyberpediaCategories,
    beforeEnter: Guard.editor
  },
  {
    path: "/CMS/cyberpedia/categories/new",
    component: CMSCyberpediaCategoriesNew,
    beforeEnter: Guard.editor
  },
  {
    path: "/CMS/cyberpedia/categories/edit/:slug",
    component: CMSCyberpediaCategoriesEdit,
    beforeEnter: Guard.editor
  },
  {
    path: "/CMS/cyberpedia/terms",
    component: CMSTerms,
    beforeEnter: Guard.editor
  },
  {
    path: "/CMS/cyberpedia/terms/new",
    component: CMSTermsNew,
    beforeEnter: Guard.editor
  },
  {
    path: "/CMS/cyberpedia/terms/edit/:slug",
    component: CMSTermsEdit,
    beforeEnter: Guard.editor
  },
  { // ***************************** CMS CATEGORIES ROUTES
    path: "/CMS/categories",
    component: Categories,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/categories/new",
    component: CategoriesNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/categories/edit/:slug",
    component: CategoriesEdit,
    beforeEnter: Guard.admin
  },
  { // *****************************  CMS GAMES ROUTES
    path: "/CMS/games/active",
    component: GamesActive,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/games/inactive",
    component: GamesInactive,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/games/new",
    component: GamesNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/games/edit/:slug",
    component: GamesEdit,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/games/:slug",
    component: GamesShow,
    beforeEnter: Guard.admin
  },
  { // *****************************  CMS USERS ROUTES
    path: "/CMS/users/active",
    component: UsersActive,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/users/inactive",
    component: UsersInactive,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/users/new",
    component: UsersNew,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/users/:slug",
    component: UsersShow,
    beforeEnter: Guard.teacher
  },
  { // *****************************  CMS SUBSCRIPTIONS
    path: "/CMS/subscriptions",
    component: Subscriptions,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/subscriptions/new",
    component: SubscriptionsNew,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/subscriptions/:id",
    component: SubscriptionsShow,
    beforeEnter: Guard.master
  },
  { // *****************************  CMS Plans
    path: "/CMS/plans",
    component: Plans,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/plans/new",
    component: PlansNew,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/plans/:id",
    component: PlansShow,
    beforeEnter: Guard.master
  },
  { // *****************************  CMS ORDERS
    path: "/CMS/orders",
    component: Orders,
    beforeEnter: Guard.master
  },
  {
    path: "/CMS/orders/:id",
    component: OrdersShow,
    beforeEnter: Guard.master
  },
  // *****************************   CMS SETTINGS
  { // ************* AVATARS
    path: "/CMS/settings/avatars",
    component: Avatars,
    beforeEnter: Guard.admin
  },
  { // ************* TAGS
    path: "/CMS/settings/tags",
    component: Tags,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/settings/tags/new",
    component: TagsNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/settings/tags/edit/:slug",
    component: TagsEdit,
    beforeEnter: Guard.admin
  },
  { // ************* LEVELS
    path: "/CMS/settings/levels",
    component: Levels,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/settings/levels/new",
    component: LevelsNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/settings/levels/:slug",
    component: LevelsShow,
    beforeEnter: Guard.admin
  },
  { // ************* Pod-Descriptor

    path: "/CMS/settings/pod-descriptor",
    component: PodDescriptor,
    beforeEnter: Guard.admin
  },
  // *****************************   CMS INSTITUTIONS
  { // ************* CLASSES
    path: "/CMS/institutions/classes/new",
    component: ClassesNew,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/institutions/classes",
    component: Classes,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/institutions/classes/:slug",
    component: ClassesShow,
    beforeEnter: Guard.teacher
  },
  { // ************* COLLEGES
    path: "/CMS/institutions/colleges",
    component: Colleges,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/colleges/new",
    component: CollegesNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/colleges/:slug",
    component: CollegeShow,
    beforeEnter: Guard.admin
  },
  { // ************* BUSINESSES
    path: "/CMS/institutions/businesses",
    component: Businesses,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/businesses/new",
    component: BusinessesNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/businesses/:slug",
    component: BusinessesShow,
    beforeEnter: Guard.admin
  },
  { // ************* COUNTRIES
    path: "/CMS/institutions/countries/new",
    component: CountryNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/countries/:slug",
    component: CountryShow,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/institutions/countries",
    component: Countries,
    beforeEnter: Guard.admin
  },
  // *****************************   CMS POLICIES
  { // ************* TNC
    path: "/CMS/policies/tnc",
    component: TNC,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/tnc/new",
    component: TNCNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/tnc/:slug",
    component: TNCShow,
    beforeEnter: Guard.admin
  },
  { // ************* PRIVACY POLICY
    path: "/CMS/policies/privacy",
    component: Privacy,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/privacy/new",
    component: PrivacyNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/privacy/:slug",
    component: PrivacyShow,
    beforeEnter: Guard.admin
  },
  { // ************* COOKIES
    path: "/CMS/policies/cookies",
    component: Cookies,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/cookies/new",
    component: CookiesNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/policies/cookies/:slug",
    component: CookiesShow,
    beforeEnter: Guard.admin
  },
  { // ************* Notifications
    path: "/CMS/notifications",
    component: Notifications,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/notifications/new",
    component: NotificationNew,
    beforeEnter: Guard.teacher
  },
  {
    path: "/CMS/notification-types",
    component: NotificationTypes,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/notification-types/new",
    component: NotificationTypeNew,
    beforeEnter: Guard.admin
  },
  {
    path: "/CMS/notification-types/edit/:id",
    component: NotificationTypeEdit,
    beforeEnter: Guard.admin
  },
  { // ************* SUPPORT
    path: "/CMS/supports",
    component: Support,
    beforeEnter: Guard.teacher
  },
  { // ************* CMS FULL SEARCH
    path: "/CMS/search",
    component: CMSSearch,
    beforeEnter: Guard.teacher
  },

  {// ***************************** 404
    path: "*",
    component: NotFound
  }
];

const router = new VueRouter({
  routes, // short for `routes: routes`
  hasbang: false,
  mode: "history"
});

router.beforeEach((to, from, next) => {
  window.scrollTo(0,0);
  if (to.path.split("/")[1].toLowerCase() == "cms") EventBus.$emit("routeChange", true);
  else EventBus.$emit("routeChange", false);
  next();
});

export default router;
