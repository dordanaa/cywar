<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'phone' => $faker->unique()->safeEmail,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => NULL,
        'password' => '$2y$10$kA27SpO.Ge5TcPZbik.dM.OHphCf1DqmSavF/CHKciB14IT7Rxaoe', // password
        'remember_token' => NULL,
        'image' => 'default_avatar.png',
        'created_by' => NULL,
        'updated_by' => NULL,
        'status' => 1,
    ];
});
