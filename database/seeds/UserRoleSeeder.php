<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UserRole::class, 50)->create()->each(
            function($u){
                factory(App\Post::class, 10)->create()
                ->each(
                    function($p) use (&$u) { 
                        $u->posts()->save($p)->make();
                    }
                );
            }
        );
    }
}
