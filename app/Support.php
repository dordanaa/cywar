<?php

namespace App;

use App\Classes;
use App\UserClass;
use App\Mail\NewSupportRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $guarded = [];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function alldata(){
        return $this->hasOne(User::class, 'id', 'user_id')
                    ->join('user_classes AS uc', 'uc.user_id', 'users.id')
                    ->join('classes AS c', 'c.id', 'uc.class_id')
                    ->select('users.id', 'users.name AS user', 'c.name AS class');
    }

    public function category(){
        return $this->hasOne(SupportCategory::class, 'id', 'support_category_id');
    }

    static public function getAll(&$data, $pagination, $status, $classes)
    {
        if(Auth::user()->role->role == 'admin')
        {
            $data = Self::join('users AS u', 'u.id', 'supports.user_id')
                        ->where('supports.status', $status)
                        ->with('category')
                        ->orderBy('created_at', 'desc')
                        ->select('supports.id', 'supports.support_category_id', 'supports.user_id', 'supports.content AS support_content', 'supports.status', 'supports.image', 'supports.created_at', 'u.name', 'u.email')
                        ->paginate($pagination);
        }
        else
        {
            $data = Self::join('users AS u', 'u.id', 'supports.user_id')
                        ->where('supports.status', $status)
                        ->join('user_classes AS uc', 'uc.user_id', 'u.id')
                        ->whereIn('uc.class_id', $classes)
                        ->with('category')
                        ->select('supports.id', 'supports.support_category_id', 'supports.user_id', 'supports.content AS support_content', 'supports.status', 'supports.image', 'supports.created_at', 'u.name')
                        ->paginate($pagination);
        }
    }

    static public function count(&$data, $classes)
    {
        if(Auth::user()->role->role == 'admin')
        {
            $data = Self::where('status', 0)->count();
        }
        else
        {
            $data = Self::join('users AS u', 'u.id', 'supports.user_id')
                        ->join('user_classes AS uc', 'uc.user_id', 'u.id')
                        ->whereIn('uc.class_id', $classes)
                        ->where('supports.status', 0)
                        ->count();
        }
    }

    static public function new($request)
    {
        $image_name = null;

        if( $request->hasFile('file') && $request->file('file')->isValid() ){
            $file = $request->file('file');
            if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return ['Support Created Succesfully', 201];

            $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
            $request->file('file')->move(public_path() . '/images/supports/', $image_name);
            // $img = Image::make(public_path() . '/images/supports/' . $image_name);
            // $img->resize(1200, 1200, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
        }

        $sup = new Self;
        $sup->support_category_id = $request->category;
        $sup->user_id = Auth::user()->id;
        $sup->content = $request->content;
        $sup->image = $image_name;
        $sup->created_at = now();
        $sup->save();


        $class = UserClass::where('user_id', $sup->user_id)->first(['class_id']);

        if($class)
        {
            $teachers = UserClass::where('user_classes.class_id', $class->class_id)
                            ->join('users AS u', 'u.id', 'user_classes.user_id')
                            ->join('user_roles AS ur', 'ur.user_id', 'u.id')
                            ->where('ur.role_id', 58)
                            ->get();

            if($teachers)
            {
                $emails = [];

                foreach($teachers AS $teacher){
                    $emails[] = $teacher->email;
                }

                if(count($emails))
                {
                    $sup->load('alldata');
                    Mail::to($emails)->send(new NewSupportRequest($sup));
                }
            }
        }

        return ['Support Created Succesfully', 201];
    }

    static public function updateStatus($classes, $request)
    {
        if(!self::checkAuth($classes, $request->id)) return ['Support Is Unavailable For You', 403];
        $support = Self::find($request->id);
        $support->status = $request->status;
        $support->updated_at = now();
        $support->updated_by = Auth::user()->id;
        $support->save();
        return ['Support Status Updated Successfully', 200];
    }

    static public function MultipleAction($classes, $request)
    {
        if(!self::checkAuth($classes, $request->data)) return ['Support Is Unavailable For You', 403];
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($classes, $id);
            }
            return ['Supports have been deleted successfully', 200];
        }else{
            if($request->mode == 'Finihsed') $status = 2;
            else if($request->mode == 'Working On') $status = 1;
            else $status = 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Supports status has been changed successfully to $request->mode", 200];
        }
    }

    static public function remove($classes, $id)
    {
        if(!self::checkAuth($classes, $id)) return ['Support Is Unavailable For You', 403];
        $sup = Self::find($id);
        if($sup){
            $sup->delete();
            return ['Support Deleted Successfully', 200];
        }
        return ['Support Not Found', 404];

    }

    static private function checkAuth($classes, $ids)
    {
        if(Auth::user()->role->role == 'admin') return true;

        if(is_array($ids))
        {
            foreach($ids AS $id)
            {
                $supp =  Self::where('id', $id)->first(['id', 'user_id']);
                if($supp && UserClass::whereIn('class_id', $classes)->where('user_id', $supp->user_id)->count() ) return true;
            }
        }
        else
        {
            $supp =  Self::where('id', $ids)->first(['id', 'user_id']);
            if($supp && UserClass::whereIn('class_id', $classes)->where('user_id', $supp->user_id)->count() ) return true;
        }
            return false;
    }

}
