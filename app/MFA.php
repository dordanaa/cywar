<?php

namespace App;

use App\User;
use App\Qrcode;
use App\Classes;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class MFA
{
    static public function updateMFA($request)
    {
        $user = User::find($request->id);
        if($user && User::checkAuthorization($user))
        {
            $user->active_2fa = $request->active_2fa;
            $user->updated_at = now();
            $user->updated_by = Auth::user()->id;
            $user->save();

            return ["$user->name MFA Updated Successfully", 200];
        }else{
            return ['Cannot Update User MFA Off Your Authorization', 400];
        }
    }

    static public function updateClassMFA($active_2fa, $id)
    {
        if(Classes::checkClass($id))
        {
            Classes::where('id', $id)->update(['active_2fa' => $active_2fa]);
            return ["Class's MFA Updated Successfully", 200];
        }
        return ['Cannot Update A Class Not Under Your Authorization', 400];
    }

    static public function newQR($user)
    {

        $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
        $user->google2fa_secret = $google2fa->generateSecretKey();
        $QRURL = $google2fa->getQRCodeInline(
            'Cywar HackerU',
            $user->email,
            $user->google2fa_secret
        );

        User::where('id', $user->id)->update(['google2fa_secret' => $user->google2fa_secret]);

        Qrcode::where('email', $user->email)->delete();

        $token = Str::random(50);

        $new = new Qrcode;
        $new->email = $user->email;
        $new->qrcode = $QRURL;
        $new->token = $token;
        $new->save();

        Token::where('user_id', $user->id)->delete();
        return $token;
    }

    static public function newClassQR($id)
    {
        $students = User::join('user_classes AS uc', 'uc.user_id', 'users.id')
                        ->join('user_roles AS ur', 'ur.user_id', 'users.id')
                        ->join('roles AS r', 'r.id', 'ur.role_id')
                        ->where('uc.class_id', $id)
                        ->where('r.role', 'student')
                        ->select('users.id', 'users.email', 'users.google2fa_secret')
                        ->get();
        
        foreach($students AS $student){
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
            $student->google2fa_secret = $google2fa->generateSecretKey();
            $QRURL = $google2fa->getQRCodeInline(
                'Cywar HackerU',
                $student->email,
                $student->google2fa_secret
            );

            User::where('id', $student->id)->update(['google2fa_secret' => $student->google2fa_secret]);

            Qrcode::where('email', $student->email)->delete();

            $token = Str::random(50);
            $new = new Qrcode;
            $new->email = $student->email;
            $new->qrcode = $QRURL;
            $new->token = $token;
            $new->save();

            $student->token = $token;

            Token::where('user_id', $student->id)->delete();
        }
        return $students;
    }

    static public function getQR($email)
    {
        $QRRow = Qrcode::where('email', $email)->first();
        $QRRow->delete();
        return [$QRRow->qrcode, 200];
    }
}
