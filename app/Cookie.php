<?php

namespace App;

use App\CookieLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Cookie extends Model
{
    static public function createNew($request)
    {
        $new = new Self;
        $new->content = $request->english;
        $new->content_heb = $request->hebrew;
        $new->created_by = Auth::user()->id;
        $new->save();

        CookieLog::new($new);
    }
 
    static public function updateCookie($request)
    {
        $update = Self::find($request->id);
        $update->content = $request->english;
        $update->content_heb = $request->hebrew;
        $update->updated_by = Auth::user()->id;
        $update->save();

        CookieLog::new($update);
    }
}
