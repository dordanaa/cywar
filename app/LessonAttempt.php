<?php

namespace App;

use App\MissionTaskHint;
use App\MissionTaskAttempt;
use App\PracticeArenaTotalScore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class LessonAttempt extends Model
{
    protected $guarded = [];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d'
    ];

    static public function new($request)
    {
        $id = Auth::user()->id;
        $lid = $request->lesson['id'];
        $points = 0;

        $lesson = Lesson::where('id', $lid)->with('points')->withCount('tasks')->first();
        if($lesson)
        {
            $data = self::getLessonPoints($id, $lesson);
            $points = $data[0];
            $status = $data[1];
        }

        if($points > 0) PracticeArenaTotalScore::set($points);
        if($status) PracticeArenaTotalScore::where('user_id', $id)->increment('lessons', 1);

        $attempt = Self::where([['user_id', $id], ['lesson_id', $lid]])->first();
        if($attempt) // if user already tested this task 
        {
            if(!$attempt->status)
            {
                $attempt->update([
                    'status' => $status,
                    'updated_at' => now(),
                    'attempts' => DB::raw('attempts + 1'),
                    'points' => $points,
                ]);
            }
        }
        else
        {
            $attempt = new Self;
            $attempt->user_id = $id;
            $attempt->lesson_id = $lid;
            $attempt->points = $points;
            $attempt->status = 0;
            $attempt->attempts = 1;
            $attempt->save();
        }
    }
    
    static public function getLessonPoints($user, $lesson)
    {
        $arr = [];
        foreach($lesson->tasks AS $task){
            $arr[] = $task->id;
        }
        $tasks = MissionTaskAttempt::where('mission_task_attempts.user_id', $user)
                                   ->where('mission_task_attempts.status', 1)
                                   ->whereIn('mission_task_attempts.mission_task_id', $arr)
                                   ->leftjoin('mission_task_hints AS hint', function($q){
                                       $q->on('hint.mission_task_id', 'mission_task_attempts.mission_task_id');
                                       $q->on('hint.user_id', 'mission_task_attempts.user_id');
                                   })->get(['mission_task_attempts.id', 'mission_task_attempts.status', 'hint.id AS hint']);
        $points = 0;
        $status = 0;
        foreach($tasks AS $task){
            if($task->hint == null) $points++;
            if($task->status == 1) $status++;
        }
        $status = count($arr) == $status ? 1 : 0;
        $points = $lesson->points->points / count($arr) * $points;
        return [$points, $status];
    }
}
