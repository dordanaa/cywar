<?php

namespace App;

use App\Mission;
use App\ClassAvailableLesson;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function module(){
        return $this->hasOne(Module::class, 'id', 'module_id')->select('name', 'id');
    }

    public function available(){
        return $this->hasOne(ClassAvailableLesson::class, 'lesson_id', 'id')
                    ->join('user_classes AS uc', 'uc.class_id', 'class_available_lessons.class_id')
                    ->where('uc.user_id', Auth::user()->id)
                    ->select('class_available_lessons.lesson_id');
    }

    public function points(){
        return $this->hasOne(Point::class, 'id', 'points_id')->select('name', 'points', 'id');
    }
   
    public function missions(){
        return $this->hasMany(Mission::class, 'lesson_id', 'id')
                    ->where('status', 1)
                    ->with('tasks')
                    // ->orderBy('order')
                    // ->select('lesson_id', 'id', 'title', 'description', 'order', 'mission_id');
                    ->select('lesson_id', 'id', 'title', 'description');
    }
   
    public function cms_missions(){
        return $this->hasMany(Mission::class, 'lesson_id', 'id')
                    // ->orderBy('order')
                    ->with('cms_tasks');
    }

    public function tasks(){ 
        return $this->hasMany(Mission::class, 'lesson_id', 'id')
                    ->join('mission_tasks AS mt', 'mt.mission_id', 'missions.id');
    }

    public function tasks_count(){ 
        return $this->hasMany(Mission::class, 'lesson_id', 'id')
                    ->join('mission_tasks AS mt', 'mt.mission_id', 'missions.id')
                    ->select('missions.lesson_id');
    }

    public function tasks_finished_count(){
        return $this->hasMany(Mission::class, 'lesson_id', 'id')
                    ->join('mission_tasks AS mt', 'mt.mission_id', 'missions.id')
                    ->join('mission_task_attempts AS mta', 'mta.mission_task_id', 'mt.id')
                    ->where('user_id', Auth::user()->id)
                    ->where('mta.status', 1)
                    ->select('missions.lesson_id');
    }

    static public function getAll($pagination, $request, &$data)
    {
        $data = Self::join('modules AS m', 'm.id', 'lessons.module_id')
                    ->join('points AS p', 'p.id', 'lessons.points_id');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->select('lessons.*', 'm.name AS module', 'p.name AS points')->paginate($pagination);
        // $data = Lesson::with('module', 'points')
        //               ->select('id', 'name', 'points_id', 'module_id', 'created_at', 'status')
        //               ->paginate($pagination);
    }

    static public function createNew($request)
    {
        $last = Self::where('module_id', $request->module_id)->orderBy('number', 'desc')->first();
        $number = $last ? $last->number + 1 : 1;
        
        $lesson = new Self;
        $lesson->name = $request->name;
        $lesson->module_id = $request->module_id;
        $lesson->points_id = $request->points_id;
        $lesson->status = $request->status;
        $lesson->number = $number;
        $lesson->docker = $request->docker;
        $lesson->play_time = $request->docker_time;
        $lesson->objectives = $request->objectives;
        $lesson->created_by = Auth::user()->id;
        $lesson->save();

        // Mission::new($request->cms_missions, $lesson->id);
    }

    static public function updateLesson($request)
    {
        self::checkNumber($request->module_id, $request->number, $request->id);
        
        $lesson = Self::find($request->id);
        $lesson->module_id = $request->module_id;
        $lesson->points_id = $request->points_id;
        $lesson->status = $request->status;
        $lesson->number = $request->number;
        $lesson->name = $request->name;
        $lesson->docker = $request->docker;
        $lesson->play_time = $request->play_time;
        $lesson->objectives = $request->objectives;
        $lesson->updated_by = Auth::user()->id;
        $lesson->save();

        return $lesson;
    }

    static public function checkNumber($mid, $number, $id)
    {
        if( $lesson = Self::where([['module_id', $mid],['number', $number],['id', '!=', $id]])->first() )
        {
            $lesson->number = $number + 1;
            $lesson->save();
            self::checkNumber($mid, $lesson->number, $lesson->id);
        }
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($id);
            }
            return ['Lessons have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Lessons status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }

    static public function remove($id)
    {
        Self::find($id)->delete();
    }

    static public function showLesson($id)
    {
        $lesson = Lesson::where('id', $id)->with('missions', 'points')->first();
        if(!$lesson) return ['Lesson Not Found', 404];
        
        if($lesson->status != 1) return ['Lesson Is Unavailable At The Moment', 404];

        $user = Auth::user();

        $role = $user->role->role;
        if($role == 'candidate') return ['Lesson Is Unavailable For You', 404];
        if($role == 'student')
        {
            if(UserAvailableLesson::where([['lesson_id', $id], ['user_id', $user->id]])->first(['id']))
            {
                return [$lesson, 200];
            }
            else
            {
                if($class = $user->class)
                {
                    if(ClassAvailableLesson::where([['class_id', $class->id], ['lesson_id', $id]])->first(['id']))
                    {
                        return [$lesson, 200];
                    }
                }
                return ['Lesson Is Unavailable For You', 400];
            }
        }
        else
        {
            return [$lesson, 200];
        }
    }
}
