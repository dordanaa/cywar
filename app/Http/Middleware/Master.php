<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Master
{
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role->role;
        if(in_array($role, ['admin', 'master'])){
            return $next($request);
        }else{
            return response()->json('Unauthorized', 401);
        }
    }
}
