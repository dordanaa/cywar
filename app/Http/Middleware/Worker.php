<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Worker
{
    // All roles that work in the company and need an access to the cms
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role->role;
        if(!in_array($role, ['student', 'candidate'])){
            return $next($request);
        }else{
            return response()->json('Unauthorized', 401);
        }
    }
}
