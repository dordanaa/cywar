<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AuthController;
class JustCode
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        preg_match('/^JC-\d+$/', $user->name, $match);
        if($match) {  
            $now = Carbon::now();
            $candidateUpdated = Carbon::parse($user->updated_at)->addHours(24); 
            
            if($now->greaterThan($candidateUpdated)) { //We Check if candidate created less than 24 hours
                AuthController::logout(Auth::user()->id);
            }
        }
        return $next($request);
    }
}