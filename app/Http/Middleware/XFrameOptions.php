<?php

namespace App\Http\Middleware;

use Closure;

class XFrameOptions
{
    public function handle($request, Closure $next)
    {
        // $response = $next($request)->header('X-Frame-Options', 'ALLOW FROM http://18.216.131.181');
        $response = $next($request)->header('Access-Control-Allow-Origin', '*')->header('X-Frame-Options', 'allow-from http://18.216.131.181');
        return $response;
    }
}
