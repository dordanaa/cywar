<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Payment
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status == 3) return response()->json('Need To Pay', 400);
        return $next($request);
    }
}
