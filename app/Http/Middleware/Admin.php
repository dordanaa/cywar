<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role->role == 'admin'){
            return $next($request);
        }else{
            return response()->json('Unauthorized', 401);
        }
    }
}
