<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSUpdateMFARequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'active_2fa' => 'required|numeric|between:0,1',
            'id' => 'required|numeric|min:1'
        ];
    }
}
