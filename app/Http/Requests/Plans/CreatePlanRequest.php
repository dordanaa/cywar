<?php

namespace App\Http\Requests\Plans;

use Illuminate\Foundation\Http\FormRequest;

class CreatePlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => "nullable|numeric",
            'name' => "required|string|min:2|max:100",
            'description' => "required|string|min:2|max:300",
            'price' => "required|numeric|min:1|max:10000",
            'duration' => "required|numeric|min:1|max:12",
        ];
    }
}
