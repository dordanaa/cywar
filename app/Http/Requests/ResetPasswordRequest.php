<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email|max:250',
            'password' => 'required|confirmed|max:250|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{8,40}$/',
            'token' => 'required|max:500'
        ];
    }
     
    public function messages()
    {
        return [
            'password.regex' => 'Minimum eight characters. At least one lowercase letter, uppercase letter and one number.',
            'class.exists' => 'This class name does not exists, please try again'
        ];
    }
}
