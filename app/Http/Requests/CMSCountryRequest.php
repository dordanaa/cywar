<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCountryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'country' => 'required|string|min:2|max:50|bail',
        ];
    }
}
