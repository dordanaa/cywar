<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSClassCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'college_id' => 'required|numeric|min:1',
            'status' => 'required|boolean',
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'name' => "required|string|min:2|max:50|bail|unique:classes,name",
        ];
    }
}
