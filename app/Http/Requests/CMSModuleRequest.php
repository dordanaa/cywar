<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSModuleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|string|min:2|max:100',
            'type' => 'required'
        ];
        if(request()->file) $rules['file'] = 'image|max:2048';
        return $rules;
    }
}
