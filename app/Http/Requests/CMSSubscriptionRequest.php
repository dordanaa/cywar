<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSSubscriptionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:2|max:50',
            'description' => 'required|min:2|max:500',
            'price' => 'required|numeric',
            'status' => 'required|numeric',
            'members' => 'required|numeric',
            'challenges' => 'required|numeric',
            'modules' => 'required|numeric',
            'duration' => 'required|numeric',
        ];
    }
}
