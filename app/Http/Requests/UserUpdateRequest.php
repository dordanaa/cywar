<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = Auth::user()->id;
        $rules = [
            'name' => 'required|min:2|max:50',
            'email' => "required|email|min:2|max:250|bail|unique:users,email,$id",
            'phone' => "nullable|string|bail|unique:users,phone,$id",
        ];

        if(request()->image) $rules['image'] = 'required';

        if(request()->password || request()->old_password){
            $rules['old_password'] = 'min:9|max:250|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{9,40}$/';
            $rules['password'] = 'min:9|max:250|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{9,40}$/';
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'old_password.regex' => 'Minimum 9 characters, at least one uppercase letter, one lowercase letter and one number',
            'password.regex' => 'Minimum 9 characters, at least one uppercase letter, one lowercase letter and one number',
            'password.min' => 'Minimum 9 characters, at least one uppercase letter, one lowercase letter and one number',
            'password.max' => 'Minimum 9 characters, at least one uppercase letter, one lowercase letter and one number',
        ];
    }
}
