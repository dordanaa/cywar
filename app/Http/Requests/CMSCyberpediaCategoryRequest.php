<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCyberpediaCategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:100',
            'description' => 'nullable|string|max:1000',
            'file' => 'nullable|image|max:2048'
        ];
    }
}
