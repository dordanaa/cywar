<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCollegeRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = request()->id;
        return [
            'id' => 'required|numeric|min:1',
            'country_id' => 'required|numeric|min:1',
            'name' => "required|string|min:2|max:50|bail|unique:colleges,name,$id",
        ];
    }
}
