<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MultipleActionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mode' => 'required|in:Activate,Delete,Deactivate,Finished,Working On,Waiting',
            'data' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'data.required' => 'Sorry, you haven\'t chose any student'
        ];
    }
}
