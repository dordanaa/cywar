<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSUpdateFileRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'file' => 'required|max:5000'
        ];
    }
}
