<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SigninRequest extends FormRequest
{
   
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'email' => 'required|max:250',
            'password' => 'required|max:250',
            'key' => 'nullable|numeric|digits:6'
        ];
    }
}
