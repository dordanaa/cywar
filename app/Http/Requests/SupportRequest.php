<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupportRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'category' => 'required|numeric|max:250',
            'content' => 'required|min:2|max:500',
        ];
        if(request()->file) $rules['file'] = 'image|max:2048';
        return $rules;
    }
}
