<?php

namespace App\Http\Requests\CMS\Avatar;

use Illuminate\Foundation\Http\FormRequest;

class CMSAvatarCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'level' => 'required|numeric',
            'status' => 'required|numeric',
            'image' => 'required|image|max:2048',
        ];
    }
}
