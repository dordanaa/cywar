<?php

namespace App\Http\Requests\CMS\Avatar;

use Illuminate\Foundation\Http\FormRequest;

class CMSAvatarUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'level' => 'required|numeric',
        ];
    }
}
