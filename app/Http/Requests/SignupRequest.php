<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignupRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:50|bail|unique:users,name',
            'email' => 'required|email|max:250|bail|unique:users,email',
            'password' => 'required|confirmed|max:250|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{8,40}$/',
            'password_confirmation' => 'required|max:250',
            'class' => 'required|string|bail|exists:classes,name',
            'phone' => 'nullable|string|bail|unique:users,phone'
        ];
        return $rules;
    }
    
    public function messages()
    {
        return [
            'password.regex' => 'Minimum eight characters. At least one lowercase letter, uppercase letter and one number.',
            'class.exists' => 'This class name does not exists, please try again'
        ];
    }
}
