<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSUserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = request()->id;
        return [
            'name' => "required|min:2|max:250",
            'role' => "required|numeric|min:2|max:250",
            'email' => "required|email|max:250|bail|unique:users,email,$id",
            'image' => 'nullable|image|max:2048',
            'active_2fa' => 'required|numeric|between:0,1',
            'phone' => "nullable|max:250|bail|unique:users,phone,$id"
        ];
    }

}
