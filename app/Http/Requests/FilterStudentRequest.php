<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterStudentRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [];
        if(request()->status) $rules['status'] = 'required|numeric';
        if(request()->country) $rules['country'] = 'required|numeric';
        if(request()->class) $rules['class'] = 'required|numeric';
        if(request()->college) $rules['college'] = 'required|numeric';
        if(request()->pagination) $rules['pagination'] = 'required|numeric';
        return $rules;
    }
}
