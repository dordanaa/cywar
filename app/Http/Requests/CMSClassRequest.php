<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSClassRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = request()->id;
        return [
            'id' => 'required|numeric|min:1',
            'college_id' => 'required|numeric|min:1',
            'start_at' => 'required|date',
            'end_at' => 'required|date',
            'name' => "required|string|min:2|max:50|bail|unique:classes,name,$id",
        ];
    }
}
