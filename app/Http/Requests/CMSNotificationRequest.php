<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSNotificationRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:100',
            'color' => 'required|string|min:2|max:100',
        ];
    }
}
