<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSLevelCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'level' => 'required|numeric|min:1',
            'points' => 'required|numeric|min:1',
        ];
    }
}
