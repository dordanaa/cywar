<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSLessonMissionsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'missions' => 'required|array|max:50',
            'missions.*' => 'nullable|max:500',
        ];
    }
}
