<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSGameRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'category_id' => 'required|numeric|min:1',
            'point_id' => 'required|numeric|min:1',
            'name' => 'required|string|min:2|max:200|unique:games,name',
            'description' => 'required|string|max:500',
            // 'title' => 'nullable|string|max:500',
            'objective' => 'required|string|max:500',
            'story' => 'required|string|max:500',
            'hint' => 'required|string|max:500',
            'image' => 'required|image|max:2048',
            'guide' => 'required|file',
            'tags' => 'nullable|string',
            'hint_points' => 'required|numeric|min:2',
        ];
    }

}
