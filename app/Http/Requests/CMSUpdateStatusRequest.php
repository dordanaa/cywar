<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSUpdateStatusRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'status' => 'required|numeric|min:0',
        ];
    }
}
