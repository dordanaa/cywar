<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|min:2|max:100',
            'description' => 'nullable|string|max:300',
            'min_points' => 'required|numeric|min:1',
            'slug' => 'required|string|min:2|max:50',
            'min_level' => 'required|numeric|min:1',
            'file' => 'nullable|image|max:2048'
        ];
    }
}
