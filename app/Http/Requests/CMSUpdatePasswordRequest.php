<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSUpdatePasswordRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|min:1',
            'password' => 'min:8|max:255|confirmed|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{8,40}$/',
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'password must be at least 9 chars & contain at least one lowercase, one uppercase and one numeric',
        ];
    }
}
