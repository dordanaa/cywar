<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCyberpediaTermRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:2|max:200',
            'category_id' => 'required|integer|min:0',
            'content' => 'required|string|min:2',
            'status' => 'required|integer|between:0,1',
            'link' => 'nullable|string|max:1000',
            'file' => 'nullable|image|max:2048'
        ];
    }
}
