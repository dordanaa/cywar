<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSLessonRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => "required|string|max:100",
            'module_id' => 'required|numeric|min:1',
            'points_id' => 'nullable|numeric|min:1',
            'objectives' => 'required|string|max:500',
            'docker' => 'nullable|string|min:2|max:200',
            'play_time' => 'required|numeric|min:1',
        ];
    }
}
