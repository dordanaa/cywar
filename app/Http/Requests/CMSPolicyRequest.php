<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSPolicyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'english' => 'required|string',
            'hebrew' => 'required|string',
        ];
    }
}
