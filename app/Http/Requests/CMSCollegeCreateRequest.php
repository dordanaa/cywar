<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSCollegeCreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'country_id' => 'required|numeric|min:1',
            'status' => 'required|boolean',
            'name' => "required|string|min:2|max:50|bail|unique:colleges,name",
        ];
    }
}
