<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable|numeric|min:1',
            'name' => 'required|string|min:2|max:50',
            'description' => 'nullable|string|max:300',
        ];
    }
}
