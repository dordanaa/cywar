<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CMSGameUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = request()->id;
        $rules = [
            'id' => 'required|numeric|min:1',
            'category_id' => 'required|numeric|min:1',
            'point_id' => 'required|numeric|min:1',
            'name' => 'required|unique:games,name,' . $id,
            'description' => 'required|string|max:500',
            // 'title' => 'required|string|max:500',
            'objective' => 'required|string|max:500',
            'story' => 'required|string|max:500',
            'hint' => 'required|string|max:500',
            'hint_points' => 'required|numeric|min:1',
        ];
        if(request()->guide) $rule['guide'] = 'required|file|mimes:pdf';
        return $rules;
    }

}
