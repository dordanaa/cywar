<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class CMSUserCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        $rules = [
            'name' => 'required|min:2|max:50',
            'email' => 'required|email|max:250|bail|unique:users,email',
            'status' => 'required|boolean',
            'role' => 'required|numeric|min:1',
            'phone' => 'required|string|bail|unique:users,phone|numeric',
            'password' => 'required|confirmed|max:250|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d#$@!%&*\^?]{8,40}$/',
            'password_confirmation' => 'required|max:250'
        ];

        if(Auth::user()->role->role != 'admin')
        {
            if(request()->role == 58) $rules['classes'] = 'required|max:250';
            if(request()->role == 67) $rules['colleges'] = 'required|max:250';
        }

        return $rules;
    }
    
    public function messages()
    {
        return [
            'password.regex' => 'Minimum eight characters. At least one lowercase letter, uppercase letter and one number.',
        ];
    }
}
