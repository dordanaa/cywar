<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|string|max:10',
            'name' => 'string|max:50',
            'email' => 'email|max:250',
            'password' => 'required|string|max:250',
            'game_id' => 'required|string',
            'points' => 'required|string|max:3',
        ];
    }
}
