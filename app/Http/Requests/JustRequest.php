<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JustRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'justcode_application_id' => 'required|string|max:10',
            'justcode_user_id' => 'required|string|max:50',
            'hackeru_exam' => 'required|string',
            'candidate_email' => 'required|email|max:100',
            'duration_in_mins' => 'nullable|integer',
            'host' => 'required|string|max:100',
            'token' => 'required|string|max:100',
        ];
    }
}
