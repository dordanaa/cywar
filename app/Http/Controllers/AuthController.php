<?php

namespace App\Http\Controllers;

use App\MFA;
use App\User;
use App\Token;
use App\PasswordReset;
use App\EmailVerification;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Services\Auth\LoginService;
use App\Http\Requests\SigninRequest;
use App\Http\Requests\SignupRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SigninAPIRequest;
use App\Http\Requests\EmailVerifyRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Services\Mail\MailService;

class AuthController extends Controller
{
    public function login(SigninRequest $request, LoginService $loginService)
    { 
      $res = $loginService::init();
      return response()->json($res[0], $res[1]);
    }
  
    public function apiLogin(SigninAPIRequest $request, LoginService $loginService)
    { 
      $res = $loginService::init();
      return response()->json($res[0], $res[1]);
    }

    public function signup(SignupRequest $request)
    {
      if($user = User::signup($request)){
        MailService::send($request->email, 'Signup', $user);
        return response()->json($user->qrcode, 200);
      }
        return response()->json('Unable to create user', 400);
    }

    static public function logout()
    {
      $id = Auth::user()->id;
      Token::where('user_id', $id)->delete();
    }

    static public function refreshToken(SigninRequest $request, LoginService $loginService)
    {
      $user = Auth::user();
      self::logout();
      $res = $loginService::init($user);
      return response()->json($res[0], $res[1]);
    }

    public function verifyEmail(EmailVerifyRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if($user->email_verified_at === NULL)
        {
          // Checks if the variables of the users, if they are valid
          $verify = EmailVerification::where([['email', $request->email], ['token', $request->token]])->first();
          if($verify)
          {
            // If user need to pay he will be have status = 3 / before payment
            $status = $user->subscription_id != 6 ? 3 : 2; 
            User::where('email', $verify->email)->update(['email_verified_at' => now(), 'status' => $status]);
            // $verify->delete();
            return response()->json('Email Verified', 200);
          }
            return response()->json('Email Not Verified', 400);
        }
          return response()->json('Email Verified', 200);
    }

    public function getQRCode(EmailVerifyRequest $request)
    {
      $user = User::where('email', $request->email)->first();
      if($user)
      {
        $res = MFA::getQR($user->email);
        return response()->json($res[0], $res[1]);
      }
    }

    public function forgotPass(Request $request)
    {
      $request->validate(['email' => 'required|email']);
      $user = User::where('email', $request->email)->first();
      if($user)
      {
        // Create a tempt token for a tempt link & send it to the user's mail  
        $user->token = Str::random(50);
        PasswordReset::insert(['email' => $user->email, 'token' => $user->token, 'created_at' => now()]);
        MailService::send($user->email, 'ResetPassword', $user);
        return $user;
      }
      return response()->json('Email Has Been Sent', 200);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
      // Checks if the user the sumbited the form in 40 mins since the mail has been sent
      $verify = PasswordReset::where([['email', $request->email], ['token', $request->token], ['created_at', '>', Carbon::now()->subMinutes(40)->toDateTimeString()]])->first();
      if($verify)
      {
        $user = User::where('email', $request->email)->first();
        if($user) $user->update([
          'password' => bcrypt($request->password)
        ]);
        return response()->json('You Have Reset Your Password Successfully', 200);
      }
      return response()->json('Sorry, Unabled To Reset Your Password, Please Try Again', 400);
    }
}
