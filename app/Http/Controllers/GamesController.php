<?php

namespace App\Http\Controllers;

use App\Game;
use App\Docker;
use App\Attempt;
use App\Category;
use App\GameHint;
use App\Container;
use App\GameDetail;
use App\UserAllowedGame;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GamesController extends Controller
{
    public function index()
    {
        Category::getCats($cats);
        return response()->json($cats);
    }

    public function game($name)
    {
        $res = Game::findGame($name);
        if(!is_array($res)){
            $data = [];
            Game::playGame($name, $data);
            return response()->json($data, 200);
        }
        return response()->json($res[0], $res[1]);
    }

    public function hint($id)
    {
        if(Game::find($id)){
            GameHint::new($id);
            $hint = GameDetail::where('game_id', $id)->first('hint');
            return response()->json($hint);
        }
    }

    public function submit(Request $request, $game)
    {
        $res = Attempt::new($request, $game);
        $jc = $res[2] ?? null;
        return response()->json(['data' => $res[0], 'jc' => $jc], $res[1]);
    }

    public function runDocker($id)
    {
        $res = Docker::run($id);
        return response()->json($res[0], $res[1]);
    }

    public static function GetGameStructure($token, $status = 1, $pagination = 1000, $showId = null) { // Show all games for Just Code
        if($token !== '0Q8k3wKBJR6dTcqtq5Bp6f5sSNKg0FFG') {
            abort(403, 'Unauthorized action.');
        }
        Game::getAll($status, $pagination, $request = null, $data);
        $gameData = $data->toArray()['data'];
        $struture = [];
        foreach ($gameData as $value) {
            $friendlyUrl = strtolower(str_replace(' ','-',$value['name']));
            if(!$showId) {
                $struture[] = [
                    'link' => $value['slug']."/".$friendlyUrl,
                    'points' => $value['points'],
                    'name' => $value['name'], 
                    'description' => $value['description'],
                 ];
            } else {
                $link = $value['slug']."/".$friendlyUrl;
                $id = $value['id'];
                $struture[$link] = $id;
            }

        }
        return $struture;
    }

    public function reset($id)
    {
        // Container::where('user_id', Auth::user()->id)->where('ip', '13.59.192.100')->delete();
        Container::where('user_id', Auth::user()->id)->where('game_id', $id)->delete();
        $res = Docker::run($id);
        return response()->json($res[0], $res[1]);
    }
    
    public function pulse(Request $request)
    {
        $res = Docker::checkPulse($request);
        $status = $res ? 200 : 500;
        return response()->json($res, $status);
    }
}
