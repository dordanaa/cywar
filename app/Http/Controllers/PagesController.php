<?php

namespace App\Http\Controllers;

use App\Tnc;
use App\Cookie;
use App\CyberpediaCategory;
use App\CyberpediaTerm;
use App\Privacy;
use App\Subscription;
use App\TotalScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{
    public function HOF(Request $request) // Hall of Fame
    {
        TotalScore::HOF($res, $request->limit);
        return response()->json($res);
    }

    public function storageIst(Request $file)
   {
      $role = Auth::user()->role->role;
      if(in_array($role, ['student', 'candidate'])) return response()->json(['data' => 'Unauthorized To Access The Game Instruction'], 400);
      $url = "LabInstructions/" . $file['file'];
      $file->validate([
         'file' => 'required|min:3|max:255|string',
      ]);
      return Storage::response($url);
   }

   public function signup()
   {
      $data = [];
      $data['tnc'] = Tnc::orderBy('updated_at', 'desc')->first();
      $data['privacy'] = Privacy::orderBy('updated_at', 'desc')->first();
      $data['cookies'] = Cookie::orderBy('updated_at', 'desc')->first();
      $data['subs'] = Subscription::where('status', 1)->where('name', '!=', 'Old Student')->orderBy('name')->get();
      return response()->json($data);
   }

   public function getNews(Request $request)
   {
      $ip = "https://hnrss.org/newest?q=$request->search&comments=0";
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $ip);
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      // curl_setopt($curl, CURLOPT_COOKIE, 'AspxAutoDetectCookieSupport=1');
      $result = curl_exec($curl);
      $result = json_decode($result);
      curl_close($curl);
      
      return $result;
   }

}
