<?php

namespace App\Http\Controllers\CMS;

use App\Game;
use App\Point;
use Exception;
use App\Lesson;
use App\Mission;
use App\Category;
use App\GameFile;
use App\GameInput;
use App\GameDetail;
use App\MissionTask;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Services\Logger\LoggerService;

class PodController extends Controller
{
    static public $podDescVersion = '';
    static public $packages = [];
    static public $packagesInstalled = 0;
    static public $currentPackage = [];
    static public $data = [];
    static public $podContext = '';
    static public $origin_schema = '';
    static public $verifying_schema = [[], []];

    /**
     * Initial Function to upload the PodFile
     */
    public function initial()
    {
        LoggerService::init('INFO', 'Starting installing', 'pod');
        
        $res = self::getPackages();
        if($res) return response()->json($res[0], $res[1]);
        
        foreach(self::$packages AS $key => $package){
            self::$currentPackage = $key;
            $res = self::installPackage($package);
            if($res)
            {
                $full_response = [
                    'Content' => $res[0],
                    'Current Package' => self::$packages[self::$currentPackage]['path'],
                    'Packages Installed' => self::$packagesInstalled
                ];
                LoggerService::init('ERROR', json_encode($full_response), 'pod');
                continue;
            }
            LoggerService::init('INFO', 'Packages: ' . self::$packages[self::$currentPackage]['path'] . ', installed successfully', 'pod');
            self::$packagesInstalled = self::$packagesInstalled + 1;
        }   

        $full_response = [
            'Content' => 'Finished installing packages',
            'Packages Installed successfully' => self::$packagesInstalled,
            'packages failed installing' => count(self::$packages) - self::$packagesInstalled
        ];
        LoggerService::init('INFO', json_encode($full_response), 'pod');
        return response()->json($full_response);
    }

    /**
     * Get the package from the packages folder in the server
     */
    private static function getPackages()
    {
        // find all the packages
        $path = public_path() . "\packages";
        $packages = array_diff(scandir($path), array('.', '..'));
        
        // run over all the packages
        foreach($packages AS $key => $package)
        {
            $res = self::scanPackage($package, $path, $key);
            if($res) return $res;
        }
    }

    /**
     * Scan the packages
     * 
     * @param array $package
     * @param string $path
     * @param numeric $key
     */
    private static function scanPackage($package, $path, $key)
    {
        $files = array_diff(scandir($path . "\\$package"), array('.', '..'));

        self::$packages[$key] = [ 'path' => $package ];

        // if(!in_array('src', $files)) return ['Package error. Did not found `src` folder', 422];
        
        // searching for the json file
        foreach($files AS $file)
        {
            $name = explode('.', $file);
            $name = $name[count($name) - 1];
            if($name === 'json') self::$packages[$key]['index'] = $file;
        }

        // checks if found json
        if(!isset(self::$packages[$key]['index'])) return ['Package error. Did not found `json` file', 422];
    }

    /**
     * Install the pacakge
     * 
     * @param array $package
     */
    private static function installPackage($package)
    {
        // read the file
        $path = public_path() . '\packages\\' . $package['path'] . '\\' . $package['index'];
        $file = file_get_contents($path);
        $package_json = json_decode($file, true);
        if(!$package_json) return ["Json File is invalid. Cannot read it", 400];

        // verify version
        $badVersion = self::setPodVersion($package_json['podDescVersion']);
        if($badVersion) return ["podDescVersion is invalid. Recieved `$badVersion`", 422];

        // get the schema
        $res = self::getSchema();
        if($res) return $res;

        // verify the schema
        $res = self::verifySchema($package_json);
        if($res) return $res;

        // set header
        $res = self::setHeader($package_json['podInfo']);
        if($res) return $res;

        // set context, (the body)
        $res = self::setContext($package_json['context']);
        if($res) return $res;

        // set files
        $res = self::setFiles($package_json['files']);
        if($res) return $res;

        // refresh the data for new package
        self::$data = [];
    }

    /**
     * Set the Version of the pod
     * 
     * @param array $version
     */
    private static function setPodVersion($version)
    {
        self::$podDescVersion = $version;
    }

    /**
     * Set the header
     * 
     * @param array $header
     */
    private static function setHeader($header)
    {
        // currently not using status in descriptor
        // $res = self::setPodStatus($header['status']);
        // if($res) return $res;

        $podContext = $header['context'];
        // check podContect ( the pod type )
        if(in_array($podContext, ['cyber-challenge', 'arena-practice', 'range-challenge']))
        {
            self::$podContext = $podContext;
            self::$data['type'] = $header['type'];
            self::$data['name'] = $header['name'];
            self::$data['serial_id'] = $header['uuid'];
            self::$data['notes'] = $header['notes'];
            self::$data['description'] = $header['skillset'];
            self::$data['previewImage'] = $header['previewImage'];

            return;
        }
        return ["Pod Context is invalid. Recieved `$podContext`. Expected: cyber-challenge|arena-practice|range-challenge", 422];
    }

    /**
     * Set the header pod status
     * 
     * @param array $header
     */
    private static function setPodStatus($status)
    {
        // check lesson status
        if(!in_array($status, ['Active', 'Inactive'])) return ["Lesson's Status is invalid. Recieved `$status`", 400];
        $status = $status == 'Active' ? 1 : 0;
        self::$data['status'] = $status;
    }

    /**
     * check the podContext type & redirect to the right settings function
     * 
     * @param array $context
     */
    private static function setContext($context)
    {
        if(self::$podContext === 'cyber-challenge') return self::setChallengeContext($context['challengeDesc']);
        else if(self::$podContext === 'arena-practice') return self::setLessonContext($context['practiceDesc']);
        // else if(self::$podContext === 'range-challenge') return self::setCyberRangeContext($context['cyberRangeDesc']);
    }

    /**
     * Set the files of the package
     * check if exists & if does, move them to the correct location
     * 
     * @param array $files
     */
    private static function setFiles($files)
    {
        if($files && count($files))
        {
            foreach($files AS $file)
            {
                // checks if has file is valid
                if($file['id'])
                {
                    self::setFilePaths($file);
                }
            }
            if(self::$data['type'] === 'code') self::setWebCode();
            $res = self::moveFiles();
            if($res) return $res;
            $res = self::saveFiles();
            if($res) return $res;
        }

    }

    /**
     * Set the Web Code Directory
     * Challenges which required visual code files
     */
    private static function setWebCode()
    {
        $package = self::$packages[self::$currentPackage]['path'];
        $file_name = self::$data['serial_id'] . 'web-code';
        $current_path = public_path() . '\packages' . "\\$package\\services\\web-code";
        $new_path = public_path() . '\\files\\' . $package . '-web-code';

        self::setFileProps($file_name, null, 'code', null, $new_path, $current_path, "$package-web-code");
    }

    /**
     * Set the file paths
     * 
     * @param numeric $key
     * @param array $files
     */
    private static function setFilePaths($file)
    {
        // Instruction guide pdf file
        if(self::$data['instructionGuide'] == $file['id'])
        {
            $file_type = 'guide';
            $db_name = 'games';
            $cat_slug = Category::where('id', self::$data['category_id'])->first(['slug'])->slug;
            $folder_path = storage_path() . '\app\\LabInstructions\\' . $cat_slug . '\\';
        }
        // preview image
        else if(self::$data['previewImage'] == $file['id'])
        {
            $file_type = 'image';
            $folder_path = public_path() . '\images\\';
            if(self::$podContext === 'cyber-challenge')
            {
                $db_name = 'games';
                $folder_path .= 'games\\';
            } 
            else if(self::$podContext === 'arena-practice')
            {
                $db_name = 'lessons';
                $folder_path .= 'lessons\\'; 
            }
            else if(self::$podContext === 'range-challenge')
            {
                $folder_path .= 'cyberRange\\'; 
            }
        }
        // all the other general files
        else
        {
            $db_name = $file_type = null;
            $folder_path = public_path() . '\files\\';
        }

        // set the paths
        $path_parts = explode('\\', $file['path']);
        $last_part = explode('.', $path_parts[count($path_parts) - 1]);
        $extenstion = $last_part[count($last_part) - 1];
        $file_name = self::$data['serial_id'] . '_' . $file['id'] . '.' . $extenstion;
        $new_path = $folder_path . $file_name;
        $current_path = public_path() . '\packages\\' . self::$packages[self::$currentPackage]['path'] . $file['path'];

        self::setFileProps($file_name, $file_type, 'download', $db_name, $new_path, $current_path, $file['id']);
    }

    /**
     * Set the file props
     * 
     * @param string $file_name
     * @param string $file_type
     * @param string $type
     * @param string $db_name
     * @param string $new_path
     * @param string $current_path
     * @param numeric $file_id
     */
    static public function setFileProps($file_name, $file_type, $type, $db_name = null, $new_path, $current_path, $file_id)
    {
        self::$data['files'][] = [
          'file_name' => $file_name,  
          'file_type' => $file_type,  
          'type' => $type,  
          'db' => $db_name,  
          'new_path' =>  $new_path,  
          'current_path' => $current_path,  
          'id' => $file_id,  
          'unique_id' => $file_id . '_' . self::$data['serial_id']
        ];
    } 

    /**
     * Set the context of a challenge
     * 
     * @param array $challenge
     */
    private static function setChallengeContext($challenge)
    {
        // set the info
        $res = self::setChallengeInfo($challenge['info']);
        if($res) return $res;
        
        // set the hints
        // in the future it should be in seperated table. should have multiple hints
        $res = self::setChallengeHints($challenge['content']['hints']);
        if($res) return $res;
        
        // set the flags
        $res = self::setChallengeFlags($challenge['content']['flags']);
        if($res) return $res;
        
        // save the main info
        $res = self::saveChallengeInfo();
        if($res) return $res;
        
        // save the details
        $res = self::saveChallengeDetails();
        if($res) return $res;
        
        // save the flags
        $res = self::saveChallengeFlags();
        if($res) return $res;
    }
    
    /**
     * Set the challenge info
     * 
     * @param array $challenge_info
     */
    private static function setChallengeInfo($challenge_info)
    {
        // set the category
        $res = self::setCategory($challenge_info, 'categories', 'category');
        if($res) return $res;

        // set the points
        $res = self::setPoints($challenge_info);
        if($res) return $res;

        // set the details
        $res = self::setChallengeDetails($challenge_info);
        if($res) return $res;
    }
    
    /**
     * Set the challenge details
     * 
     * @param array $challenge_info
     */
    private static function setChallengeDetails($challenge_info)
    {
        $identifier = $challenge_info['identifier'];
        self::$data['scenario'] = $scenario = $challenge_info['scenario'];
        if( !is_array($challenge_info['objectives']) ) return ["mission `$identifier`. Found error in objectives. Recieved `" . $challenge_info['objectives'] . '`', 422];
        self::$data['objectives'] = $objectives = self::setChallengeObjectives($challenge_info['objectives']);
        self::$data['instructionGuide'] = $instructionGuide = $challenge_info['instructionGuide'];
        
        // verify all the params
        if(!is_string($scenario)) return ["mission `$identifier`. Found error in scenario. Recieved `$scenario`", 422];
        if(!is_string($objectives)) return ["mission `$identifier`. Found error in objectives. Recieved `$objectives`", 422];
        if(!is_string($instructionGuide)) return ["mission `$identifier`. Found error in instructionGuide. Recieved `$instructionGuide`", 422];
    }

    /**
     * Set the objectives
     * Parse an array into string with the objectives schema
     * 
     * @param array $array
     */
    private static function setChallengeObjectives($array)
    {
        $string = '';
        foreach($array AS $obj)
        {
            $string .= "- $obj ~";
        }
        return $string;
    }
    
    /**
     * Set the challenge hint
     * 
     * @param array $challenge_hints
     */
    private static function setChallengeHints($challenge_hints)
    {
        // in the future should be many hints. now only one
        self::$data['hint'] = $challenge_hints[0];
    }
    
    /**
     * Set the challenge flags
     * 
     * @param array $challenge_flags
     */
    private static function setChallengeFlags($challenge_flags)
    {
        foreach($challenge_flags AS $flag){
            self::$data['flags'][] = $flag;
        }
    }

    /**
     * Set the context of a lesson
     * 
     * @param array $lesson
     */
    private static function setLessonContext($lesson)
    {
        $res = self::setLessonInfo($lesson['info']);
        if($res) return $res;
        $res = self::setLessonMissions($lesson['content']);
        if($res) return $res;

        $res = self::saveLessonMissions();
        if($res) return $res;
    }

    /**
     * Set the lesson info
     * 
     * @param array $lesson_info
     */
    private static function setLessonInfo($lesson_info)
    {
        // set the category
        $res = self::setCategory($lesson_info, 'modules', 'module');
        if($res) return $res;

        // set the points
        $res = self::setPoints($lesson_info);
        
        // check lesson overview
        if(!is_string($lesson_info['overview'])) return ["Lesson's overview must be in a type of string", 400];
        self::$data['objectives'] = $lesson_info['overview'];

        $res = self::saveLesson();
        if($res) return $res;
    }

    /**
     * Set the lesson missions
     * Set the order of the missions
     * 
     * @param array $data
     */
    private static function setLessonMissions($data)
    {
        $missions = $data['missions'];
        $orders = $data['order'];
        foreach($missions AS $mission_key => $mission)
        {
            foreach($orders AS $order_key => $order)
            {
                // checks for match to find the mission order
                if(isset($order[$mission['id']]))
                {
                    $missions[$mission_key]['order'] = $order_key + 1;
                    foreach($mission['tasks'] AS $mission_task_key => $mission_task)
                    {
                        foreach($order[$mission['id']] AS $order_task_key => $order_task)
                        {
                            // checks for match to find the tasks order
                            if($order_task === $mission_task['id']) 
                            {
                                $missions[$mission_key]['tasks'][$mission_task_key]['order'] = $order_task_key + 1;
                            }
                        }   
                    }
                }
            }
        }

        self::$data['missions'] = $missions;
    }

    /**
     * get the schema by the version
     */
    private static function getSchema()
    {
        $v = self::$podDescVersion;
        try{
            $file = file_get_contents("podVersions/$v.json");
            self::$origin_schema = json_decode($file, true);
        }catch(Exception $ex){
            return ['Schema File not found', 400];
        }
    }

    /**
     * verify the schema
     * 
     * @param file $file
     */
    private static function verifySchema($file)
    {
        $file = collect($file);
        $schema = collect(self::$origin_schema);
        // $type = $file['podInfo']['context'];

        // run inside files and makes arrays of key & value
        // incoming json file
        self::runLoop($file, 0);
        // json schema for validation
        self::runLoop($schema, 1, $file);

        // order the arrays so incase of wrong order but correct schema it will still match
        asort(self::$verifying_schema[0]);
        asort(self::$verifying_schema[1]);

        // dd(self::$verifying_schema[0], self::$verifying_schema[1]);
        
        // check for differences
        $diff = array_diff(self::$verifying_schema[1], self::$verifying_schema[0]);
        $diff2 = array_diff(self::$verifying_schema[0], self::$verifying_schema[1]);
        
        if($diff || $diff2) return [[
            'title' => 'Schemas Does not Match',
            'differences' => $diff ? $diff : $diff2
        ], 422];
    }
     
    /**
     * its a loop function for the running inside the schema
     * 
     * @param array $data
     * @param numeric $index
     */
    private static function runLoop($data, $index, $origin = null)
    {
        foreach($data AS $key => $value)
        {
            // if its the incoming file
            if($index === 0)
            {
                // set if its practice arena or challenge
                if($key === 'practiceDesc' && $value) self::$verifying_schema[] = 'arena-practice';
                if($key === 'challengeDesc' && $value) self::$verifying_schema[] = 'cyber-challenge';
                
                if($key === 'files' && !$value) continue;
                if($key === 'ports' && !$value) continue;
            }
            else
            {
                // ignores what ever it is not
                if( (self::$verifying_schema[2] === 'arena-practice' && $key === 'challengeDesc') || (self::$verifying_schema[2] === 'cyber-challenge' && $key === 'practiceDesc') ){
                    self::$verifying_schema[$index][] = $key;
                    continue;
                }
                if($key === 'files' && !$origin['files']) continue;
                if($key === 'ports' && !$origin['ports']) continue;
            }
            
            if($key === 'order')
            {
                self::$verifying_schema[$index][] = $key;
                continue;
            } 
            
            // checks if string, and then makes sure that a key is unique
            if(is_string($key)) if(!in_array($key, self::$verifying_schema[$index])) self::$verifying_schema[$index][] = $key;
            if(is_array($value) || is_object($value)) self::runLoop($value, $index);
        }
    }
     
    /**
     * check if the category is valid and set it
     * 
     * @param array $data
     * @param string $type
     * @param string $path
     */
    private static function setCategory($data, $type, $path)
    {
        $cat_name = $data[$path];
        $category = DB::table($type)->where('name', $cat_name)->first();
        if(!$category) return ["category `$cat_name` Not Found", 400];
        self::$data['category_id'] = $category->id;
        if(isset($category->type) && strtolower($category->type) != strtolower($data['category'])) return ["Module's type does not match", 400];
    }
     
    /**
     * check if the points is valid and set it
     * 
     * @param array $data
     */
    private static function setPoints($data)
    {
         $difficulty = $data['difficulty'];
         if( $points = Point::where('name', $difficulty)->first() ) self::$data['points_id'] = $points->id;
         else return ["Difficulty is invalid. Recieved `$difficulty`", 400];
    }
    
    /**
     * Creates / Updates a challenge
     * 
     */
    private static function saveChallengeInfo()
    {
        try{
            $challenge = Game::where('serial_id', self::$data['serial_id'])->first();
            if(!$challenge) $challenge = new Game;

            $challenge->category_id = self::$data['category_id'];
            $challenge->serial_id = self::$data['serial_id'];
            $challenge->name = self::$data['name'];
            $challenge->point_id = self::$data['points_id'];
            $challenge->description = self::$data['description'];
            // $challenge->image = self::$data['serial_id'] . '_' .self::$data['previewImage'];
            // $challenge->guide = self::$data['serial_id'] . '_' .self::$data['instructionGuide'];
            $challenge->notes = self::$data['notes'];
            $challenge->status = 1;
            $challenge->save();

            self::$data['id'] = $challenge->id;
        }catch(Exception $ex){
            $challenge = self::$data['serial_id'];
            return ["Failed to upload the challenge `$challenge`", 400];
        }        
    }
    
    /**
     * Creates / Updates a challenge's details
     * 
     */
    private static function saveChallengeDetails()
    {
        try{
            $challenge = GameDetail::where('game_id', self::$data['id'])->first();
            if(!$challenge) $challenge = new GameDetail;

            $challenge->game_id = self::$data['id'];
            $challenge->story = self::$data['scenario'];
            $challenge->objective = self::$data['objectives'];
            $challenge->hint = self::$data['hint']['text'];
            $challenge->hint_points = self::$data['hint']['cost'];
            $challenge->save();

        }catch(Exception $ex){
            $challenge = self::$data['serial_id'];
            return ["Failed to upload the challenge details `$challenge`", 400];
        }
    }
    
    /**
     * Creates / Updates a challenge's flags
     * 
     */
    private static function saveChallengeFlags()
    {
        foreach(self::$data['flags'] AS $key => $flag){
            try{
                $input = GameInput::where('serial_id', $flag['id'])->first();
                if(!$input) $input = new GameInput;
    
                $input->game_id = self::$data['id'];
                $input->serial_id = $flag['id'];
                $input->name = $flag['title'];
                $input->value = $flag['value'];
                $input->save();
            }catch(Exception $ex){
                $challenge = self::$data['serial_id'];
                return ["Failed to upload the challenge flag number $key, in challenge `$challenge`", 400];
            }
        }
    }
    
    /**
     * Creates / Updates a lesson
     * 
     */
    private static function saveLesson()
    {
        try{
            $lesson = Lesson::where('serial_id', self::$data['serial_id'])->first();
            if(!$lesson) $lesson = new Lesson;
            $lesson->module_id = self::$data['category_id'];
            $lesson->serial_id = self::$data['serial_id'];
            $lesson->name = self::$data['name'];
            $lesson->points_id = self::$data['points_id'];
            $lesson->objectives = self::$data['objectives'];
            $lesson->notes = self::$data['notes'];
            $lesson->status = 1;
            $lesson->save();

            self::$data['id'] = $lesson->id;
        }catch(Exception $ex){
            $lesson = self::$data['serial_id'];
            return ["Failed to upload the lesson `$lesson`", 400];
        }
    }
    
    /**
     * Creates / Updates a lesson missions
     * 
     */
    private static function saveLessonMissions()
    {
        $missions = self::$data['missions'];
        
        foreach($missions AS $data){
            $missionIdentifier = $data['id'];
            $missionDescription = $data['description'];
            $missionName = $data['name'];

            if(!isset($data['order'])) return ["mission `$missionIdentifier`. Found error in checker order. Haven't found order.", 422];
            $missionOrder = $data['order'];
            
            // verify all the params
            if(!is_string($missionIdentifier)) return ["mission `$missionIdentifier`. Found error in missionIdentifier. Recieved `$missionIdentifier`", 422];
            if(!is_string($missionDescription)) return ["mission `$missionIdentifier`. Found error in missionDescription. Recieved `$missionDescription`", 422];
            if(!is_string($missionName)) return ["mission `$missionIdentifier`. Found error in missionName. Recieved `$missionName`", 422];
            
            try{
                $mission = Mission::where('serial_id', $missionIdentifier)->first();
                if(!$mission) $mission = new Mission;
    
                $mission->lesson_serial_id = self::$data['serial_id'];
                $mission->lesson_id = self::$data['id'];
                $mission->serial_id = $missionIdentifier;
                $mission->title = $missionName;
                $mission->description = $missionDescription;
                $mission->order = $missionOrder;
                $mission->status = 1;
                $mission->save();
    
                foreach($data['tasks'] AS $task){
                    $res = self::saveLessonMissionTasks($task, $missionIdentifier);
                    if($res) return $res;
                }
                
            }catch(Exception $ex){
                return ["Failed to upload the mission $missionIdentifier", 400];
            }
        }
    }
    
    /**
     * Creates / Updates a lesson mission tasks
     * 
     * @param array $data
     * @param string $missionIdentifier
     */
    private static function saveLessonMissionTasks($data, $missionIdentifier)
    {
        $checkerIdentifier = $data['checkerId'];
        $taskName = $data['id'];
        $taskText = $data['instruction'];
        $taskSolText = $data['solution'];
        $taskExpl = $data['explanation'];

        if(!isset($data['order'])) return ["Mission `$missionIdentifier`, Task `$checkerIdentifier`. Found error in checker order. Haven't found order.", 422];
        $taskOrder = $data['order'];
        
        // verify all the params
        if(!is_string($checkerIdentifier)) return ["Task `$checkerIdentifier`. Found error in checkerIdentifier. Recieved `$checkerIdentifier`", 422];
        if(!is_string($taskText)) return ["Task `$checkerIdentifier`. Found error in taskText. Recieved `$taskText`", 422];
        if(!is_string($taskName)) return ["Task `$checkerIdentifier`. Found error in taskName. Recieved `$taskName`", 422];
        if(!is_string($taskSolText)) return ["Task `$checkerIdentifier`. Found error in taskSolText. Recieved `$taskSolText`", 422];
        if(!is_string($taskExpl)) return ["Task `$checkerIdentifier`. Found error in taskExpl. Recieved `$taskExpl`", 422];
        
        try{
            $task = MissionTask::where('serial_id', $checkerIdentifier)->first();
            if(!$task) $task = new MissionTask;

            $task->mission_serial_id = $missionIdentifier;
            $task->serial_id = $checkerIdentifier;
            $task->content = $taskText;
            $task->solution = $taskSolText;
            $task->explanation = $taskExpl;
            $task->order = $taskOrder;
            $task->status = 1;
            $task->save();

        }catch(Exception $ex){
            return ["Failed to upload the task $checkerIdentifier", 400];
        }
    }
  
    /**
     * move the files to the correct location on the server
     */
    private static function moveFiles()
    {
        foreach(self::$data['files'] AS $file)
        {
            try{
                rename($file['current_path'], $file['new_path']);
            }catch(Exception $ex){
                $flag_id = $file['id'];
                $challenge = self::$data['serial_id'];
                return ["Failed to move the file `$flag_id`, of `$challenge`", 400];
            }
        }
    }
    
    /**
     * Save the files
     * 
     * @param numeric $key
     */
    private static function saveFiles()
    {
        foreach(self::$data['files'] AS $data)
        {
            try{
                // if its a preview image or instruction
                if(isset($data['db']) && $data['db']) DB::table($data['db'])->where('serial_id', self::$data['serial_id'])->update([$data['file_type'] => $data['file_name']]);
                else
                {
                    $file = GameFile::where('serial_id', $data['unique_id'])->first();
                    if(!$file) $file = new GameFile;
    
                    $file->game_id = self::$data['id'];
                    $file->serial_id = $data['unique_id'];
                    $file->name = $data['id'];
                    $file->type = $data['type'];
                    $file->save();
                }
            }catch(Exception $ex){
                $flag_id = $data['id'];
                return ["Failed to save the file `$flag_id`", 400];
            }
        }
    }
}
