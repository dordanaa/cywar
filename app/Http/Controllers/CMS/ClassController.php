<?php

namespace App\Http\Controllers\CMS;

use App\MFA;
use App\Classes;
use Illuminate\Http\Request;
use App\ClassAvailableLesson;
use App\ClassUnavailableGame;
use App\Services\Mail\MailService;
use App\Http\Requests\CMSClassRequest;
use App\Http\Requests\CMSUpdateMFARequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\CMSClassCreateRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSUpdatePasswordRequest;

class ClassController extends CMSController
{
    public function index($pagination, Request $request)
    {
        Classes::getClasses($pagination, $cats, $request);
        return response()->json($cats);
    }
    
    public function show($id)
    {
        $data = [];
        $res = Classes::show($id, $data['class']);
        if($res) return response()->json($res, 400);
        $data['colleges'] = self::getColleges();
        return response()->json($data, 200);
    }
    
    public function new()
    {
        $data = self::getData();
        return response()->json($data, 200);
    }
    
    public function getStudents($id, $pagination, Request $request)
    {
        Classes::getStudents($id, $pagination, $data, $request);
        return response()->json($data, 200);
    }
    
    public function getUsers($id, $pagination)
    {
        $data = null;
        Classes::getUsers($id, $pagination, $data);
        return response()->json($data, 200);
    }

    public function create(CMSClassCreateRequest $request)
    {
        Classes::createNew($request);
        return response()->json('Class Created Successfully', 201);
    }
    
    public function update(CMSClassRequest $request)
    {
        $res = Classes::updateClass($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updatePass(CMSUpdatePasswordRequest $request)
    {
        $res = Classes::updateClassPasswords($request->password, $request->id);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateMFA(CMSUpdateMFARequest $request)
    {
        $res = MFA::updateClassMFA($request->active_2fa, $request->id);
        return response()->json($res[0], $res[1]);
    }
    
    public function newQRCode($id)
    {
        if(is_numeric($id))
        {
            $students =  MFA::newClassQR($id);
            if(request()->ip() != '127.0.0.1') {
                foreach($students AS $student){
                    MailService::send($student->email, 'QRCode', $student);
                }
            }
            return response()->json('Sent QR code successfully');
        }
            return response()->json('Failed to find the user', 400);
    }
    
    public function delete(Request $request)
    {
        $res = Classes::remove($request->id);
        return response()->json($res[0], $res[1]);
    }
    
    public function lessons(Request $request)
    {
        $res = ClassAvailableLesson::new($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function games(Request $request)
    {
        $res = ClassUnavailableGame::new($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Classes::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        $res = Classes::updateStatus($request);
        return response()->json($res[0], $res[1]);
    }
        
    public function getDockers($id)
    {
        Classes::getLiveDockers($id, self::$data);
        return response()->json(self::$data);
    }
     
    public function closeDockers($id, Request $request)
    {
        $res = Classes::closeDocker($request, $id);
        return response()->json($res[0], $res[1]);
    }

    public function getStatistics($id)
    {
        Classes::getStatistics($id, self::$data);
        return response()->json(self::$data);
    }

}
