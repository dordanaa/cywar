<?php

namespace App\Http\Controllers\CMS;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSSubscriptionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class SubscriptionController extends Controller
{
    public function index($pagination)
    {
        $data = Subscription::orderBy('name')->paginate($pagination);
        return response()->json($data);
    }
   
    public function edit(Subscription $subscription)
    {
        return response()->json($subscription, 200);
    }
    
    public function create(CMSSubscriptionRequest $request)
    {
        $res = Subscription::createNew($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSSubscriptionRequest $request)
    {
        $res = Subscription::updateCat($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        Subscription::remove($request->id);
        return response()->json('Subscription Deleted Sucessfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Subscription::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Subscription::updateStatus($request);
        return response()->json('Subscription Status Updated Successfully', 200);
    }
}
