<?php

namespace App\Http\Controllers\CMS;

use App\Role;
use App\Classes;
use App\College;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CMSController extends Controller
{
    static public $data = [];

    static public function getData()
    {
        $role = Auth::user()->role->role;
        $data = [];

        if($role == 'admin')
        {
            $data['classes'] = Classes::where('status', 1)->select('id', 'name', 'college_id')->get();
            $data['colleges'] = College::where('status', 1)->select('id', 'name', 'country_id')->get();
            $data['countries'] = Country::where('status', 1)->select('id', 'country')->get();
            $data['roles'] = Role::all();
        }
        else
        {
            $user = Auth::user();
            if($user->country) $data['countries'] = Country::where('status', 1)->where('country', $user->country->country)->select('id', 'country')->get();
            
            $colleges = [];
            foreach($user->colleges AS $item){
                $colleges[] = $item->name;
            }

            $data['colleges'] = College::where('status', 1)->whereIn('name', $colleges)->select('id', 'name', 'country_id')->get();

            $data['classes'] = Classes::where('classes.status', 1)
                                      ->join('user_classes AS uc', 'uc.class_id', 'classes.id')
                                      ->where('uc.user_id', $user->id)
                                      ->select('classes.id', 'classes.name', 'classes.college_id')->get();

            $data['roles'] = Role::where('role', 'teacher')->get();
        }

        return $data;

    }

    static public function getDataArray($data)
    {
        $arr['countries'] = [];
        $arr['colleges'] = [];
        $arr['classes'] = [];

        if(isset($data['countries']))
        {
            foreach($data['countries'] AS $item){
                $arr['countries'][] = $item['id'];
            }
        }
        if(isset($data['colleges']))
        {
            foreach($data['colleges'] AS $item){
                $arr['colleges'][] = $item['id'];
            }
        }
        if(isset($data['classes']))
        {
            foreach($data['classes'] AS $item){
                $arr['classes'][] = $item['id'];
            }
        }

        return $arr;
    }

    static public function getClasses()
    {
        $role = Auth::user()->role->role;
        $data = [];

        if($role == 'admin')
        {
            $data['classes'] = Classes::where('status', 1)->get();
        }
        else
        {
            $data['classes'] = Classes::where('classes.status', 1)
                                      ->join('user_classes AS uc', 'uc.class_id', 'classes.id')
                                      ->where('uc.user_id', Auth::user()->id)
                                      ->get(['classes.*']);
        }

        return $data;
    }

    static public function getColleges()
    {
        $role = Auth::user()->role->role;
        $data = [];

        if($role == 'admin')
        {
            $data = College::where('status', 1)->get();
        }
        else
        {
            $data = Classes::join('user_classes AS uc', 'uc.class_id', 'classes.id')
                                       ->join('colleges AS c', 'c.id', 'classes.college_id')
                                       ->where('uc.user_id', Auth::user()->id)
                                       ->get(['c.*']);
        }

        return $data;
    }

    static public function getCountries()
    {
        $role = Auth::user()->role->role;
        $data = [];

        if($role == 'admin')
        {
            $data = Country::where('status', 1)->get();
        }
        else
        {
            $data['countries'] = Classes::join('user_classes AS uc', 'uc.class_id', 'classes.id')
                                        ->join('colleges AS co', 'co.id', 'classes.college_id')
                                        ->join('countries AS c', 'c.id', 'co.country_id')
                                        ->where('uc.user_id', Auth::user()->id)
                                        ->get(['c.*']);
        }
        
        return $data;
    } 

    static public function classesId()
    {
        $data = [];
        $classes = self::getClasses();
        foreach($classes['classes'] AS $class){
            $data[] = $class->id;
        }
        return $data;
    }
    
}
