<?php

namespace App\Http\Controllers\CMS;

use App\Cookie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSPolicyRequest;

class CookieController extends Controller
{
    public function index($pagination)
    {
        $data = Cookie::orderBy('updated_at', 'desc')->paginate($pagination);
        return response()->json($data);
    }
    
    public function show(Cookie $cookie)
    {
        if(! $cookie) return response()->json('Sorrry, Cookie Not Found', 404);
        return response()->json($cookie);
    }
 
    public function create(CMSPolicyRequest $request)
    {
        Cookie::createNew($request);
        return response()->json('Cookie Created Successfully', 201);
    }
 
    public function delete(Request $request)
    {
        $cookie = Cookie::find($request->id);
        if($cookie){
            $cookie->delete();
            return response()->json('Cookie Deleted Successfully', 201);
        }
            return response()->json('Cookie Not Found', 404);
    }
 
    public function update(CMSPolicyRequest $request)
    {
        Cookie::updateCookie($request);
        return response()->json('Cookie Updated Successfully', 200);
    }
}
