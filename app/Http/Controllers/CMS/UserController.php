<?php

namespace App\Http\Controllers\CMS;

use App\MFA;
use App\Role;
use App\User;
use App\UserClass;
use App\UserClassesLog;
use App\UserAllowedGame;
use Illuminate\Http\Request;
use App\Http\Requests\JustRequest;
use App\Services\Mail\MailService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CMSUpdateMFARequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\CMSUserCreateRequest;
use App\Http\Requests\CMSUserUpdateRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSUpdatePasswordRequest;

class UserController extends CMSController
{
    public function index($status, $pagination, Request $request)
    {
        $all = self::getClasses();
        $arr = self::getDataArray($all);
        $students = User::getUsers($status, $pagination, $arr, $request);
        return response()->json($students);
    }
 
    public function mutiple(MultipleActionRequest $request)
    {
        $res = User::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }

    public function create(CMSUserCreateRequest $request)
    {
        $res = User::createUser($request);
        return response()->json($res[0], $res[1]);
    }

    public function show($id)
    {
        if(!is_numeric($id)) return response()->json('The id must be a number', 400);
        $res = User::showUser($id);
        return response()->json($res[0], $res[1]);
    }

    public function profile($id)
    {
        if($id != Auth::user()->id) return response()->json('Page Unavailable', 404);
        $res = User::showUser(Auth::user()->id);
        return response()->json($res[0], $res[1]);
    }

    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        $res = User::updateStatus($request);
        return response()->json($res[0], $res[1]);
    }

    public function updateProfile(CMSUserUpdateRequest $request)
    {
        $res = User::updateProfile($request);
        return response()->json($res[0], $res[1]);
    }

    public function updateClass(Request $request)
    {
        $res = UserClass::updateMultipleClasses($request);
        return response()->json($res[0], $res[1]);
    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);
        if(User::checkAuthorization($user, Auth::user()->role->role))
        {
            User::fullDelete($request->id);
            return response()->json('User Deleted Successfully', 200);
        }
            return response()->json('Cannot Delete User Not Under Your Authorization', 200);
    }

    public function gamesAllowed(Request $request)
    {
        $res = UserAllowedGame::new($request);
        return response()->json($res[0], $res[1]);
    }

    public function roles()
    {
        $role = Auth::user()->role->role;
        $roles = [];
        if($role == 'admin'){
            $roles = Role::all();
        }else if($role == 'master'){
            $roles = Role::whereIn('role', ['teacher', 'student'])->get();
        }
        return response()->json($roles);
    }

    public function updatePassword(CMSUpdatePasswordRequest $request)
    {
        $res = User::updatePassword($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateMFA(CMSUpdateMFARequest $request)
    {
        $res = MFA::updateMFA($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function newQRCode($id)
    {
        if(is_numeric($id) && $user = User::where('id', $id)->first(['id', 'email', 'name', 'google2fa_secret']) )
        {
            $user->token = MFA::newQR($user);
            MailService::send($user->email, 'QRCode', $user);
            return response()->json('Sent QR code successfully');
        }
            return response()->json('Failed to find the user', 400);
    }

    public function createJcUser(JustRequest $request)
    {
        if($request->token === ')8^v`r?cxrDv!e.pa!Tl%^%a:NN[t9') {
            $reponse = User::createJcCandidate($request);
            return $reponse;
         } else {
             return response('Not Authorized',401);
         }
    }

    public function getStatistics($id)
    {
        User::getStatistics($id, self::$data);
        return response()->json(self::$data);
    }
    
    public function getDockers($id)
    {
        User::getLiveDockers($id, self::$data);
        return response()->json(self::$data);
    }
     
    public function closeDockers($id, Request $request)
    {
        $res = User::closeDocker($request, $id);
        return response()->json($res[0], $res[1]);
    }
      
    public function classesHistory($id)
    {
        UserClassesLog::getClassesHistory($id, self::$data);
        return response()->json(self::$data);
    }
}
