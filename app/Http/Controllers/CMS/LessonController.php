<?php

namespace App\Http\Controllers\CMS;

use App\Point;
use App\Lesson;
use App\Module;
use App\Mission;
use App\LessonContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSLessonRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSLessonMissionsRequest;

class LessonController extends Controller
{
    public function index($pagination, Request $request)
    {
        Lesson::getAll($pagination, $request, $data);
        return response()->json($data);
    }
    
    public function edit(Lesson $lesson)
    {
        $data = [];
        $data['lesson'] = $lesson->load('cms_missions');
        $data['lessons'] = Lesson::where('module_id', $lesson->module_id)->count();
        $data['module'] = Module::find($lesson->module_id);
        $data['points'] = Point::all();
        return response()->json($data, 200);
    }

    public function new($id)
    {
        $data = [];
        $data['lessons'] = Lesson::where('module_id', $id)->count();
        $data['module'] = Module::find($id);
        $data['points'] = Point::all();
        return response()->json($data, 200);

    }
    
    public function create(CMSLessonRequest $request)
    {
        Lesson::createNew($request);
        return response()->json('Lesson Created Successfully', 201);
    }
    
    public function update(CMSLessonRequest $request)
    {
        Lesson::updateLesson($request);
        return response()->json('Lesson Updated Successfully', 200);
    }

    public function updateMissions(CMSLessonMissionsRequest $request)
    {
        Mission::updateMissions($request->missions, $request->id);
        return response()->json('Lesson Missions Updated Successfully');
    }
    
    public function delete(Request $request)
    {
        Lesson::remove($request->id);
        return response()->json('Lesson Deleted Successfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Lesson::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Lesson::updateStatus($request);
        return response()->json('Lesson Status Updated Successfully', 200);
    }

    public function getContainers($id)
    {
        LessonContainer::getLessonContainers($id, $data);
        return response()->json($data);
    }
        
    public function closeContainer(Request $request)
    {
        LessonContainer::where('id', $request->id)->delete();
        return response()->json('Container Closed Sucessfully', 200);
    }
        
}
