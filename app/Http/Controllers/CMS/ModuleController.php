<?php

namespace App\Http\Controllers\CMS;

use App\Lesson;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSModuleRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class ModuleController extends Controller
{
    public function index($pagination, Request $request)
    {
        Module::getAllCMS($pagination, $request, $data);
        return response()->json($data);
    }
    
    public function edit(Module $module)
    {
        return response()->json($module, 200);
    }
    
    public function create(CMSModuleRequest $request)
    {
        $res = Module::createNew($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSModuleRequest $request)
    {
        $res = Module::updateMod($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        Module::remove($request->id);
        return response()->json('Module Deleted Sucessfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Module::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Module::updateStatus($request);
        return response()->json('Module Status Updated Successfully', 200);
    }
        
    public function lessons($id, $pagination)
    {
        $data = Lesson::where('module_id', $id)->orderBy('number')->with('tasks')->withCount('missions')->paginate($pagination);
        return response()->json($data, 200);
    }
}
