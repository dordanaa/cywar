<?php

namespace App\Http\Controllers\CMS;

use App\CyberpediaCategory;
use Illuminate\Http\Request;
use App\Http\Requests\CMSCyberpediaCategoryRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Controllers\Controller;

class CyberpediaCategoryController extends Controller
{
    public function index($pagination, Request $request)
    {
        CyberpediaCategory::getAll($pagination, $request, $data);
        return response()->json($data);
    }
    
    public function edit(CyberpediaCategory $category)
    {
        return response()->json($category, 200);
    }
    
    public function create(CMSCyberpediaCategoryRequest $request)
    {
        CyberpediaCategory::createNew($request);
        return response()->json('Category Created Successfully', 201);
    }
    
    public function update(CMSCyberpediaCategoryRequest $request)
    {
        CyberpediaCategory::updateCategory($request);
        return response()->json('Category Updated Successfully', 200);
    }
    
    public function delete(Request $request)
    {
        CyberpediaCategory::remove($request->id);
        return response()->json('Category Deleted Successfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = CyberpediaCategory::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        CyberpediaCategory::updateStatus($request);
        return response()->json('Category Status Updated Successfully', 200);
    }
}
