<?php

namespace App\Http\Controllers\CMS;

use App\Game;
use App\User;
use App\Search;
use App\Attempt;
use App\Country;
use App\UserLog;
use App\Category;
use App\Mail\MonthlyReport;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\CMS\CMSController;

class DashboardController extends CMSController
{
    public function basics()
    {
        $data = [];
        $data[] = [ // *********************** GAMES WON
            'title' => 'Challenges Won',
            'icon' => 'emoji_events',
            'color' => 'green lighten-1',
            'func' => 'won',
            'total' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->count(),
            'today' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->where('attempts.created_at', NOW())->count(),
            'week' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->where('attempts.created_at', '>=', Carbon::now()->startOfWeek())->count(),
            'month' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count(),
        ];
        $data[] = [ // *********************** GAMES PlAYED
            'title' => 'Challenges Played',
            'icon' => 'videogame_asset',
            'color' => 'blue lighten-1',
            'func' => 'played',
            'total' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->count(),
            'today' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.created_at', NOW())->count(),
            'week' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.created_at', '>=', Carbon::now()->startOfWeek())->count(),
            'month' => Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count(),
        ];
        $data[] = [ // *********************** STUDENTS LOGINS
            'title' => 'Students Logins',
            'icon' => 'supervised_user_circle',
            'color' => 'orange lighten-1',
            'func' => 'logins',
            'total' => UserLog::join('users', 'users.id', 'user_logs.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->count(),
            'today' => UserLog::join('users', 'users.id', 'user_logs.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('user_logs.created_at', NOW())->count(),
            'week' => UserLog::join('users', 'users.id', 'user_logs.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('user_logs.created_at', '>=', Carbon::now()->startOfWeek())->count(),
            'month' => UserLog::join('users', 'users.id', 'user_logs.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('user_logs.created_at', '>=', Carbon::now()->startOfMonth())->count(),
        ];
        $data[] = [ // *********************** STUDENTS SIGNUPS
            'title' => 'Students Signups',
            'icon' => 'group_add',
            'color' => 'deep-purple lighten-2',
            'func' => 'sigups',
            'total' => User::join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->count(),
            'today' => User::join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('users.created_at', NOW())->count(),
            'week' => User::join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('users.created_at', '>=', Carbon::now()->startOfWeek())->count(),
            'month' => User::join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('users.created_at', '>=', Carbon::now()->startOfMonth())->count(),
        ];
        return $data;
    }

    static public function games()
    {
        $data = Category::where('status', 1)->withCount('games')->get(['id', 'name']);
        return $data;
    }

    public function populars()
    {
        $data = Game::where('status', 1)
                    ->withCount('attempts')
                    ->orderBy('attempts_count', 'desc')
                    ->limit(5)->get(['id', 'name']);
        return $data;
    }

    public function hardest()
    {
        $games = Game::where('status', 1)
                    ->withCount('attempts', 'wins')
                    ->get();
        $data = [];
        foreach($games AS $game)
        {
            if($game->attempts_count > 25)
            {
                $game->success = round((100 * $game->wins_count) / $game->attempts_count, 1);
                $data[] = $game;
            }
        }
        return $data;
    }

    static public function students()
    {
        $data = Country::where('status', 1)->with('users')->get(['id', 'country']);
        foreach($data AS $country)
        {
            $country->students_count = 0;
            foreach($country->users AS $user)
            {
                if($user->role == 'student')
                {
                    $country->students_count++;
                }
                $user->role = '';
            }
        }
        return $data;
    }

    
    // ****
    // NOT WORKING FOR NOW
    // ****
    // public function fullSearch($cat, $search)
    // {
    //     Search::fullSearch($cat, $search, $data);
    //     return response()->json($data);
    // }

    static public function generateReport()
    {
        // $emails = ['gala@hackerupro.co.il', 'ilanm@hackeru.co.il', 'alexb@hackerupro.co.il', 'lion@hackerupro.co.il', 'aric@hackerupro.co.il'];
        $emails = ['gala@hackerupro.co.il'];
        $data = [];
        $now = now();
        $data['date'] = "$now->month/$now->year";
        $data['students'] = self::students();
        $data['cats'] = self::games();
        $data['games'] = Game::count();
        $data['games_added'] = Game::where('created_at', '>', Carbon::now()->startOfMonth())->count();
        $data['games_played'] = Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count();
        $data['games_finished'] = Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count();

        Mail::to($emails)->send(new MonthlyReport($data));
        return true;
    }

}
