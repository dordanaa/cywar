<?php

namespace App\Http\Controllers\CMS;

use App\College;
use App\CollegeType;
use Illuminate\Http\Request;
use App\Http\Requests\CMSCollegeRequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSCollegeCreateRequest;

class BusinessController extends CMSController
{
    public function index($pagination)
    {
        $cats = College::where('college_type_id', 2)->with('country', 'students')->withCount('classes')->paginate($pagination);
        return response()->json($cats);
    }
    
    public function show(College $college)
    {
        $data['college'] = $college;
        $data['countries'] = self::getCountries();
        $data['types'] = CollegeType::where('status', 1)->get();
        return response()->json($data, 200);
    }
    
    public function classes($id, $pagination)
    {
        College::getClasses($id, $pagination, $data);
        return response()->json($data, 200);
    }

    public function new()
    {
        $data['countries'] = self::getCountries();
        $data['types'] = CollegeType::where('status', 1)->get();
        return response()->json($data, 200);
    }
 
    public function create(CMSCollegeCreateRequest $request)
    {
        College::createNew(2, $request);
        return response()->json('College Created Successfully', 201);
    }
    
    public function update(CMSCollegeRequest $request)
    {
        College::updateCollege(2, $request);
        return response()->json('College Updated Successfully', 200);
    }
    
    public function delete(Request $request)
    {
        $res = College::remove($request->id);
        return response()->json($res[0], $res[1]);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = College::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        College::updateStatus($request);
        return response()->json('College Status Updated Successfully', 200);
    }
}
