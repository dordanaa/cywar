<?php

namespace App\Http\Controllers\CMS;

use App\Avatar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMS\Avatar\CMSAvatarCreateRequest;
use App\Http\Requests\CMS\Avatar\CMSAvatarUpdateRequest;

class AvatarController extends Controller
{
    public function index($pagination)
    {
        $cats = Avatar::paginate($pagination);
        return response()->json($cats);
    }

    public function classes($id, $pagination)
    {
        $data = null;
        Avatar::getClasses($id, $pagination, $data);
        return response()->json($data, 200);
    }
 
    public function create(CMSAvatarCreateRequest $request)
    {
        $res = Avatar::createNew($request);
        return response()->json($res, 201);
    }
    
    public function update(CMSAvatarUpdateRequest $request)
    {
        Avatar::updateAvatar($request);
        return response()->json('Avatar Updated Successfully', 200);
    }
    
    public function delete(Request $request)
    {
        Avatar::where('id', $request->id)->delete();
        return response()->json('Avatar Deleted Successfully', 200);
    }

    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Avatar::updateStatus($request);
        return response()->json('Avatar Status Updated Successfully', 200);
    }
}
