<?php

namespace App\Http\Controllers\CMS;

use App\Level;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSLevelCreateRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class LevelController extends Controller
{
    public function index($pagination)
    {
        $cats = Level::paginate($pagination);
        return response()->json($cats);
    }
    
    public function show(Level $level)
    {
        return response()->json($level, 200);
    }
    
    public function classes($id, $pagination)
    {
        $data = null;
        Level::getClasses($id, $pagination, $data);
        return response()->json($data, 200);
    }

    public function new()
    {
        $data = [];
        $data['countries'] = self::getCountries();
        return response()->json($data, 200);
    }
 
    public function create(CMSLevelCreateRequest $request)
    {
        $res = Level::createNew($request);
        return response()->json($res, 201);
    }
    
    public function update(CMSLevelCreateRequest $request)
    {
        Level::updateLevel($request);
        return response()->json('Level Updated Successfully', 200);
    }
    
    public function delete(Request $request)
    {
        $res = Level::remove($request->id);
        return response()->json($res[0], $res[1]);
    }

    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Level::updateStatus($request);
        return response()->json('Level Status Updated Successfully', 200);
    }
}
