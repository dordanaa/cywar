<?php

namespace App\Http\Controllers\CMS;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSTagRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class TagsController extends Controller
{
    public function index($pagination, Request $request)
    {
        Tag::getAll($pagination, $request, $data);
        return response()->json($data);
    }
   
    public function edit(Tag $tag)
    {
        return response()->json($tag, 200);
    }
    
    public function create(CMSTagRequest $request)
    {
        $res = Tag::createNew($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSTagRequest $request)
    {
        $res = Tag::updateTag($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        Tag::remove($request->id);
        return response()->json('Category Deleted Sucessfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Tag::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Tag::updateStatus($request);
        return response()->json('Category Status Updated Successfully', 200);
    }
}
