<?php

namespace App\Http\Controllers\CMS;

use App\Support;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class SupportController extends CMSController
{
    public function count()
    {
        Support::count(self::$data, self::classesId());
        return response()->json(self::$data);
    }
    
    public function index(Request $request)
    {
        Support::getAll(self::$data, $request->pagination, $request->status, self::classesId());
        return response()->json(self::$data);
    }

    public function mutiple(MultipleActionRequest $request)
    {
        $res = Support::MultipleAction(self::classesId(), $request);
        return response()->json($res[0], $res[1]);
    }

    public function delete(Request $request)
    {
        $res = Support::remove(self::classesId(), $request->id);
        return response()->json($res[0], $res[1]);
    }

    public function status(CMSUpdateStatusRequest $request)
    {
        $res = Support::updateStatus(self::classesId(), $request);
        return response()->json($res[0], $res[1]);
    }

}
