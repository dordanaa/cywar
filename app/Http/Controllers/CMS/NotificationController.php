<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NotificationType;

class NotificationController extends Controller
{
    public function index($pagination)
    {
        $cats = NotificationType::paginate($pagination);
        return response()->json($cats);
    }
    
    public function new()
    {
        $data = [];
        $data['types'] = NotificationType::all();
        return response()->json($data);
    }

    public function create(CMSNotificationRequest $request)
    {
        $valid = in_array($request->color, ['white','red','blue','green','yellow','purple','pink','orange','teal']);
        if(!$valid) return response()->json('Color is not available',400);
        NotificationType::createNew($request);
        return response()->json('Notification Type Created Successfully', 201);
    }

    public function update(CMSNotificationRequest $request)
    {
        $valid = in_array($request->color, ['white','red','blue','green','yellow','purple','pink','orange','teal']);
        if(!$valid) return response()->json('Color is not available',400);
        NotificationType::updateType($request);
        return response()->json('Notification Type Created Successfully', 201);
    }

    public function mutiple(MultipleActionRequest $request)
    {
        $res = NotificationType::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }

    public function delete(Request $request)
    {
        $res = NotificationType::remove($request->id);
        return response()->json($res[0], $res[1]);
    }

    public function edit($id)
    {
        $type = NotificationType::find($id);
        return response()->json($type);
    }

    public function status(CMSUpdateStatusRequest $request)
    {
        NotificationType::updateStatus($request);
        return response()->json('Notification Type Status Updated Successfully', 200);
    }
}
