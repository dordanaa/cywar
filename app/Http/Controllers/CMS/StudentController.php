<?php

namespace App\Http\Controllers\CMS;

use App\MFA;
use App\User;
use App\Attempt;
use App\UserClass;
use App\UserClassesLog;
use App\UserAvailableLesson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\GameLockReqeust;
use App\Http\Requests\CMSUpdateMFARequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\CMSUserUpdateRequest;
use App\Http\Requests\FilterStudentRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSUpdatePasswordRequest;
use App\Services\Mail\MailService;

class StudentController extends CMSController
{
    public function index($status, $pagination, Request $request)
    {
        $all = self::getClasses();
        $arr = self::getDataArray($all);
        User::getStudents($status, $arr, $students, $pagination, $request);
        return response()->json($students);
    }
   
    public function filter(FilterStudentRequest $request)
    {
        $all = self::getData();
        $arr = self::getDataArray($all);
        User::filter($request, $arr, $data);
        return response()->json($data);
    }

    public function all(Request $request)
    {
        $data = [];
        $all = self::getData();
        $arr = self::getDataArray($all);
        User::getStudents('all', $arr, $data['students'], 10, $request);
        $data['countries'] = isset($all['countries']) ? $all['countries'] : [];
        $data['colleges'] = isset($all['colleges']) ? $all['colleges'] : [];
        $data['classes'] = isset($all['classes']) ? $all['classes'] : [];
   
        return response()->json($data);
    }

    public function mutiple(MultipleActionRequest $request)
    {
        $res = User::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }

    public function show($id)
    {
        if(!is_numeric($id)) return response()->json('The id must be a number', 400);
        $res = User::showStudent($id);
        return response()->json($res[0], $res[1]);
    }

    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        $res = User::updateStatus($request);
        return response()->json($res[0], $res[1]);
    }

    public function updateProfile(CMSUserUpdateRequest $request)
    {
        $res = User::updateProfile($request);
        return response()->json($res[0], $res[1]);
    }

    public function updateClass(Request $request)
    {
        $res = UserClass::updateClass($request);
        return response()->json($res[0], $res[1]);
    }

    public function removeClass(Request $request)
    {
        $res = UserClass::removeClass($request->id);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        $user = User::find($request->id);
        if(User::checkAuthorization($user, Auth::user()->role->role))
        {
            User::fullDelete($request->id);
            return response()->json('Student Deleted Successfully', 200);
        }
            return response()->json('Cannot Delete Student Not Under Your Authorization', 200);
    }
    
    public function updatePassword(CMSUpdatePasswordRequest $request)
    {
        $res = User::updatePassword($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateMFA(CMSUpdateMFARequest $request)
    {
        $res = MFA::updateMFA($request);
        return response()->json($res[0], $res[1]);
    }

    public function newQRCode($id)
    {
        if(is_numeric($id) && $user = User::where('id', $id)->first(['id', 'email', 'name', 'google2fa_secret']) )
        {
            $user->token = MFA::newQR($user);
            MailService::send($user->email, 'QRCode', $user);
            return response()->json('Sent QR code successfully');
        }
            return response()->json('Failed to find the user', 400);
    }
    
    public function updateLessons(Request $request)
    {
        $res = UserAvailableLesson::new($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function getDockers($id)
    {
        User::getLiveDockers($id, self::$data);
        return response()->json(self::$data);
    }
    
    public function getStatistics($id)
    {
        User::getStatistics($id, self::$data);
        return response()->json(self::$data);
    }
    
    public function closeDockers($id, Request $request)
    {
        $res = User::closeDocker($request, $id);
        return response()->json($res[0], $res[1]);
    }
    
    public function classesHistory($id)
    {
        UserClassesLog::getClassesHistory($id, self::$data);
        return response()->json(self::$data);
    }
    
    public function gameLocked(GameLockReqeust $request)
    {
        Attempt::updateLock($request->status, $request->user_id, $request->game_id);
        return response()->json(self::$data);
    }
    
    public function resetActivities(Request $request)
    {
        $res = User::resetActivities($request);
        return response()->json($res[0], $res[1]);
    }
}
