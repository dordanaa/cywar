<?php

namespace App\Http\Controllers\CMS;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSCountryRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\CMSCountryCreateRequest;

class CountryController extends Controller
{
    public function index($pagination)
    {
        $cats = Country::withCount('colleges')->with('classes', 'students')->paginate($pagination);
        return response()->json($cats);
    }
    
    public function show(Country $country)
    {
        return response()->json($country, 200);
    }
    
    public function colleges($id, $pagination)
    {
        $data = null;
        Country::getColleges($id, $pagination, $data);
        return response()->json($data, 200);
    }

    public function new()
    {
        $data = [];
        $data['countries'] = self::getCountries();
        return response()->json($data, 200);
    }
 
    public function create(CMSCountryCreateRequest $request)
    {
        Country::createNew($request);
        return response()->json('Country Created Successfully', 201);
    }
    
    public function update(CMSCountryRequest $request)
    {
        Country::updateCountry($request);
        return response()->json('Country Updated Successfully', 200);
    }
    
    public function delete(Request $request)
    {
        $res = Country::remove($request->id);
        return response()->json($res[0], $res[1]);
    }

    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Country::updateStatus($request);
        return response()->json('Country Status Updated Successfully', 200);
    }
}
