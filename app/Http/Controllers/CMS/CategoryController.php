<?php

namespace App\Http\Controllers\CMS;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CMSCategoryRequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class CategoryController extends CMSController
{
    public function index($pagination, Request $request)
    {
        Category::getAll($pagination, $request, $data);
        return response()->json($data);
    }
   
    public function edit(Category $category)
    {
        return response()->json($category, 200);
    }
    
    public function create(CMSCategoryRequest $request)
    {
        $res = Category::createNew($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSCategoryRequest $request)
    {
        $res = Category::updateCat($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        Category::remove($request->id);
        return response()->json('Category Deleted Sucessfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Category::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Category::updateStatus($request);
        return response()->json('Category Status Updated Successfully', 200);
    }
}
