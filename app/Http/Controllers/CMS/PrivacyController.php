<?php

namespace App\Http\Controllers\CMS;

use App\Privacy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMSPolicyRequest;

class PrivacyController extends Controller
{
    public function index($pagination)
    {
        $data = Privacy::orderBy('updated_at', 'desc')->paginate($pagination);
        return response()->json($data);
    }
    
    public function show(Privacy $privacy)
    {
        if(! $privacy) return response()->json('Sorrry, Privacy Policy Not Found', 404);
        return response()->json($privacy);
    }
 
    public function create(CMSPolicyRequest $request)
    {
        Privacy::createNew($request);
        return response()->json('Privacy Policy Created Successfully', 201);
    }
 
    public function delete(Request $request)
    {
        $privacy = Privacy::find($request->id);
        if($privacy){
            $privacy->delete();
            return response()->json('Privacy Policy Deleted Successfully', 201);
        }
            return response()->json('Privacy Policy Not Found', 404);
    }
 
    public function update(CMSPolicyRequest $request)
    {
        Privacy::updatePrivacy($request);
        return response()->json('Privacy Policy Updated Successfully', 200);
    }
}
