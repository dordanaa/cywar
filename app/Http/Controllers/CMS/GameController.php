<?php

namespace App\Http\Controllers\CMS;

use App\Game;
use App\GameFile;
use App\Container;
use Illuminate\Http\Request;
use App\Http\Requests\CMSGameRequest;
use App\Http\Requests\CMSGameFileRequest;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\CMSGameUpdateRequest;
use App\Http\Requests\CMSUpdateFileRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;

class GameController extends CMSController
{
    public function index($status, $pagination, Request $request)
    {
        Game::getAll($status, $pagination, $request, $data);
        return response()->json($data);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = Game::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSGameUpdateRequest $request)
    {
        Game::updateGame($request);
        return response()->json('Game updated Successfully', 200);
    }

    public function new()
    {
        Game::editGame(null, $res);    
        return response()->json($res, 200);
    }

    public function create(CMSGameRequest $request)
    {
        $res = Game::createGame($request);
        return response()->json($res[0], $res[1]);
    }

    public function show($id)
    {
        if(!is_numeric($id)) return response()->json('The id must be a number', 400);
        $res = Game::showGame($id);    
        return response()->json($res[0], $res[1]);
    }

    public function edit($id)
    {
        if(!is_numeric($id)) return response()->json('The id must be a number', 400);
        Game::editGame($id, $res);
        if(!$res['game']) return response()->json('Game Not Found', 400);    
        return response()->json($res, 200);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        Game::updateStatus($request);
        return response()->json('Game Status Updated Successfully', 200);
    }
    
    public function image(CMSUpdateFileRequest $request)
    {
        $res = Game::updateImage($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function guide(CMSUpdateFileRequest $request)
    {
        $res = Game::updateGuide($request);
        return response()->json($res, 200);
    }
    
    public function file(CMSGameFileRequest $request)
    {
        $res = GameFile::addFiles($request);
        return response()->json($res);
    }
    
    public function deleteFile(Request $request)
    {
        GameFile::remove($request->id);
        return response()->json('Game File Deleted Sucessfully', 200);
    }
    
    public function delete(Request $request)
    {
        Game::remove($request->id);
        return response()->json('Game Deleted Sucessfully', 200);
    }

    public function closeDocker(Request $request)
    {
        Container::where('game_id', $request->game_id)->where('port', $request->port)->delete();
        return response()->json('Container Closed Sucessfully', 200);
    }

}
