<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class GeneralController extends Controller
{
    public function autocomplete(Request $request)
    {
        $parts = explode(' ', $request->model);
        $model = count($parts) > 1 ? $parts[1] : $request->model;
        if($model == 'Students') {
            $model = 'users';
            $roles = [33, 149]; // student & candidate
        }
        if($model == 'Users') $roles = [58, 67, 134]; // teacher & master & admin

        if($model == 'Terms') $model = 'cyberpedia_terms'; 

        // Country table has country column, not name
        $search = $model == 'Countries' ? 'country' : 'name';
        $model = strtolower($model);

        // set the table
        $data = DB::table($model);
        // check by the status
        if(in_array($request->status, ['0', '1', '2'])) $data = $data->where("$model.status", $request->status);

        // filter by roles ( only for users table )
        if(isset($roles)) $data = $data->join('user_roles AS ur', 'ur.user_id', 'users.id')->whereIn('role_id', $roles);

        // search by the value
        if(isset($roles))
        {
            $data = $data->where(function ($q) use($request, $model, $search){
                $q->where("$model.$search", 'LIKE', "%$request->search%")
                  ->orWhere("email", 'LIKE', "%$request->search%")
                  ->orWhere("phone", 'LIKE', "%$request->search%");
            })->limit(10)->get(["users.id", "users.name", 'users.email', 'users.phone']);
        }
        else
        {
            $data = $data->where("$model.$search", 'LIKE', "%$request->search%")->limit(10)->get(["$model.id", "$model.$search"]);
        }

        // search by name / email / phone ( only for users table )

        return response()->json($data);
    }
}
