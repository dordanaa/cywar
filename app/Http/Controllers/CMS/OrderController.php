<?php

namespace App\Http\Controllers\CMS;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Controllers\PaypalController;

class OrderController extends CMSController
{
    public function index(Request $request)
    {
        Order::getAll($request, self::$data);
        return response()->json(self::$data);
    }
    
    public function show($id)
    {
        $order = Order::find($id);
        $agreement = PaypalController::getAgreement($order->order_id);
        return response()->json($agreement);
    }
}
