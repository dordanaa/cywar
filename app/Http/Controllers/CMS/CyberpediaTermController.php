<?php

namespace App\Http\Controllers\CMS;

use App\CyberpediaCategory;
use App\CyberpediaTerm;
use Illuminate\Http\Request;
use App\Http\Requests\CMSCyberpediaTermRequest;
use App\Http\Requests\MultipleActionRequest;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Controllers\Controller;

class CyberpediaTermController extends Controller
{
    public function index($pagination, Request $request)
    {
        CyberpediaTerm::getAll($pagination, $request, $data);
        return response()->json($data);
    }
    
    public function edit($id)
    {
        $data['cats'] = CyberpediaCategory::where('status', 1)->get(['id', 'name']);
        $data['term'] = CyberpediaTerm::find($id);
        return response()->json($data, 200);
    }
    
    public function new()
    {
        $data = CyberpediaCategory::where('status', 1)->get(['id', 'name']);
        return response()->json($data);
    }
    
    public function create(CMSCyberpediaTermRequest $request)
    {
        $res = CyberpediaTerm::createNew($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function update(CMSCyberpediaTermRequest $request)
    {
        $res = CyberpediaTerm::updateTerms($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function delete(Request $request)
    {
        CyberpediaTerm::remove($request->id);
        return response()->json('Term Deleted Successfully', 200);
    }
    
    public function mutiple(MultipleActionRequest $request)
    {
        $res = CyberpediaTerm::MultipleAction($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function updateStatus(CMSUpdateStatusRequest $request)
    {
        CyberpediaTerm::updateStatus($request);
        return response()->json('Category Status Updated Successfully', 200);
    }
}
