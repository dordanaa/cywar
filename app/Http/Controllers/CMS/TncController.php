<?php

namespace App\Http\Controllers\CMS;

use App\Tnc;
use Illuminate\Http\Request;
use App\Http\Controllers\CMS\CMSController;
use App\Http\Requests\CMSPolicyRequest;

class TncController extends CMSController
{
    public function index($pagination)
    {
        $data = Tnc::orderBy('updated_at', 'desc')->paginate($pagination);
        return response()->json($data);
    }
    
    public function show(Tnc $tnc)
    {
        if(! $tnc) return response()->json('Sorrry, TNC Not Found', 404);
        return response()->json($tnc);
    }
 
    public function create(CMSPolicyRequest $request)
    {
        Tnc::createNew($request);
        return response()->json('TNC Created Successfully', 201);
    }
 
    public function delete(Request $request)
    {
        $tnc = Tnc::find($request->id);
        if($tnc){
            $tnc->delete();
            return response()->json('TNC Deleted Successfully', 201);
        }
            return response()->json('TNC Not Found', 404);
    }
 
    public function update(CMSPolicyRequest $request)
    {
        Tnc::updateTnc($request);
        return response()->json('TNC Updated Successfully', 200);
    }
}
