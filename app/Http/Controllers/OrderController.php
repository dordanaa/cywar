<?php

namespace App\Http\Controllers;

use App\Services\Payment\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function setOrder(Request $request)
    {
        request()->service = 'PayPal';
        $response = OrderService::create();
        return response()->json($response);
    }
}
