<?php

namespace App\Http\Controllers;

use App\Support;
use App\SupportCategory;
use App\Http\Requests\SupportRequest;
use App\Services\MondayAPI\CreateMondayTicket;

class SupportController extends Controller
{
    public function index()
    {
        $cats = SupportCategory::where('status', 1)->get(['id', 'name']);
        return response()->json($cats);
    }

    public function create(SupportRequest $request)
    {
        $res = Support::new($request);
        return response()->json($res[0], $res[1]);
        // new CreateMondayTicket($request->input());
    }
}
