<?php

namespace App\Http\Controllers;

use App\User;
use App\UserLog;
use Illuminate\Http\Request;

class APIStudentController extends Controller
{
    public function index($token, Request $request)
    {   
        $our_password = 'ABy56e$RlL#';
        if (Self::hashDecrypted($token, 'd') == $our_password) {
            User::createUserFromCampus($request);
            return response()->json($request);
        }
    }
    
    public function updateStatus($token, Request $request)
    {   
        $data = ['email' => $request['email'], 'status' => $request['active']];

        if($data['email'] && is_string($data['email']) && strlen($data['email']) < 255 && $data['status'] & is_numeric($data['status']) && in_array($data['status'], [1, 0])){
            $our_password = 'ABy56e$RlL#';
            if (Self::hashDecrypted($token, 'd') == $our_password) {
                $status = User::updateStatusFromCampus($request);
                return json_encode($status);
            }
        }else{
            return json_encode(false);
        }
    }

    static function hashDecrypted($string, $action = 'e')
    {
        $secret_key = 'sag8_ckm#h3!apr_secret_key';
        $secret_iv = 'sag8_ckm#h3!apr_secret_iv';
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'e') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        } else if ($action == 'd') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}
