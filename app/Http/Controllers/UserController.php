<?php

namespace App\Http\Controllers;

use App\Game;
use App\User;
use App\Avatar;
use App\Level;
use App\Lesson;
use App\Attempt;
use App\LessonAttempt;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    public function profile(){
        $id = Auth::user()->id;
        $data = User::where('id', $id)->withCount('played', 'finished', 'hints')->first();
        $data->games = Game::where('status', 1)->count();
        $data->details = User::details($id);
        $data->pa_details = User::practice_arena_details($id);
        return response()->json($data);
    }
    
    public function details(){
        $id = Auth::user()->id;
        $data = User::where('id', $id)->withCount('played', 'finished', 'hints')->first();
        $data->details = User::details($id);
        $data->levels = Level::where('status', 1)->get(['level', 'points', 'next_level']);
        return response()->json($data);
    }
    
    public function edit(){
        $id = Auth::user()->id;
        $data = [];
        $data['user'] = User::where('id', $id)->first(['name', 'email', 'phone', 'image']);
        $data['user']->details = User::details($id);
        $data['avatars'] = Avatar::where('status', 1)->orderBy('level')->get(['id', 'image', 'level']);
        return response()->json($data);
    }
    
    public function games(){
        $games = Attempt::where('user_id', Auth::user()->id)
                        ->join('games AS g', 'g.id', 'attempts.game_id')
                        ->select('attempts.*','g.name')->get();
        return response()->json($games);
    }
    
    public function practiceArena(){
        $data = [];
        $data['lessons'] = LessonAttempt::where('user_id', Auth::user()->id)
                        ->join('lessons AS s', 's.id', 'lesson_attempts.lesson_id')
                        ->select('lesson_attempts.*','s.name')->get();
        $data['count'] = Lesson::where('status', 1)->count();
        return response()->json($data);
    }

    public function update(UserUpdateRequest $request)
    {
        $res = User::updateUser($request);
        return response()->json($res[0], $res[1]);
    }

}
