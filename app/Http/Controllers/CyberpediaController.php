<?php

namespace App\Http\Controllers;

use App\CyberpediaTerm;
use App\CyberpediaCategory;

class CyberpediaController extends Controller
{
   public function getAllTerms()
   {
      $data = CyberpediaCategory::where('status', 1)->with('validTerms')->get(['id', 'name', 'description', 'image']);
      return response()->json($data);
   }

   public function showTerm(CyberpediaTerm $term)
   {
        return response()->json($term);
   }

   public function search($value)
   {
       CyberpediaTerm::search($data, $value);
       return response()->json($data);
   }
}
