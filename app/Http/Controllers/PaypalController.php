<?php

namespace App\Http\Controllers;


use App\Module;
use App\Category;
use App\PaypalPlan;
use App\Subscription;
use App\PaypalAgreement;
use Illuminate\Http\Request;
use App\Http\Requests\CMSUpdateStatusRequest;
use App\Http\Requests\Plans\CreatePlanRequest;

class PaypalController extends Controller
{
    public function newPlan()
    {
        $data['categories'] = Category::where('status', 1)->with('games')->get();
        $data['practice_arena'] = Module::where('status', 1)->with('lessons')->get();
        return response()->json($data);
    }

    public function createAgreement($id)
    {
        $url = PaypalAgreement::createAgreement($id);
        return $url;
    }   

    public function executeAgreement(Request $request)
    {
        $res = PaypalAgreement::executeAgreement($request->status, $request->token);
        return response()->json($res[0], $res[1]);
    }   
    
    static public function getAgreement($id)
    {
        $agreement = PaypalAgreement::showAgreement($id);
        return $agreement;
    }   
    
    public function updateAgreement($id)
    {
        $agreement = PaypalAgreement::updateAgreement($id);
        return $agreement;
    }   
    
    public function getTransc($id)
    {
        $transcs = PaypalAgreement::showTransc($id);
        return $transcs;
    }   
    
    public function stopAgreement($id)
    {
        $res = PaypalAgreement::stopAgreement($id);
        return $res;
    }   
    
    public function reactiveAgreement($id)
    {
        $res = PaypalAgreement::reactiveAgreement($id);
        return $res;
    }   

    public function createPlan(CreatePlanRequest $request)
    {
        $plan = PaypalPlan::createPlan($request);
        Subscription::createPlan($request, $plan->id);
        return $plan;
    }

    public function updatePlan(CreatePlanRequest $request)
    {
        $plan = PaypalPlan::updatePlan($request);

        if($plan) {
            $res = Subscription::updatePlan($request);
            return response()->json($res[0], $res[1]);
        }
        return response()->json('Unabled to update the Plan');
    }

    public function activatePlan($id)
    {
        $plan = PaypalPlan::activate($id);
        return $plan;
    }

    public function changeStatus(CMSUpdateStatusRequest $request)
    {
        if(in_array($request->status, [0, 1]) && is_string($request->plan_id))
        {
            $sub = Subscription::find($request->id);
            if($sub)
            {
                if($sub->status == 2) $res = PaypalPlan::activate($request->plan_id);
                else $res = PaypalPlan::updatePlanStatus($request->plan_id, $request->status);
                if($res)
                {
                    Subscription::updateStatus($request);
                    return response()->json('Status Updated Successfully');
                }
            }
        }
        return response()->json('Unabled to update the status', 400);
    }

    public function getPlans()
    {
        Subscription::getAll($plans);
        return $plans;
    }

    public function getActivePlans()
    {
        Subscription::getAllActive($plans);
        return $plans;
    }

    public function showPlan($id)
    {
        $sub = Subscription::find($id);
        if($sub)
        {
            return response()->json($sub);
        }
            return response()->json('Plan not found', 400);
    }

    public function deletePlan($id)
    {
        $res = Subscription::remove($id);
        return response()->json($res[0], $res[1]);
    }

    // public function setPayment(Request $request)
    // {
    //     $apiContext = new ApiContext(
    //         new OAuthTokenCredential(
    //             'ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V', // Client ID
    //             'ELqyHPegFdgPXnIawK8oKrK0MjzcobLYUWPfcKYDjSj_pBfXayWRaUmCdVmIIQelf0waFJ9YtepsI4a9' // Client Sercret
    //         )
    //     );

    //     $payer = new Payer();
    //     $payer->setPaymentMethod("paypal");

    //     $subscription = Subscription::find($request->subscription_id);
    //     Auth::user()->update(['subscription_id' => $request->subscription_id]);

    //     $item1 = new Item();
    //     $item1->setName($subscription->name . ' Subscription')
    //         ->setCurrency('USD')
    //         // ->setSku("123123") // Similar to `item_number` in Classic API
    //         ->setQuantity(1)
    //         ->setPrice($subscription->price);

    //     $itemList = new ItemList();
    //     $itemList->setItems(array($item1));

    //     $details = new Details();
    //     // $details->setShipping(1.2)
    //     //         ->setTax(1.3)
    //     //         ->setSubtotal(17.50);

    //     $amount = new Amount();
    //     $amount->setCurrency("USD")
    //         ->setTotal($subscription->price)
    //         ->setDetails($details);

    //     $transaction = new Transaction();
    //     $transaction->setAmount($amount)
    //         ->setItemList($itemList)
    //         ->setDescription("Payment description")
    //         ->setInvoiceNumber(uniqid());

    //     $baseUrl = 'http://127.0.0.1:8000/';
    //     $redirectUrls = new RedirectUrls();
    //     $redirectUrls->setReturnUrl("$baseUrl/payment?success=true")
    //         ->setCancelUrl("$baseUrl/payment?success=false");
        
    //     $payment = new Payment();
    //     $payment->setIntent("sale")
    //         ->setPayer($payer)
    //         ->setRedirectUrls($redirectUrls)
    //         ->setTransactions(array($transaction));
        
    //     $request = clone $payment;

    //     try {
    //         $payment->create($apiContext);
    //     } catch (Exception $ex) {
    //         echo $ex;
    //         exit(1);
    //     }

    //     return $payment;
    // }

    // public function execOrder(Request $request)
    // {
    //     $apiContext = new ApiContext(
    //         new OAuthTokenCredential(
    //             'ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V', // Client ID
    //             'ELqyHPegFdgPXnIawK8oKrK0MjzcobLYUWPfcKYDjSj_pBfXayWRaUmCdVmIIQelf0waFJ9YtepsI4a9' // Client Sercret
    //         )
    //     );
        
    //     $payment = Payment::get($request->paymentID, $apiContext);
        
    //     $execution = new PaymentExecution();
    //     $execution->setPayerId($request->payerID);

    //     try {
    //         $result = $payment->execute($execution, $apiContext);
    //     }catch (Exception $ex){
    //         echo $ex;
    //         exit(1);
    //     }

    //     Order::new($request);

    //     return $result;
    // }
}
