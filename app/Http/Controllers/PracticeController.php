<?php

namespace App\Http\Controllers;

use App\Docker;
use App\Lesson;
use App\Module;
use App\MissionTask;
use App\LessonAttempt;
use App\LessonContainer;
use App\MissionTaskHint;
use App\MissionTaskAttempt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PracticeController extends Controller
{
    public function index()
    {
        if(Auth::user()->role->role == 'candidate') return response()->json('Practice Arena Is Unavailable For You', 400);
        $data = null;
        Module::getAll($data);
        return response()->json($data);
    }

    public function show($id)
    {
        $res = Lesson::showLesson($id);
        return response()->json($res[0], $res[1]);
    }

    public function taskSolution($id)
    {
        MissionTaskHint::new($id);
        $hint = MissionTask::getSolution($id);
        return response()->json($hint);
    }
    
    public function runDocker($id)
    {
        $res = Docker::runPractice($id);
        return response()->json($res[0], $res[1]);
    }
    
    public function testTask(Request $request)
    {
        $res = Docker::testTask($request);
        if(!$res) return response()->json(['status' => 0, 'content' => 'Sorry, we have a problem, please let us know...'], 400);
        if(isset($res->error)) {
            // $emails = ['gala@hackerupro.co.il', 'alexb@hackerupro.co.il', 'nathant@hackerupro.co.il'];
            // Mail::to($emails)->send(new PracticeArena($res->error));
            return response()->json(['status' => 0, 'content' => $res->error], 404);
        }
        MissionTaskAttempt::new($res, $request);
        LessonAttempt::new($request);
        return response()->json($res, $res->status ? 200 : 417);
    }
    
    public function pulse(Request $request)
    {
        $res = Docker::checkPulse($request);
        return response()->json($res[0], $res[1]);
    }
    
    public function downloadFile(Request $request)
    {
        $res = Docker::createRDPFile($request);
        return response()->json($res);
    }
    
    public function reset($id)
    {
        $container = LessonContainer::where('user_id', Auth::user()->id)->where('container_id', $id)->first();
        if($container) {
            $container->delete();
            return response()->json('Container Removed');
        }
        else{
            return response()->json('Container Not Found', 404);
        }
    }

}
