<?php

namespace App\Http\Controllers;

use App\Services\Mail\MailService;
use App\Services\Pods\PodsService;
use Symfony\Component\HttpFoundation\Request;


class PodsController extends Controller
{
    public function getAllIPS(Request $request)
    {
        return PodsService::getAllContainers($request->token);
    }
}
