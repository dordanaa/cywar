<?php

namespace App;

use Exception;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\Payer;
use PayPal\Api\Agreement;
use PayPal\Api\PatchRequest;
use PayPal\Api\ShippingAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use PayPal\Api\AgreementStateDescriptor;

class PaypalAgreement extends PaypalPlan
{
    static public function showAgreement($id)
    {
        $agreement = Agreement::get($id, self::apiContext());
        return $agreement;
    }
  
    static public function showTransc($id)
    {
        $params = array('start_date' => date('Y-m-d', strtotime('-15 years')), 'end_date' => date('Y-m-d', strtotime('+5 days')));
        $agreement = Agreement::searchTransactions($id, $params, self::apiContext());
        return $agreement;
    }

    static public function createAgreement($id)
    {
        $createdPlan = Self::showPlan($id);
        $agreement = self::agreementDetails();
        $plan = new Plan();
        $plan->setId($createdPlan->getId());
        $agreement->setPlan($plan);
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);
        
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setLine1('111 First Street')
                        ->setCity('Saratoga')
                        ->setState('CA')
                        ->setPostalCode('95070')
                        ->setCountryCode('US');
        $agreement->setShippingAddress($shippingAddress);
        
        try {
            
            $agreement = $agreement->create(self::apiContext());

            $approvalUrl = $agreement->getApprovalLink();

        }catch (Exception $ex){
            return false;
        }
        
        $user = Auth::user();
        $user->subscription_id = $agreement->plan->id;
        $user->save();

        return $approvalUrl;
    }

    static protected function agreementDetails()
    {
        $agreement = new Agreement();
        $agreement->setName('Base Agreement2')
                ->setDescription('Basic Agreement2')
                ->setStartDate(now()->addMinute()->format('c'));
        return $agreement;
    }

    static protected function executeAgreement($status, $token)
    {
        if($status == 'true')
        {
            try{
                $agreement = new Agreement();
                $agreement->execute($token, self::apiContext());
                Order::new($agreement);
                return ['Subscription Payment Completed Successfully', 200];
            }catch(Exception $ex){
                return ['Subscription Payment Failed', 400];
            }
        }
    }

    static public function updateAgreement($id)
    {
        $createdAgreement = Agreement::get($id, self::apiContext());

        try{
            $patch = new Patch();
            $patch->setOp('replace')
                ->setPath('/')
                ->setValue(json_decode(
                    '{
                        "description": "New Description HAHA"
                    }'
                ));
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);

            $createdAgreement->update($patchRequest, self::apiContext());
            $agreement = Agreement::get($createdAgreement->getId(), self::apiContext());
        } catch(Exception $ex){
            return 'Update Agreement Failed';
        }

        return $agreement;
    }

    static public function stopAgreement($id)
    {
        $createdAgreement = Agreement::get($id, self::apiContext());

        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Suspending the agreement");

        try {
            $createdAgreement->suspend($agreementStateDescriptor, self::apiContext());
            $agreement = Agreement::get($createdAgreement->getId(), self::apiContext());
        } catch (Exception $ex) {
            return 'Stop Agreement Failed';
        }

        $user = Auth::user();
        $user->status = 0;
        $user->save();

        return $agreement;

    }

    static public function reactiveAgreement($id)
    {
        $suspendedAgreement = Agreement::get($id, self::apiContext());

        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Reactivating the agreement");

        try {
            $suspendedAgreement->reActivate($agreementStateDescriptor, self::apiContext());
            $agreement = Agreement::get($suspendedAgreement->getId(), self::apiContext());
        } catch (Exception $ex) {
            return 'Reactive Agreement Failed';
        }
        
        $user = Auth::user();
        $user->status = 1;
        $user->save();

        return $agreement;

    }
}
