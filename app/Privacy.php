<?php

namespace App;

use App\PrivacyLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    protected $table = 'privacy';

    static public function createNew($request)
    {
        $new = new Self;
        $new->content = $request->english;
        $new->content_heb = $request->hebrew;
        $new->created_by = Auth::user()->id;
        $new->save();

        PrivacyLog::new($new);
    }
 
    static public function updatePrivacy($request)
    {
        $update = Self::find($request->id);
        $update->content = $request->english;
        $update->content_heb = $request->hebrew;
        $update->updated_by = Auth::user()->id;
        $update->save();

        PrivacyLog::new($update);
    }
}
