<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qrcode extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
