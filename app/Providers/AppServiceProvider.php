<?php

namespace App\Providers;

use App\Services\Auth\LoginService;
use App\Services\Payment\OrderService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LoginService::class, function ($app) {
            if(isset(request()->type) && $type = request()->type)
            {
                if(request()->type == 'cywar') return new LoginService(180, $type);
                else if(request()->type == 'jc') return new LoginService(180, $type);
                else if(request()->type == 'campus') return new LoginService(180, $type);
            }
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
