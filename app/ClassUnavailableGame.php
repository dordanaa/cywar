<?php

namespace App;

use App\Classes;
use Illuminate\Database\Eloquent\Model;

class ClassUnavailableGame extends Model
{
    static public function new($request)
    {
        $class = Classes::find($request->id);
        if(Classes::checkClass($class->id))
        {
            Self::where('class_id', $class->id)->delete();

            $data = [];
            foreach($request->games AS $game)
            {
                if(isset($game['id']) && is_numeric($game['id']))
                {
                    $data[] = ['class_id' => $request->id, 'game_id' => $game['id']];
                }
            }
            Self::insert($data);
            return ['Class Games Available Updated Successfully',200];
        }else{
            return ['Cannot Update, The Class Games Available Off Your Authorization',400];
        }
    }
}
