<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    static public function getAll($pagination, $request, &$data)
    {
        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = Self::orderBy($request->sortBy, $order)->paginate($pagination);
        }
        else{
            $data = Self::paginate($pagination);
        }
    }
 
    static public function createNew($request)
    {
        $tag = new Self;
        $tag->name = $request->name;
        $tag->description = $request->description;
        $tag->status = 1;
        $tag->created_by = Auth::user()->id;
        $tag->save();

        return ['Tag Created Successfully', 201];
    }
 
    static public function updateTag($request)
    {
        $tag = Self::find($request->id);
        $tag->name = $request->name;
        $tag->description = $request->description;
        $tag->status = $request->status;
        $tag->updated_by = Auth::user()->id;
        $tag->save();

        return ['Tag Update Successfully', 200];
    }

    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['Tag Cannot Be Deleted While Having Games', 400]; 
            }
            return ['Tags have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Tags status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function remove($id)
    {
        DB::delete("DELETE t.*, gt.* FROM tags AS t "
                    . "LEFT JOIN game_tags AS gt ON gt.tag_id = t.id "
                    . "WHERE t.id = ?", [$id]);

        return true;
    }
    
}
