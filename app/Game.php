<?php

namespace App;

use Image;
use App\User;
use App\Point;
use App\Category;
use App\UserAllowedGame;
use App\ClassUnavailableGame;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Game extends Model
{
    public function hint(){
        return $this->hasOne(GameHint::class, 'game_id', 'id')->where('game_hints.user_id', Auth::user()->id)->select(['game_id']);
    }
   
    public function finished(){
        return $this->hasOne(Attempt::class)->where('attempts.user_id', Auth::user()->id)->where('status', 1)->select('game_id');
    }
   
    public function locked(){
        return $this->hasOne(Attempt::class)->where('attempts.user_id', Auth::user()->id)->where('locked', 1)->select('game_id');
    }
   
    public function failed(){
        return $this->hasOne(Attempt::class)->where('attempts.user_id', Auth::user()->id)->where('status', 0)->select('game_id');
    }
   
    public function AllDetails(){ // FOR CMS
        return $this->hasOne(GameDetail::class)->select('game_id', 'title', 'hint', 'hint_points', 'play_time', 'story', 'objective', 'docker', 'user_id');
    }
   
    public function details(){
        return $this->hasOne(GameDetail::class)
                    ->leftjoin('users AS u', 'u.id', 'game_details.user_id')
                    ->select('u.name', 'game_details.game_id', 'game_details.title', 'game_details.hint_points', 'game_details.story', 'game_details.objective', 'game_details.docker', 'game_details.play_time');
    }
   
    // public function containers(){
    //     return $this->hasMany(Container::class, 'game_id', 'id')->groupBy('game_id');
    // }
   
    // public function activeUsers(){
    //     return $this->hasMany(Container::class, 'game_id', 'id')
    //                 ->join('users AS u', 'u.id', 'containers.user_id')
    //                 ->join('user_roles AS ur', 'ur.user_id', 'u.id')
    //                 ->join('roles AS r', 'r.id', 'ur.role_id')
    //                 ->select('containers.*', 'u.name AS user', 'r.role');
    // }
   
    public function attempts(){
        return $this->hasMany(Attempt::class, 'game_id', 'id')->select(['id', 'game_id']);
    }
   
    public function attemptLogs(){
        return $this->hasMany(AttemptLog::class, 'game_id', 'id')->select(['id', 'game_id']);
    }
   
    public function files(){
        return $this->hasMany(GameFile::class)->select(['id', 'game_id', 'name', 'type']);
    }
   
    public function AllInputs(){
        return $this->hasMany(GameInput::class)->select(['game_id', 'name', 'value']);
    }
   
    public function tags(){
        return $this->hasMany(GameTag::class)
                    ->join('tags AS t', 't.id', 'game_tags.tag_id')
                    ->select(['game_id', 'tag_id AS id', 'name']);
    }
   
    public function inputs(){
        return $this->hasMany(GameInput::class)->select(['game_id', 'name']);
    }
   
    public function category(){
        return $this->hasOne(Category::class, 'id', 'category_id')->select('id', 'name', 'slug');
    }
   
    public function points(){
        return $this->hasOne(Point::class, 'id', 'point_id')->select('id', 'name', 'points');
    }
   
    public function plays(){
        return $this->hasOne(Attempt::class, 'game_id', 'id')->select('game_id');
    }
   
    public function wins(){
        return $this->hasOne(Attempt::class, 'game_id', 'id')->where('status', 1)->select('game_id');
    }
   
    // static public function getGames(&$data)
    // {
    //     $id = Auth::user()->id;
    //     $invalids = [];
    //     $userNotAllowed = UserAllowedGame::where('user_id', $id)->get();
    //     foreach($userNotAllowed AS $item){
    //         $invalids[] = $item->game_id;
    //     }
    //     $class = Auth::user()->class;
    //     if($class)
    //     {
    //         $classNotAllowed = ClassUnavailableGame::where('class_id', $class->id)->get();
    //         foreach($classNotAllowed AS $item){
    //             $invalids[] = $item->game_id;
    //         }
    //     }

    //     $games = Game::where('games.status', 1)
    //                  ->join('game_details AS gd', 'gd.game_id', 'games.id')
    //                  ->join('categories AS c', 'c.id', 'games.category_id')
    //                  ->with('hint', 'finished', 'points');
    //     if(in_array(Auth::user()->role->role, ['admin','teacher','master'])){
    //         $games = $games->select('games.*', 'c.slug', 'gd.story');
    //     }
    //     else{
    //         $games = $games->select('c.slug', 'games.id', 'games.point_id', 'games.category_id', 'games.name', 'gd.story', 'games.image');    
    //     }

    //     $games = $games->orderBy('games.name')->get()->toArray();
    //     $arr = [];
    //     foreach($games AS $key => $game){
    //         if(in_array($game['id'], $invalids)){
    //             unset($games[$key]);
    //         }else{
    //             $arr[] = $game;
    //         }
    //     }
    //     $data = $arr;
    // }
   
    static public function playGame($name, &$data)
    {
        $id = Auth::user()->id;
        $data['game'] = Game::where('games.name', $name)
                    ->join('categories AS c', 'c.id', 'games.category_id')
                    ->where('games.status', 1)
                    ->with('inputs', 'files', 'details', 'hint', 'finished', 'points')
                    ->select('games.*', 'c.name AS category')->first();
        if(!$data['game']) return;
        $data['game']->hint_text = Game::addHint($data['game']);
        // $data['user'] = User::where('id', $id)->withCount('played', 'finished', 'hints')->first();
        // $data['user']->details = User::details($id);
    }

    static public function addHint($game)
    {
        if($game->hint) return GameDetail::where('game_id', $game->id)->first(['hint'])->hint;
    }

    static public function getAll($status, $pagination, $request, &$data)
    {
        $data = Self::where('games.status', $status)
                    ->join('categories AS c', 'c.id', 'games.category_id')
                    ->join('points AS p', 'p.id', 'games.point_id')
                    ->select('games.id', 'games.name', 'games.notes', 'games.image', 'games.description', 'games.created_at', 'c.name AS category', 'c.slug', 'p.name AS points');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }
        
        $data = $data->paginate($pagination);
    }

    static public function showGame($id) // For CMS
    {
        $game = Self::where('id', $id)
                    ->with('category', 'points', 'details')
                    ->withCount('plays', 'wins')
                    ->first();
                    
        $date = new \DateTime();
        $date->modify('-3 hours');
        $formatted_date = $date->format('Y-m-d H:i:s');
        $game->containers = Container::where('containers.game_id', $game->id)
                                    ->where('containers.created_at', '>', $formatted_date)
                                    ->groupBy('port')
                                    ->get();

        if($game->containers && count($game->containers))
        {
            $game->active_users = Container::where('containers.game_id', $game->id)
                                        ->where('containers.created_at', '>', $formatted_date)
                                        ->join('users AS u', 'u.id', 'containers.user_id')
                                        ->join('user_roles AS ur', 'ur.user_id', 'u.id')
                                        ->join('roles AS r', 'r.id', 'ur.role_id')
                                        ->select('containers.*', 'u.name AS user', 'r.role')
                                        ->get();
        }
        
        return $game ? [$game, 200] : ['Game Not Found', 400];
    }

    static public function findGame($name) // For Playing
    {
        $game = Game::where('name', $name)->first();
        if(!$game) return ['Game Not Found', 404];  
        $user = Auth::user();
        $game->load('locked');
        if($game->locked) return ['Sorry, You Have No Access To This Game', 400];
        if(UserAllowedGame::where([['user_id', $user->id],['game_id', $game->id]])->first()) return ['Sorry, You Have No Access To This Game', 400];
        $class = $user->class;
        if($class && ClassUnavailableGame::where([['class_id', $class->id], ['game_id', $game->id]])->first()) return ['Sorry, You Have No Access To This Game', 400];
        return true;
    }

    static public function editGame($id, &$data)
    {
        $data = [];
        $data['game'] = Self::where('id', $id)
                    ->with('category', 'points', 'AllDetails', 'files', 'AllInputs', 'tags')
                    ->first();
        $data['categories'] = Category::where('status', 1)->get(['id', 'name']);
        $data['points'] = Point::get(['id', 'name']);
        $data['tags'] = Tag::where('status', 1)->get(['id', 'name']);
        $data['users'] = User::where('status', 1)
                             ->join('user_roles AS ur', 'ur.user_id', 'users.id')
                             ->join('roles AS r', 'r.id', 'ur.role_id')
                             ->where('r.role', '!=', 'student')->get(['users.id', 'users.name']);
    }
    
    static public function updateStatus($request)
    {
        $game = Self::find($request->id);
        $game->status = $request->status;
        $game->updated_at = now();
        $game->updated_by = Auth::user()->id;
        $game->save();
    }
    
    static public function updateImage($request)
    {
        $image_name = Images::uploadImage($request, '/images/games/', 'file');
        if(is_array($image_name)) return $image_name;

        $game = Self::find($request->id);
        $game->imagde = $image_name;
        $game->updated_at = now();
        $game->updated_by = Auth::user()->id;
        $game->save();
        return [$image_name, 200];
    }
    
    static public function updateGuide($request)
    {
        $game = Game::where('id', $request->id)->with('category')->first();
        $slug = $game->category->slug;
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $file = $file->storeAs("LabInstructions/$slug", $fileName, 'local');
        
        $game->guide = $fileName;
        $game->updated_at = now();
        $game->updated_by = Auth::user()->id;
        $game->save();
        
        return $game;
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($id);
            }
            return ['Games have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Games status has been changed successfully to $request->mode", 200];
        }
    }

    static public function createGame($request)
    {
        $id = Auth::user()->id;
        $image_name = self::uploadImage($request);
        $guide = self::uploadGuide($request);
        
        $game = new Self;
        $game->name = $request->name;
        $game->description = $request->description;
        $game->category_id = $request->category_id;
        $game->image = $image_name ? $image_name : 'construction.jpg';
        $game->guide = $guide ? $guide : '';
        $game->status = $request->status ? 1 : 0;
        $game->point_id = $request->point_id;
        $game->notes = $request->notes;
        $game->updated_at = now();
        $game->created_by = $id;
        $game->save();
        $request->id = $game->id;

        GameDetail::createGame($request);
        GameTag::newGame($request->tags, $request->id);
        GameInput::newGameInputs($game->id, $request);
        
        return ['Game Created Successfully', 201];
    }

    static private function uploadImage($request)
    {
        $image_name = null;
        if( $request->hasFile('image') && $request->file('image')->isValid() ){
            $file = $request->file('image');

            if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return false;
            
            $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
            $request->file('image')->move(public_path() . '/images/games/', $image_name);
            // $img = Image::make(public_path() . '/images/games/' . $image_name);
            // $img->resize(1250, 700, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
            return $image_name;
        }else{
            return $image_name;
        }
    }

    static private function uploadGuide($request)
    {
        $slug = Category::where('id', $request->category_id)->first()->slug;
        $file = $request->file('guide');
        if($file){
            $fileName = $file->getClientOriginalName();
            $file = $file->storeAs("LabInstructions/$slug", $fileName, 'local');
            return $file ? $fileName : false;
        }
    }

    static public function updateGame($request)
    {
        $id = Auth::user()->id;
        $game = Self::find($request->id);

        if($game->category_id != $request->category_id) // If game category has Changed
        {
            $newSlug = Category::find($request->category_id)->slug;
            $lastSlug = Category::find($game->category_id)->slug;
            Storage::disk('local')->move("LabInstructions/$lastSlug/$game->guide", "LabInstructions/$newSlug/$game->guide");
        }

        $game->name = $request->name;
        $game->description = $request->description;
        $game->category_id = $request->category_id;
        $game->point_id = $request->point_id;
        if($request->notes) $game->notes = $request->notes;
        $game->updated_at = now();
        $game->updated_by = $id;
        $game->save();


        GameDetail::updateGame($game->id, $request);
        $game->tags()->delete();
        GameTag::newGame($request->tags, $request->id);
        $game->inputs()->delete();
        GameInput::newGameInputs($game->id, $request);

        return ['Game Created Successfully', 201];
    }

    static public function remove($id)
    {
        DB::delete("DELETE g.*, gd.*, gf.*, gi.* FROM games AS g "
                    . "LEFT JOIN game_details AS gd ON gd.game_id = g.id "
                    . "LEFT JOIN game_files AS gf ON gd.game_id = g.id "
                    . "LEFT JOIN game_inputs AS gi ON gd.game_id = g.id "
                    . "WHERE g.id = ?", [$id]);
    }

    static public function closeDocker($request)
    {
      Container::where('game_id', $request->game_id)->where('port', $request->port)->delete();
    }
}
