<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class MissionTaskAttempt extends Model
{
    protected $guarded = [];

    static public function new($data, $task)
    {
        $id = Auth::user()->id;
        $status = $data->status ? 1 : 0; 
        $attempt = Self::where([['user_id', $id], ['mission_task_id', $task->task]])->first();
        if($attempt) // if user already tested this task 
        {
            if($attempt->status) return; // if user already finished task, no need to add attempts 
            $attempt->update([
                'status' => $status,
                'updated_at' => now(),
                'attempts' => DB::raw('attempts + 1'),
            ]);
        }
        else
        {
            $attempt = new Self;
            $attempt->user_id = $id;
            $attempt->mission_task_id = $task->task;
            $attempt->status = $status;
            $attempt->attempts = 1;
            $attempt->save();
        }
    }
}
