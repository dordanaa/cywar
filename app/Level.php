<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    static public function createNew($request)
    {
        $last = Self::orderBy('level', 'desc')->first();
        if($last)
        {
            $last->next_level = $request->points;
            $last->save();
        }

        $level = new Self;
        $level->level = $request->level;
        $level->points = $request->points;
        $level->next_level = NULL;
        $level->status = 1;
        $level->created_by = Auth::user()->id;
        $level->save();

        return $level;
    }

    static public function updateLevel($request)
    {
        $level = Self::find($request->id);
        if($level)
        {
            $level->level = $request->level;
            $level->points = $request->points;
            $level->updated_by = Auth::user()->id;
            $level->save();
            
            $last = Self::where('level', $level->level -1 )->first();
            if($last)
            {
                $last->next_level = $request->points;
                $last->save();
            }
        }
    }

    static public function updateStatus($request)
    {
        $level = Self::find($request->id);
        $level->status = $request->status;
        $level->updated_at = now();
        $level->updated_by = Auth::user()->id;
        $level->save();
    }
    
    static public function remove($id)
    {
        $delete = Level::where('id', $id)->first();
        if($delete->next_level == NULL)
        {
            $delete->delete();
        }else {
            return ['Can Only Delete Last Level', 400];
        }
        $level = Level::orderBy('level', 'desc')->first();
        $level->next_level = null;
        $level->updated_by = Auth::user()->id;
        $level->save();

        return ['Level Deleted Successfully', 200];
    }
}
