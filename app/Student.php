<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function logs(){
        return $this->hasMany(UserLogBp::class, 'email', 'email');
    }
    
    public function attempts(){
        return $this->hasMany(AttemptBp::class, 'student_id', 'id');
    }
    
    public function a_logs(){
        return $this->hasMany(AttemptLogBp::class, 'student_id', 'id');
    }
    
    public function totalScore(){
        return $this->hasOne(TotalscoreBp::class, 'student_id', 'id');
    }
    
    public function hints(){
        return $this->hasMany(GameHintBp::class, 'student_id', 'id');
    }
}
