<?php

namespace App;

use App\Services\Images\ImageService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CyberpediaTerm extends Model
{
    // protected $casts = [
    //     'created_at' => 'datetime:Y-m-d',
    //     'updated_at' => 'datetime:Y-m-d',
    // ];

    static public function getAll($pagination, $request, &$data)
    {
        $data = Self::join('cyberpedia_categories AS cc', 'cc.id', 'cyberpedia_terms.category_id');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->select('cyberpedia_terms.name', 'cyberpedia_terms.id', 'cyberpedia_terms.status', 'cyberpedia_terms.content', 'cyberpedia_terms.image', 'cyberpedia_terms.link', 'cc.name AS category')->paginate($pagination);
    }

    /**
     * Autocomplete Search
     */
    static public function search(&$data, $value)
    {
        $data = Self::where('name', 'LIKE', "%$value%")
                    ->orWhere('content', 'LIKE', "%$value%")
                    ->limit(5)
                    ->select('id', 'category_id', 'name')
                    ->get();
    }

    static public function createNew($request)
    {
        try{
            $image_name = ImageLoader::new($request, 'file', '/images/cyberpedia/terms/');
            $category = new Self;
            $category->category_id = $request->category_id;
            $category->name = $request->name;
            $category->content = $request->content;
            $category->image = $image_name ? $image_name : NULL;
            $category->link = $request->link;
            $category->status = $request->status ? 1 : 0;
            $category->created_at = now();
            $category->created_by = Auth::user()->id;
            $category->save();
            return ['Term Created Succesfully', 201];
        }catch(Exception $ex){
            return ['Failed Creating Term', 400];
        }
    }

    static public function updateTerms($request)
    {
        try{
            $image_name = ImageLoader::new($request, 'file', '/images/cyberpedia/terms/');
            $category = Self::find($request->id);
            $category->category_id = $request->category_id;
            $category->name = $request->name;
            $category->content = $request->content;
            $category->image = $image_name ? $image_name : NULL;
            $category->link = $request->link;
            $category->status = $request->status ? 1 : 0;
            $category->created_at = now();
            $category->created_by = Auth::user()->id;
            $category->save();
            return ['Term Updated Succesfully', 200];
        }catch(Exception $ex){
            dd($ex);
            return ['Failed Updating Term', 400];
        }
    }

    static public function checkNumber($mid, $number, $id)
    {
        if( $lesson = Self::where([['module_id', $mid],['number', $number],['id', '!=', $id]])->first() )
        {
            $lesson->number = $number + 1;
            $lesson->save();
            self::checkNumber($mid, $lesson->number, $lesson->id);
        }
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($id);
            }
            return ['Terms have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Terms status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }

    static public function remove($id)
    {
        $term = Self::find($id);
        ImageService::remove('\images\cyberpedia\terms\\' . $term->image);
        $term->delete();
    }
}
