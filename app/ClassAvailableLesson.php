<?php

namespace App;

use App\Classes;
use Illuminate\Database\Eloquent\Model;

class ClassAvailableLesson extends Model
{
    static public function new($request)
    {
        $class = Classes::find($request->id);
        if(Classes::checkClass($class->id))
        {
            Self::where('class_id', $class->id)->delete();

            $data = [];
            foreach($request->lessons AS $lesson)
            {
                if(isset($lesson['id']) && is_numeric($lesson['id']))
                {
                    $data[] = ['class_id' => $request->id, 'lesson_id' => $lesson['id']];
                }
            }
            Self::insert($data);
            return ['Class Lessons Available Updated Successfully',200];
        }else{
            return ['Cannot Update, The Class Lessons Available Off Your Authorization',400];
        }
    }
}
