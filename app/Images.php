<?php

namespace App;

use Exception;
use App\Services\Logger\LoggerService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Images extends Model
{
    /**
     * Uploads a image
     * Generic function for uploading images
     * 
     * @param object $data
     * @param string $path
     * @param string $file_name
     */
    static public function uploadImage($req, $path, $file_name)
    {
        try{
            if( $req->hasFile($file_name) && $req->file($file_name)->isValid() ){
            
                $file = $req->file($file_name);
                
                if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return ['Image Upload Failed', 400];
                
                $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
                $req->file($file_name)->move(public_path() . $path, $image_name);
                // $img = Image::make(public_path() . '/images/cats/' . $image_name);
                // $img->resize(1250, 700, function ($constraint) {
                //     $constraint->aspectRatio();
                // });
                // $img->save();
                return $image_name;
            }
        }catch(Exception $ex){
            LoggerService::init('ERROR', $ex->getMessage());
            return ['Image Upload Failed', 400];
        }
    }
}
