<?php

namespace App;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class UserClass extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id')->with('role');
    }
    
    public function students(){
        return $this->hasMany(User::class, 'id', 'user_id');
    }

    static public function updateClass($request)
    {
        $id = $request->id;
        $user = User::find($id);
        if(Auth::user()->role->role != 'admin' && !in_array($request->class, User::IdOfClasses())) return ['Cannot Update The Student Class To Not Your Own Class', 400];
        if(User::checkAuthorization($user))
        {
            if ($class = Self::where('user_id', $id)->first()) $class->delete();
            Self::create([
                'user_id' => $id,
                'class_id' => $request->class,
            ]);

            UserClassesLog::new($id, $request->class);
    
            return ['User Class Updated Successfully',201];
        }else{
            return ['Cannot Update The Student Class Off Your Authorization',400];
        }

    }

    static public function updateMultipleClasses($request)
    {
        $id = $request->id;
        $role = User::find($id)->role->role; 
        if(!in_array($role, ['teacher', 'master'])) return ['User Is Not Able To Have Classes', 400];

        if ($classes = Self::where('user_id', $id)->get()){ // Delete Old Classes
            if(count($classes))
            {
                foreach($classes AS $class){
                    $class->delete();
                }
            }
        }

        $data = [];
        foreach($request->classes AS $class){
            $data[] = [
                'user_id' => $id,
                'class_id' => $class['id'],
            ];
            if(!in_array($role, ['admin', 'master'])) UserClassesLog::new($id, $class['id']);
        }
        Self::insert($data);

        return ['Student Class Updated Successfully',201];
    }

    static public function removeClass($id)
    {
        $user = User::find($id);
        if(User::checkAuthorization($user))
        {
            Self::where('user_id', $id)->first()->delete();
            return ['Student Class Deleted Successfully',200];
        }else{
            return ['Cannot Delete The Student Class Off Your Authorization',400];
        }
    }

}
