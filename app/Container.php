<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    public $timestamps = false;

    public function game(){
        return $this->hasOne(Game::class, 'id', 'game_id');
    }
    
    static public function newContainer($data, $id)
    {
        $con = new Self;
        $con->user_id = Auth::user()->id;
        // $con->ip = '13.59.192.100';
        // $con->port = '50020';
        $con->ip = $data->ip;
        $con->port = $data->port;
        $con->game_id = $id;
        $con->created_at = now();
        $con->save();
    }
}
