<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    static public function updateStatus($request)
    {
        $avatar = Self::find($request->id);
        $avatar->status = $request->status;
        $avatar->updated_at = now();
        $avatar->updated_by = Auth::user()->id;
        $avatar->save();
    }
    
    static public function updateAvatar($request)
    {
        $avatar = Self::find($request->id);
        $avatar->level = $request->level;
        $avatar->updated_at = now();
        $avatar->updated_by = Auth::user()->id;
        $avatar->save();
    }
    
    static public function createNew($request)
    {
        $image_name = null;
        if( $request->hasFile('image') && $request->file('image')->isValid() ){
            $file = $request->file('image');
            $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
            $request->file('image')->move(public_path() . '/images/avatars/', $image_name);
            // $img = Image::make(public_path() . '/images/avatars/' . $image_name);
            // $img->resize(500, 500, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
            
            $avatar = new Self;
            $avatar->level = $request->level;
            $avatar->image = $image_name;
            $avatar->status = $request->status ? 1 : 0;
            $avatar->created_at = now();
            $avatar->created_by = Auth::user()->id;
            $avatar->save();
            return $avatar;
        }
    }
}
