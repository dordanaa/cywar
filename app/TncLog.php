<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class TncLog extends Model
{
    public $timestamps = false;
    
    static public function new($data)
    {
        $new = new Self;
        $new->tnc_id = $data->id;
        $new->content = $data->content;
        $new->content_heb = $data->content_heb;
        $new->created_by = Auth::user()->id;
        $new->save();
    }
}
