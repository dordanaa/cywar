<?php

namespace App;

use App\User;
use App\Attempt;
use App\UserClass;
use App\ClassAvailableLesson;
use App\ClassUnavailableGame;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Classes extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    
    protected $table = 'classes';
    protected $guarded = [];

    public function students(){ // **************************************** NOT THE BEST WAY
        return $this->hasMany(UserClass::class, 'class_id', 'id')
                    ->join('users AS u', 'u.id', 'user_classes.user_id')
                    ->join('user_roles AS ur', 'ur.user_id', 'user_classes.user_id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->where('r.role', 'student')
                    ->select('class_id');
    }

    public function college(){
        return $this->hasOne(College::class, 'id', 'college_id')->select('id', 'name', 'country_id');
    }

    public function teacher(){
        return $this->hasOne(UserClass::class, 'class_id', 'id')
                    ->join('user_roles AS ur', 'ur.user_id', 'user_classes.user_id')
                    ->join('users AS u', 'ur.user_id', 'u.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->where('role', 'teacher')
                    ->select('user_classes.*', 'u.name', 'r.role');
    }

    public function master(){
        return $this->hasOne(UserClass::class, 'class_id', 'id')
                    ->join('user_roles AS ur', 'ur.user_id', 'user_classes.user_id')
                    ->join('users AS u', 'ur.user_id', 'u.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->where('role', 'master')
                    ->select('user_classes.*', 'u.name', 'r.role', 'u.email');
    }
    
    public function lessonsAllowed(){
        return $this->hasMany(ClassAvailableLesson::class, 'class_id', 'id')
                    ->join('lessons AS l', 'l.id', 'class_available_lessons.lesson_id')
                    ->where('l.status', 1)
                    ->select('class_available_lessons.class_id', 'l.id', 'l.name');
    }
    
    public function gamesNotAllowed(){
        return $this->hasMany(ClassUnavailableGame::class, 'class_id', 'id')
                    ->join('games AS g', 'g.id', 'class_unavailable_games.game_id')
                    ->where('g.status', 1)
                    ->select('class_unavailable_games.class_id', 'g.id', 'g.name');
    }


    static public function createNew($request)
    {
        $class = new Self;
        $class->college_id = $request->college_id;
        $class->name = $request->name;
        $class->start_at = $request->start_at;
        $class->end_at = $request->end_at;
        $class->notes = $request->notes;
        $class->status = $request->status ? 1 : 0;
        $class->created_by = Auth::user()->id;
        $class->save();

        if(Auth::user()->role->role == 'master')
        {
            UserClass::insert([
                'class_id' => $class->id,
                'user_id' => Auth::user()->id
            ]);
        }
    }

    static public function getStudents($id, $pagination, &$data, $request)
    {
        $students = User::join('user_classes AS uc', 'uc.user_id', 'users.id')
                        ->where('uc.class_id', $id)
                        ->where('r.role', 'student')
                        ->join('user_roles AS ur', 'ur.user_id', 'users.id')
                        ->join('roles AS r', 'r.id', 'ur.role_id')
                        ->leftjoin('total_scores AS tp', 'tp.user_id', 'users.id')
                        ->with('lastLogged', 'played');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $students = $students->orderBy($request->sortBy, $order);
        }

        $students = $students->select('users.id', 'users.name', 'users.email', 'users.phone', 'users.status', 'tp.total_points', 'tp.games')->paginate($pagination);
        foreach($students AS $student){
            $student->makeHidden(['password']);
        }
        $data = $students;
    }

    static public function getUsers($id, $pagination, &$data)
    {
        $users = User::join('user_classes AS uc', 'uc.user_id', 'users.id')
                    ->where('uc.class_id', $id)
                    ->where('r.role', '!=', 'student')
                    ->join('user_roles AS ur', 'ur.user_id', 'users.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->with('lastLogged', 'played')
                    ->select('users.id','users.name', 'users.email', 'users.phone', 'users.status', 'r.role')->paginate($pagination);
        foreach($users AS $student){
            $student->makeHidden(['password']);
        }
        $data = $users;
    }

    static public function updateClass($request)
    {
        if(self::checkClass($request->id))
        {
            $class = Self::find($request->id);
            $class->college_id = $request->college_id;
            $class->name = $request->name;
            $class->start_at = $request->start_at;
            $class->end_at = $request->end_at;
            $class->notes = $request->notes;
            $class->updated_by = Auth::user()->id;
            $class->save();

            return ['Class Updated Successfully', 200];
        }
        return ['Cannot Update A Class Not Under Your Authorization', 400];
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                return self::remove($id);
            }
            return ['Categories have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Categories status has been changed successfully to $request->mode", 200];
        }
    }

    static public function updateStatus($request)
    {
        if(self::checkClass($request->id))
        {
            $cat = Self::find($request->id);
            $cat->status = $request->status;
            $cat->updated_at = now();
            $cat->updated_by = Auth::user()->id;
            $cat->save();

            return ['Class Status Updated Successfully', 200];
        }
        return ['Cannot Update A Class Not Under Your Authorization', 400];
    }

    static public function show($id, &$data)
    {
        if(self::checkClass($id))
        {
            $data = Self::where('id', $id)->with('lessonsAllowed', 'gamesNotAllowed', 'teacher')->first();
            if(!$data) return ['Class Not Found'];
            return false;
        }
            return ['Cannot Show the Class Not Under Your Authorization'];

    }

    static public function updateClassPasswords($password, $id)
    {
        if(self::checkClass($id))
        {
            User::join('user_classes AS uc', 'uc.user_id', 'users.id')->where('uc.class_id', $id)->update([
                'password' => bcrypt($password)
            ]);
            return ["Class's users passwords Updated Successfully", 200];
        }
        return ['Cannot Update A Class Not Under Your Authorization', 400];
    }
    
    static public function remove($id)
    {
        if(self::checkClass($id))
        {
            DB::delete("DELETE c.*, uc.* FROM classes AS c "
                        . "LEFT JOIN user_classes AS uc ON uc.class_id = c.id "
                        . "WHERE c.id = ?", [$id]);
                    
            return ['Class Deleted Successfully', 200];
        }
        return ['Cannot Delete A Class Not Under Your Authorization', 400];
    }

    static public function getClasses($pagination, &$data, $request)
    {
        $cats = Classes::with('students')
                       ->join('colleges AS c', 'c.id', 'classes.college_id');

        if(Auth::user()->role->role != 'admin')
        {
            $classes = User::IdOfClasses();
            $cats = $cats->whereIn('classes.id', $classes);
        }

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $cats->orderBy($request->sortBy, $order);
        }

        $data = $cats->select('classes.*', 'c.name AS college')->paginate($pagination);
    }

    static public function checkClass($id)
    {
        if(Auth::user()->role->role != 'admin')
        {
            $classes = User::IdOfClasses();
            return in_array($id, $classes) ? true : false;
        }
            return true;
    }

    static public function getStatistics($id, &$data)
    {
        $students = UserClass::where('user_classes.class_id', $id)
                              ->join('users AS u', 'u.id', 'user_classes.user_id')
                              ->join('user_roles AS ur', 'ur.user_id', 'u.id')
                              ->join('roles AS r', 'r.id', 'ur.role_id')
                              ->where('r.role', 'student')
                              ->select('u.id', 'u.name')
                              ->get();
        $arrID = [];
        foreach($students AS $student){
            $arrID[] = $student->id;
        }

        $data['populars'] = Attempt::whereIn('attempts.user_id', $arrID)
                                    ->join('games AS g', 'g.id', 'attempts.game_id')
                                    ->groupBy('attempts.game_id')
                                    ->orderBy('total', 'desc')
                                    ->select('game_id', 'g.name', DB::raw('count(*) as total'))
                                    ->limit(10)
                                    ->get();

        $data['allChallenges'] = Attempt::whereIn('user_id', $arrID)
                                ->join('games AS g', 'g.id', 'attempts.game_id')
                                ->select('game_id', 'g.name', 'attempts.status')
                                ->get();

        $data['categories'] = Attempt::whereIn('user_id', $arrID)
                                ->join('games AS g', 'g.id', 'attempts.game_id')
                                ->join('categories AS c', 'c.id', 'g.category_id')
                                ->groupBy('g.category_id')
                                ->orderBy('total', 'desc')
                                ->select('c.name', DB::raw('count(*) as total'))
                                ->get();

        // $failedGames = Attempt::whereIn('user_id', $arrID)
        //                 ->where('attempts.status', 0)
        //                 ->join('games AS g', 'g.id', 'attempts.game_id')
        //                 ->groupBy('game_id')
        //                 ->orderBy('total', 'desc')
        //                 ->select('game_id', 'g.name', DB::raw('count(*) as total'))
        //                 ->limit(5)
        //                 ->get();

        // $finishedGames = Attempt::whereIn('user_id', $arrID)
        //                 ->where('attempts.status', 1)
        //                 ->join('games AS g', 'g.id', 'attempts.game_id')
        //                 ->groupBy('game_id')
        //                 ->orderBy('total', 'desc')
        //                 ->select('game_id', 'g.name', DB::raw('count(*) as total'))
        //                 ->limit(5)
        //                 ->get();

        $data['totalScores'] = TotalScore::whereIn('total_scores.user_id', $arrID)
                                ->join('users AS u', 'u.id', 'total_scores.user_id')
                                ->orderBy('total_scores.total_points', 'desc')
                                ->select('u.name', 'u.id', 'total_scores.total_points', 'total_scores.games')
                                ->limit(5)
                                ->get();
    }

    static public function getLiveDockers($id, &$data)
    {
        $date = new \DateTime();
        $date->modify('-3 hours');
        $formatted_date = $date->format('Y-m-d H:i:s');
        $users = UserClass::where('class_id', $id)->get();
        $data['challenges'] = UserClass::where('user_classes.class_id', $id)
                                       ->join('containers', 'containers.user_id', 'user_classes.user_id')
                                       ->join('users AS u', 'u.id', 'user_classes.user_id')
                                       ->join('games AS g', 'g.id', 'containers.game_id')
                                       ->join('categories AS c', 'c.id', 'g.category_id')
                                       ->where('containers.created_at', '>', $formatted_date)
                                       ->get(['containers.id', 'containers.port', 'containers.ip', 'containers.created_at', 'u.name AS user', 'g.name AS game', 'c.name AS category',]);

        $data['lessons'] = UserClass::where('user_classes.class_id', $id)
                                       ->join('lesson_containers', 'lesson_containers.user_id', 'user_classes.user_id')
                                       ->join('lessons AS l', 'l.id', 'lesson_containers.lesson_id')
                                       ->join('users AS u', 'u.id', 'user_classes.user_id')
                                       ->join('modules AS m', 'm.id', 'l.module_id')
                                       ->where('lesson_containers.created_at', '>', $formatted_date)
                                       ->get(['lesson_containers.id', 'lesson_containers.container_id', 'lesson_containers.port', 'u.name AS user', 'lesson_containers.ip', 'lesson_containers.created_at', 'l.name AS lesson', 'm.name AS module']);
    }
    
    static public function closeDocker($request, $id)
    {
        if( self::checkAuthorization($id) )
        {
            DB::table($request->table)->where('id', $request->id)->delete();
            return ['Container Has Been Deleted', 200];
        }
        return ['The Student is not under your Authorization', 400];
    }

}
