<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class GameFile extends Model
{
    static public function addFiles($request)
    {
        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $file = $file->storeAs('files', $fileName, 'pub');
        
        $new = new Self;
        $new->name = $fileName;
        $new->game_id = $request->id;
        $new->created_by = Auth::user()->id;
        $new->save();

        $res = [
            'name' => $fileName,
            'id', $new->id
        ];
        return $res;
    }
    
    static public function newFiles($request)
    {
        if(!count($request->files)) return;
        $data = [];
        foreach($request->files AS $i){
            $file = $i->file('file');
            $fileName = $file->getClientOriginalName();
            $file = $file->storeAs('file', $fileName, 'pub');
            $data[] = [
                'game_id' => $request->id,
                'name' => $fileName,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        Self::insert($data);
    }

    static public function remove($id)
    {
        $file = Self::where('id', $id)->first();
        $file->delete();
        $game = Game::where('id', $file->game_id)->with('category')->first();
        $slug = $game->category->slug;
        Storage::disk('pub')->delete("games/$slug/$game->name/$file->name");
    }
}
