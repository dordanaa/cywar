<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    
    public function country(){
        return $this->hasOne(Country::class, 'id', 'country_id')->select('id', 'country');
    }
    
    public function classes(){
        return $this->hasMany(Classes::class, 'college_id', 'id');
    }
    
    public function students(){
        return $this->hasMany(Classes::class, 'college_id', 'id')
                    ->rightjoin('user_classes AS uc', 'uc.class_id', 'classes.id')
                    ->rightjoin('users AS u', 'u.id', 'uc.user_id')
                    ->rightjoin('user_roles AS ur', 'ur.user_id', 'uc.user_id')
                    ->rightjoin('roles AS r', 'r.id', 'ur.role_id')
                    ->where('r.role', 'student');
    }

    static public function createNew($type, $request)
    {
        $college = new Self;
        $college->country_id = $request->country_id;
        $college->college_type_id = $type;
        $college->name = $request->name;
        $college->status = $request->status ? 1 : 0;
        $college->save();
    }

    static public function updateCollege($type, $request)
    {
        $college = Self::find($request->id);
        $college->country_id = $request->country_id;
        $college->name = $request->name;
        $college->college_type_id = $type;
        $college->save();
    }

    static public function getClasses($id, $pagination, &$data)
    {
        $data = Self::where('colleges.id', $id)
                    ->join('classes AS c', 'c.college_id', 'colleges.id')
                    ->paginate($pagination);
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['College Cannot Be Deleted While Having Games', 400]; 
            }
            return ['Colleges have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Colleges status has been changed successfully to $request->mode", 200];
        }
    }

    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }
    
    static public function remove($id)
    {
        // If College has classes cannot be deleted
        $college = Self::where('id', $id)->with('classes')->first();
        if(count($college->classes) > 0) return ['Cannot Delete College With Classes, Please Remove All Classes First', 400];

        DB::delete("DELETE c.* FROM colleges AS c "
                    . "WHERE c.id = ?", [$id]);

        return ['College Deleted Successfully', 200];
    }
}
