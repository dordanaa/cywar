<?php

namespace App;

use App\Game;
use App\UserAllowedGame;
use App\ClassUnavailableGame;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    
    public function games(){
        return $this->hasMany(Game::class, 'category_id', 'id');
    }
    
    public function fullgames(){
        return $this->hasMany(Game::class, 'category_id', 'id')
                    ->where('status', 1)
                    ->orderBy('point_id')
                    ->with('finished', 'points', 'details', 'hint', 'failed', 'locked');
    }

    static public function getAll($pagination, $request, &$data)
    {
        $data = Self::withCount('games');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->paginate($pagination);

    }

    static public function createNew($request)
    {
        $image_name = Images::uploadImage($request, '/images/cats/', 'file');
        if(is_array($image_name)) return $image_name;

        $game = new Self;
        $game->name = $request->name;
        $game->description = $request->description;
        $game->slug = $request->slug;
        $game->min_points = $request->min_points;
        $game->min_level = $request->min_level;
        $game->image = $image_name ? $image_name : 'construction.jpg';
        $game->status = $request->status ? 1 : 0;
        $game->created_at = now();
        $game->created_by = Auth::user()->id;
        $game->save();
        return ['Category Created Succesfully', 201];
    }

    static public function updateCat($request)
    {
        $image_name = Images::uploadImage($request, '/images/cats/', 'file');
        if(is_array($image_name)) return $image_name;
        
        $game = Self::find($request->id);
        if(!$game) return ['Game Not Found', 400];
        
        $game->name = $request->name;
        $game->description = $request->description;
        $game->slug = $request->slug;
        $game->min_points = $request->min_points;
        $game->min_level = $request->min_level;
        if($image_name) $game->image = $image_name;
        $game->status = $request->status == 1 ? 1 : 0;
        $game->created_at = now();
        $game->created_by = Auth::user()->id;
        $game->save();
        return ['Category Updated Succesfully', 200];
    }

    static public function getCats(&$data)
    {
        $user = Auth::user();

        $data['data'] = Category::where('status', 1)->with('fullgames')->get();
        $blocked = [];
        $userNotAllowed = UserAllowedGame::where('user_id', $user->id)->get();
        foreach($userNotAllowed AS $item){
            $blocked[] = $item->game_id;
        }
        $class = $user->class;
        if($class)
        {
            $classNotAllowed = ClassUnavailableGame::where('class_id', $class->id)->get();
            foreach($classNotAllowed AS $item){
                $blocked[] = $item->game_id;
            }
        }
        $data['blocked'] = $blocked;
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['Category Cannot Be Deleted While Having Games', 400]; 
            }
            return ['Categories have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Categories status has been changed successfully to $request->mode", 200];
        }
    }

    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }

    static public function remove($id)
    {
        // If a category has games, cannot be deleted
        if( count(Game::where('category_id', $id)->get()) ) return false;

        DB::delete("DELETE c.*, g.*, gd.*, gf.*, gi.* FROM categories AS c "
                    . "LEFT JOIN games AS g ON g.category_id = c.id "
                    . "LEFT JOIN game_details AS gd ON gd.game_id = g.id "
                    . "LEFT JOIN game_files AS gf ON gd.game_id = g.id "
                    . "LEFT JOIN game_inputs AS gi ON gd.game_id = g.id "
                    . "WHERE c.id = ?", [$id]);

        return true;
    }

}
