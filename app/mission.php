<?php

namespace App;

use App\MissionTask;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Mission extends Model
{
    public function tasks(){
        return $this->hasMany(MissionTask::class, 'mission_id', 'id')
                    ->where('status', 1)
                    ->with('userCompleted')
                    ->select('mission_id', 'id', 'content', 'explanation');
    }

    public function cms_tasks(){
        return $this->hasMany(MissionTask::class, 'mission_serial_id', 'serial_id')->select('mission_serial_id', 'mission_id', 'id', 'content', 'solution', 'status', 'explanation');
    }

    static public function new($missions, $lesson_id)
    {
        $id = Auth::user()->id;
        foreach($missions AS $key => $mission){
            $newmission = new Self;
            $newmission->lesson_id = $lesson_id;
            $newmission->title = $mission['title'];
            $newmission->status = $mission['status'];
            // $newmission->order = $key;
            // $newmission->mission_id = $mission['mission_id'];
            $newmission->description = $mission['description'];
            $newmission->created_by = $id;
            $newmission->save();
            MissionTask::new($mission, $newmission, $id);
        }
    }

    static public function updateMissions($data, $lesson_id)
    {
        $id = Auth::user()->id;
        if(isset($data['cms_missions_del']) && count($data['cms_missions_del']))
        {
            foreach($data['cms_missions_del'] AS $item){
                if(isset($item['id']))
                {
                    DB::delete("DELETE m.*, t.* FROM missions AS m "
                    . "LEFT JOIN mission_tasks AS t ON t.mission_id = m.id "
                    . "WHERE m.id = ?", [$item['id']]);
                }
            }
        }

        if(isset($data['cms_missions']) && count($data['cms_missions']))
        {
            foreach($data['cms_missions'] AS $key => $item){
                if(isset($item['id']))
                {
                    $mission = Self::find($item['id']);
                    $mission->lesson_id = $lesson_id;
                    $mission->title = $item['title'];
                    $mission->status = $item['status'];
                    // $mission->order = $key + 1;
                    $mission->description = $item['description'];
                    $mission->updated_by = $id;
                    $mission->updated_at = now();
                    $mission->save();
                }
                else 
                {
                    $mission = new Self;
                    $mission->lesson_id = $lesson_id;
                    $mission->title = $item['title'];
                    $mission->status = $item['status'];
                    // $mission->order = $key + 1;
                    // $mission->mission_id = $item['mission_id'];
                    $mission->description = $item['description'];
                    $mission->created_by = $id;
                    $mission->created_at = now();
                    $mission->save();
                }
                MissionTask::updateTask($item, $id, $mission->id);
            }
        }
        
    }
}
