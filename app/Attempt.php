<?php

namespace App;

use App\Game;
use App\GameHint;
use App\AttemptLog;
use App\TotalScore;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

class Attempt extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    
    protected $guarded = [];

    static public function new($request, $gid)
    {
        $id = Auth::user()->id;
        $game = Game::where('id', $gid)->with('points')->first();
        if(!$game) return ['Game does not exists', 400];
        
        $data = self::checkForm($request->inputs, $game); // Check if form inputs are valid

        $points = $data['points'];
        // Checks if user used an hint
        if(GameHint::where([['game_id', $game->id], ['user_id', $id]])->first())
        {
            $hint = GameDetail::where('game_id', $game->id)->first(['hint_points'])->hint_points;
            $points = $points - ($game->points->points * ($hint / 100)); // Calc points if student used an hint
            if($points < 0 ) $points = 0; // Points cannot be under 0
        }

        AttemptLog::new($game->id, $id, $points, $data['status']); // New Attempt log record

        $attempt = Attempt::where([['game_id', $game->id], ['user_id', $id]])->first();
        if($attempt && $attempt->status != 1 || !$attempt) // Checks if haven't finished the game already
        {
            if($attempt) // If already tried this game
            {
                if(($points - $attempt->points) > 0) TotalScore::new($id, $points - $attempt->points, 0); // Updates Totalscore only if needed 

                $attempt->increment('attempts');
                if($attempt->points < $points)
                {
                    $attempt->update([
                        'points' => $points,
                        'status' => $data['status'],
                        'locked' => in_array(Auth::user()->role->role, ['student', 'candidate']) ? $data['status'] : 0,
                        'updated_at' => now()
                    ]);
                }
            }else // Creating first attempt for this game
            {
                if($points) TotalScore::new($id, $points, 1);

                $attempt = new Self;
                $attempt->user_id = $id;
                $attempt->game_id = $game->id;
                $attempt->points = $points;
                $attempt->status = $data['status'];
                $attempt->locked = in_array(Auth::user()->role->role, ['student', 'candidate']) ? $data['status'] : 0;
                $attempt->attempts = 1;
                $attempt->save();
            }
        }

        //Cookie::get('jc');
        $jsCookie = Cookie::get('jc');
        if($jsCookie) {
           $decryptedJc = Crypt::decrypt($jsCookie);

           
           if($decryptedJc) {
               
            $decryptedJc['token'] = 'MGEvnglqeijUpZW13o7yxIGddEjELc';
            $decryptedJc['grade'] = $points;

            $jcWebHook = 'https://'.$decryptedJc['host'].'/webhook/hackeru_report';
            $jcRedirect = 'https://'.$decryptedJc['host'].'/myinvites';

            $website = curl_init();
            curl_setopt($website, CURLOPT_URL, $jcWebHook);
            curl_setopt($website, CURLOPT_POST, true);
            curl_setopt($website, CURLOPT_POSTFIELDS, http_build_query($decryptedJc));
            curl_setopt($website, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($website, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($website, CURLOPT_TIMEOUT, 30);
            $exec = curl_exec($website);

            curl_close($website);
            }

           return ['Your test has been submitted',200, $jcRedirect];
        }
        if($data['status'])
        {
            return ['You have finished the challenge successfully', 200];
        }else 
        {
            if($data['points'] > 0)
            {
                return ['You are partly right', 400];
            }else{
                return ['You have failed the challenge', 400];
            }
        };
       
    }

    static private function checkForm($inputs, $game)
    {
        $valid = true;
        $points = 0;
        $status = 0;
        if($inputs && !is_array($inputs)) // Checks if it is an array
        {
            $valid = false;
        }

        if($valid) // If valid
        {
            $gameInputs = GameInput::where('game_id', $game->id)->get();
            foreach($gameInputs AS $i => $input) // Checks each input
            {
                if($input->value === $inputs[$i]) // Comparing form input to its correct value
                {
                    $points += $game->points->points / count($inputs); // Counting points for each correct input
                }
            }
            $status = $game->points->points == $points ? 1 : 0; // Check the game status, if finished or not
        }
        return [
            'points' => $points,
            'status' => $status,
        ];
    }

    static public function updateLock($status, $user_id, $game_id)
    {
        Self::where('user_id', $user_id)
            ->where('game_id', $game_id)
            ->update(['locked' => $status]);
    }

}
