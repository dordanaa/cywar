<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class GameDetail extends Model
{
    static public function createGame($data)
    {
        $game = new Self;
        $game->game_id = $data->id;
        // $game->title = $data->title;
        $game->user_id = $data->user_id;
        $game->hint_points = $data->hint_points;
        $game->hint = $data->hint;
        $game->story = $data->story;
        $game->objective = $data->objective;
        $game->play_time = $data->play_time;
        $game->docker = $data->docker ? $data->docker : NUll;
        $game->updated_by = Auth::user()->id;
        $game->save();
    }

    static public function updateGame($id, $data)
    {
        $game = Self::where('game_id', $id)->first();
        // $game->title = $data->title;
        $game->hint_points = $data->hint_points;
        $game->hint = $data->hint;
        $game->user_id = $data->user_id;
        $game->story = $data->story;
        $game->objective = $data->objective;
        $game->play_time = $data->play_time;
        $game->docker = $data->docker ? $data->docker : NUll;
        $game->updated_by = Auth::user()->id;
        $game->save();
    }
}
