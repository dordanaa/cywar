<?php

namespace App;

use Exception;
use PayPal\Api\Plan;
use PayPal\Api\Patch;
use PayPal\Api\Currency;
use PayPal\Api\ChargeModel;
use PayPal\Rest\ApiContext;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Auth\OAuthTokenCredential;

class PaypalPlan
{
    protected static function apiContext()
    {
        return new ApiContext(
            new OAuthTokenCredential(
                'ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V', // Client ID
                'ELqyHPegFdgPXnIawK8oKrK0MjzcobLYUWPfcKYDjSj_pBfXayWRaUmCdVmIIQelf0waFJ9YtepsI4a9' // Client Sercret
            )
        );
    } 

    static public function getPlans()
    {
        $params = array('page_size' => '5');
        return Plan::all($params, self::apiContext());
    }

    static public function showPlan($id)
    {
        return Plan::get($id, self::apiContext());
    }

    static public function createPlan($request)
    {
        $plan = self::Plan($request);

        $paymentDefinition = self::PaymentDefinition($request);

        // $chargeModel = self::chargeModel();
        // $paymentDefinition->setChargeModels(array($chargeModel));

        $merchantPreferences= self::merchantPreferences();

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        try {
            return $plan->create(self::apiContext());
        } catch (Exception $ex) {
            return false;
        }
    }

    static public function updatePlan($request)
    {
        $sub = Subscription::find($request->id);
        if($sub->status != 2) return true;

        $updatePlan = self::showPlan($request->plan_id);

        try {
            $patch = new Patch();

            $data = [
                "name" => "$request->name",
                "frequency" => "Month",
                "amount" => [
                    "currency" => "USD",
                    "value" => "$request->price"
                ]
            ];
        
            $paymentDefinitions = $updatePlan->getPaymentDefinitions();
            $paymentDefinitionId = $paymentDefinitions[0]->getId();
            $patch->setOp('replace')
                ->setPath('/payment-definitions/' . $paymentDefinitionId)
                ->setValue($data);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
        
            $updatePlan->update($patchRequest, self::apiContext());
            $plan = Plan::get($updatePlan->getId(), self::apiContext());
        } catch (Exception $ex) {
            return false;
        }
            return $plan;
    }

    static public function deletePlan($id)
    {
        $plan = self::showPlan($id);
        try {
            $plan->delete(self::apiContext());
            return response()->json('Plan deleted successfully');
        }catch(Exception $ex){
            return response()->json($ex);
        }
    }

    static public function updatePlanStatus($id, $status)
    {
        $updatePlan = self::showPlan($id);

        try {
            $patch = new Patch();
            
            if($status == 1)
            {
                $value = new PayPalModel('{
                    "state":"ACTIVE"
                }');
            }
            else if($status == 0)
            {
                $value = new PayPalModel('{
                    "state":"INACTIVE"
                }');
            }
            
            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
        
            $updatePlan->update($patchRequest, self::apiContext());
        
            $plan = Plan::get($updatePlan->getId(), self::apiContext());
        } catch (Exception $ex) {
            return false;
        }
            return $plan;
    }

    static public function activate($id)
    {
        $createdPlan = self::showPlan($id);
        
        try {
            $patch = new Patch();
            $value = new PayPalModel('{
                "state":"ACTIVE"
            }');
    
            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);

            $createdPlan->update($patchRequest, self::apiContext());

            $plan = Plan::get($createdPlan->getId(), self::apiContext());

        } catch (Exception $ex) {
            return false;
        }
        return $plan;
    }

    protected static function Plan($request)
    {
        $plan = new Plan();
        $plan->setName($request->name)
             ->setDescription($request->description)
             ->setType('fixed');
        return $plan;
    }

    protected static function PaymentDefinition($request)
    {
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
                          ->setType('REGULAR')
                          ->setFrequency('Month')
                          ->setFrequencyInterval("1")
                          ->setCycles($request->duration)
                          ->setAmount(new Currency(array('value' => $request->price, 'currency' => 'USD')));
        return $paymentDefinition;
    }

    protected static function chargeModel()
    {
        $chargeModel = new ChargeModel();
        $chargeModel->setType('SHIPPING')
                    ->setAmount(new Currency(array('value' => 0, 'currency' => 'USD')));
        return $chargeModel;
    }

    protected static function merchantPreferences()
    {
        // $baseUrl = $_SERVER['SERVER_NAME'];
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl("https://cywar.hackeru.com/agreement?status=true")
                            ->setCancelUrl("https://cywar.hackeru.com/agreement?status=false")
                            ->setAutoBillAmount("yes")
                            ->setInitialFailAmountAction("CONTINUE")
                            ->setMaxFailAttempts("0")
                            ->setSetupFee(new Currency(array('value' => 0, 'currency' => 'USD')));
        return $merchantPreferences;
    }
}

// {
//     "name": "Test Test",
//     "description": "Template creation.",
//     "type": "FIXED",
//     "payment_definitions": [
//         {
//             "id": "PD-5CD94700HP794570KYDQGTBA",
//             "name": "Regular Payments",
//             "type": "REGULAR",
//             "frequency": "Month",
//             "amount": {
//                 "currency": "USD",
//                 "value": "1"
//             },
//             "cycles": "12",
//             "charge_models": [
//                 {
//                     "id": "CHM-3PM15955FX948750CYDQGTBA",
//                     "type": "SHIPPING",
//                     "amount": {
//                         "currency": "USD",
//                         "value": "1"
//                     }
//                 }
//             ],
//             "frequency_interval": "1"
//         }
//     ],
//     "merchant_preferences": {
//         "setup_fee": {
//             "currency": "USD",
//             "value": "1"
//         },
//         "max_fail_attempts": "0",
//         "return_url": "http://127.0.0.1:8000//payment?success=true",
//         "cancel_url": "http://127.0.0.1:8000//payment?success=false",
//         "auto_bill_amount": "YES",
//         "initial_fail_amount_action": "CONTINUE"
//     },
//     "id": "P-69W001607A4386444YDQGTBA",
//     "state": "CREATED",
//     "create_time": "2019-12-01T09:52:42.884Z",
//     "update_time": "2019-12-01T09:52:42.884Z",
//     "links": [
//         {
//             "href": "https://api.sandbox.paypal.com/v1/payments/billing-plans/P-69W001607A4386444YDQGTBA",
//             "rel": "self",
//             "method": "GET"
//         }
//     ]
// }