<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class GameHint extends Model
{
    public $timestamps = false;

    static public function new($gid)
    {
        $id = Auth::user()->id;
        if(!Self::where([['game_id',$gid],['user_id', $id]])->first()) // Checks if already exists
        {
            $hint = new Self;
            $hint->game_id = $gid;
            $hint->user_id = $id;
            $hint->created_at = now();
            $hint->save();
        }
    }
}
