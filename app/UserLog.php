<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];
    
    public $timestamps = false;

    static public function new($request, $user){
        $check = Self::where('user_id', $user->id)->count();
        if(!$check){
            // First Login
        }
        // Need keeping records of all the the user logs
        // if($check >= 10){
        //     $first = Self::where('user_id', $user->id)->first();   
        //     if($first) 
        //         $first->delete();
        // }
        $log = new Self;
        $log->user_id = $user->id;
        $log->user_agent = $request->header('user-agent');
        $log->ip = $request->ip();
        $log->created_at = now();
        $log->save();
    }
}
