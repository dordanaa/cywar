<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $hidden = ['role_id'];
    public $timestamps = false;

    static public function new($id, $role)
    {
        $new = new Self;
        $new->user_id = $id;
        $new->role_id = $role;
        $new->save();
    }
}
