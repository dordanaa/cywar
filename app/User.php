<?php

namespace App;

use App\Role;
use App\Level;
use App\Token;
use App\Avatar;
use App\Lesson;
use App\Attempt;
use App\Classes;
use App\UserRole;
use App\Container;
use App\UserClass;
use App\TotalScore;
use App\MissionTask;
use App\Subscription;
use App\LessonAttempt;
use App\UserClassesLog;
use App\LessonContainer;
use App\MissionTaskAttempt;
use Illuminate\Support\Str;
use App\PracticeArenaTotalScore;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Notifications\Notifiable;
use App\Http\Controllers\APIStudentController;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    
    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function alllogs(){
        return $this->hasMany(UserLog::class, 'user_id', 'id');
    }

    public function uniteColleges(){
        return $this->hasMany(HeadmasterCollege::class, 'user_id', 'id')
                    ->join('classes AS c', 'c.college_id', 'headmaster_colleges.college_id');
    }

    public function subscription(){
        return $this->hasOne(Subscription::class, 'plan_id', 'subscription_id')->select('name', 'id', 'duration');
    }

    public function role(){
        return $this->hasOne(UserRole::class)->join('roles', 'roles.id', 'user_roles.role_id');
    }

    public function fullAddress(){
        return $this->hasOne(UserClass::class, 'user_id', 'id')
                    ->leftjoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->leftjoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->leftjoin('countries AS cr', 'cr.id', 'cg.country_id')
                    ->select('user_classes.user_id', 'cr.country', 'cg.name AS college', 'c.name AS class', 'c.id AS cid');
    }

    public function country(){
        return $this->hasOne(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->leftJoin('countries AS cr', 'cr.id', 'cg.country_id')
                    ->select('user_classes.user_id', 'cr.country');
    }

    public function colleges(){
        return $this->hasMany(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->groupBy('cg.id')
                    ->select('user_classes.user_id', 'cg.name');
    }
    
    public function college(){
        return $this->hasOne(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->select('user_classes.user_id', 'cg.name');
    }
    
    public function fullAddresses(){
        return $this->hasMany(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->leftJoin('countries AS cr', 'cr.id', 'cg.country_id')
                    ->select('user_classes.user_id', 'cr.country', 'cg.name AS college', 'c.name AS class', 'c.status AS status', 'c.id AS cid', 'c.active_2fa AS class_2fa');
    }
    
    public function classes(){
        return $this->hasMany(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->select('user_classes.user_id', 'c.name', 'c.id', 'c.active_2fa');
    }

    public function class(){
        return $this->hasOne(UserClass::class, 'user_id', 'id')
                    ->leftJoin('classes AS c', 'c.id', 'user_classes.class_id')
                    ->select('user_classes.user_id', 'c.name', 'c.id', 'c.active_2fa');
    }

    public function gamesUnallowed(){
        return $this->hasMany(UserAllowedGame::class, 'user_id', 'id')
                    ->join('games AS g', 'g.id', 'user_unavailable_games.game_id')
                    ->select('user_unavailable_games.user_id', 'g.id', 'g.name');
    }

    public function played(){
        return $this->hasMany(Attempt::class, 'user_id', 'id');
    }

    public function playedPractice(){
        return $this->hasMany(LessonAttempt::class, 'user_id', 'id')
                    ->join('lessons AS l', 'l.id', 'lesson_attempts.lesson_id')
                    ->join('modules AS m', 'm.id', 'l.module_id')
                    ->select('l.name AS lesson', 'm.name AS module', 'lesson_attempts.status', 'lesson_attempts.points', 'lesson_attempts.user_id');
    }

    public function finished(){
        return $this->hasMany(Attempt::class, 'user_id', 'id')->where('status', 1);
    }

    public function finishedPractice(){
        return $this->hasMany(LessonAttempt::class, 'user_id', 'id')->where('status', 1);
    }

    public function failed(){
        return $this->hasMany(Attempt::class, 'user_id', 'id')->where('status', 0);
    }

    public function hints(){
        return $this->hasMany(GameHint::class, 'user_id', 'id');
    }

    public function hintsPractice(){
        return $this->hasMany(MissionTaskHint::class, 'user_id', 'id');
    }

    public function lessonsAllowed(){
        return $this->hasMany(UserAvailableLesson::class, 'user_id', 'id')
                    ->join('lessons AS l', 'l.id', 'user_available_lessons.lesson_id')
                    ->select('user_available_lessons.user_id', 'l.name', 'l.id');
    }

    public function points(){
        return $this->hasOne(TotalScore::class, 'user_id', 'id')->select('user_id', 'total_points');
    }

    public function lastLogged(){
        return $this->hasOne(UserLog::class, 'user_id', 'id')->select('user_id', 'created_at')->orderBy('created_at', 'desc');
    }

    public function lastAttempt(){
        return $this->hasOne(AttemptLog::class, 'user_id', 'id')->select('user_id', 'created_at')->orderBy('created_at', 'desc');
    }

    public function lastIp(){
        return $this->hasOne(UserLog::class, 'user_id', 'id')->select('user_id', 'ip')->orderBy('created_at', 'desc');
    }

    public function games(){
        return $this->hasMany(Attempt::class, 'user_id', 'id')
                    ->join('games AS g', 'g.id', 'attempts.game_id')
                    ->join('categories AS c', 'c.id', 'g.category_id')
                    ->select('attempts.user_id', 'attempts.game_id', 'attempts.points', 'c.name AS category', 'g.name', 'attempts.status', 'attempts.attempts', 'attempts.locked');
    }

    static public function signup($r)
    {
        $user = new User;
        $user->subscription_id = $r->subscription_id ? $r->subscription_id : 6;
        $user->name = $r->name;
        $user->phone = $r->phone;
        $user->email = $r->email;
        $user->status = isset($r->class) && $r->class ? 2 : 3;
        $user->password = bcrypt($r->password);
        $user->save();
  
        if($user){
          UserRole::insert(['user_id' => $user->id, 'role_id' => 33]);
          if(isset($r->class) && $r->class)
        //   if(isset($r->class) && $r->class && $r->sub_name == 'Normal Class')
          {
            $class = Classes::where('name', $r->class)->first(['id']);
            UserClass::insert([['user_id' => $user->id, 'class_id' => $class->id]]);
            UserClassesLog::new($user->id, $class->id);
          }

          
            // One time url to recieve the QR for 2fa authentication
            $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
            $key = $google2fa->generateSecretKey();
            $QRURL = $google2fa->getQRCodeInline(
                'Cywar HackerU',
                $user->email,
                $key
            );
            $user->qrcode = $QRURL;
            User::where('id', $user->id)->update(['google2fa_secret' => $key]);

            // Email Verification, with token authentication
            $verify = EmailVerification::create([
                'email' => $user->email, 
                'token' => Str::random(50)
            ]);
            $user->token = $verify->token; 
            return $user;
        }
          return false;
    }

    static public function updateUser($request)
    {
        $user = Self::find(Auth::user()->id);
        if($request->old_password || $request->password){ // Confirms Old Password
            if( ! Hash::check($request->old_password, $user->password)){
                return ['Old password is incorrect', 404];
            }
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if($request->phone) $user->phone = $request->phone;
        if($request->password) $user->password = bcrypt($request->password);
        $user->updated_at = now();
        $user->updated_by = $user->id;

        if($user->image != $request->image){ // Check if the avatar is truley available for the user
            $avatar = Avatar::where('image', $request->image)->first(['level', 'image']);
            if(self::level() >= $avatar->level || Auth::user()->role->role != 'student'){
                $user->image = $avatar->image;
            }else{
                return ['Unabled To Updated The Avatar', 400];
            }
        }
        
        $user->save();
        return ['User updated successfully', 200];
    }

    static public function filter($request, $arr, &$data)
    {
        $data = [];
        $students = Self::join('user_roles AS ur', 'ur.user_id', 'users.id')
                        ->join('roles AS r', 'r.id', 'ur.role_id')
                        ->leftJoin('subscriptions AS s', 's.id', 'users.subscription_id')
                        ->where('r.role', 'student')
                        ->with('points')
                        ->leftJoin('user_classes AS uc', 'uc.user_id', 'users.id')
                        ->Leftjoin('classes AS c', 'c.id', 'uc.class_id')
                        ->Leftjoin('colleges AS cg', 'cg.id', 'c.college_id')
                        ->Leftjoin('countries AS cr', 'cr.id', 'cg.country_id');

        $role = Auth::user()->role->role;
        if ($role != 'admin') $students = $students->whereIn('c.id', $arr['classes']); // Filters The Students by the user Authorization

        if(isset($request->status) && $request->status != 3) $students = $students->where('users.status', $request->status); // If Status is filtered
        if(isset($request->class) && $request->class != 0){
            $students = $students->where('uc.class_id', $request->class); // If filtered by class
        }
        else{
            if(isset($request->college) && $request->college != 0){
                $students = $students->where('cg.id', $request->college);   // If filtered by college
            }else{
                if(isset($request->country) && $request->country != 0){  // If filtered only by country
                    $students = $students->where('cr.id', $request->country);
                }
            }
        }

        $data = $students->select('users.id', 'users.status', 's.name AS sub', 'users.name', 'users.created_at', 'users.email', 'users.phone', 'cg.name AS college', 'c.name AS class', 'cr.country')
                                     ->orderBy('users.created_at')->paginate($request->pagination);
    }

    static public function getStudents($status, $arr, &$data, $pagination, $request) // Get Only Students
    {
        $data = Self::join('user_roles AS ur', 'ur.user_id', 'users.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->where('r.role', 'student')
                    ->leftJoin('subscriptions AS s', 's.id', 'users.subscription_id')
                    ->leftJoin('total_scores AS ts', 'ts.user_id', 'users.id')
                    // ->with('points', 'lastIp')
                    ->Leftjoin('user_classes AS uc', 'uc.user_id', 'users.id')
                    ->Leftjoin('classes AS c', 'c.id', 'uc.class_id')
                    ->Leftjoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->Leftjoin('countries AS cr', 'cr.id', 'cg.country_id');

        if($status != 'all') $data = $data->where('users.status', $status);

        $role = Auth::user()->role->role;
        if ($role != 'admin') $data = $data->whereIn('c.id', $arr['classes']); // Filters The Students by the user Authorization
        
        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->select('users.id', 'users.name', 's.name AS sub', 'ts.total_points AS points', 'users.status', 'users.created_at', 'users.email', 'users.email_verified_at', 'users.phone', 'cg.name AS college', 'c.name AS class', 'cr.country')
                     ->orderBy('users.created_at')->paginate($pagination);
    }

    static public function getUsers($status, $pagination, $arr, $request) // Get All Users But Students
    {
        $data = Self::join('user_roles AS ur', 'ur.user_id', 'users.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->where('r.role', '!=', 'student');

        if($status != 'all') $data = $data->where('users.status', $status);

        $data = $data->leftJoin('user_classes AS uc', 'uc.user_id', 'users.id')
                     ->leftJoin('classes AS c', 'c.id', 'uc.class_id')
                     ->Leftjoin('colleges AS cg', 'cg.id', 'c.college_id')
                     ->Leftjoin('countries AS cr', 'cr.id', 'cg.country_id');

        $role = Auth::user()->role->role;
        if ($role != 'admin') $data = $data->whereIn('c.id', $arr['classes']); // Filters The Students by the user Authorization
        
        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $students = $data->select('users.*', 'cr.country', 'r.role')->groupBy('users.id')->paginate($pagination);
        return $students;
    }

    static public function showStudent($id)
    {
        $user = User::where('id', $id)
                    ->with('fullAddress', 'role', 'games', 'gamesUnallowed', 'lastLogged', 'lastAttempt', 'lastIp', 'subscription', 'playedPractice', 'lessonsAllowed')
                    ->withCount('played', 'finished', 'hints', 'finishedPractice', 'hintsPractice')
                    ->first();
        $user->details = User::details($id);

        if(!$user) return ['The Student is not found', 400];
        if($user->role->role != 'student') return ['The User is not a student', 400];

        if(self::checkAuthorization($user)){
            return [$user, 200];
        }else{
            return ['The Student is not under your Authorization', 400];
        }
    }

    static public function showUser($id)
    {
        $user = User::where('id', $id)
                    ->with('fullAddresses', 'role', 'games', 'gamesUnallowed', 'lastLogged', 'lastAttempt', 'lastIp', 'playedPractice')
                    ->withCount('played', 'finished', 'hints', 'finishedPractice', 'hintsPractice')
                    ->first();

        $user->details = User::details($id);

        if(!$user) return ['The User is not found', 400];
        if($user->role->role == 'student') return ['The User is a student', 400];

        if(self::checkAuthorization($user)){
            return [$user, 200];
        }else{
            return ['The User is not under your Authorization', 400];
        }
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $user = Self::find($id);
                if(! self::checkAuthorization($user)) return ['The Students are not under your Authorization', 400];
                self::fullDelete($id);
            }
            return ['Students have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;

            foreach($request->data AS $id){
                $user = Self::find($id);
                if(! self::checkAuthorization($user)) return ['The Students are not under your Authorization', 400];
            }

            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);

            return ['The Students Status Updated Successfully', 200];

        }
    }

    static public function updateProfile($request)
    {
        $user = Self::find($request->id);
        if($user && self::checkAuthorization($user))
        {

            $user->name = $request->name;
            $user->email = $request->email;
            $user->active_2fa = $request->active_2fa;
            $user->updated_at = now();
            $user->updated_by = Auth::user()->id;
            $user->phone = $request->phone;
            $user->save();

            if($request->role && $request->role != $user->role->id){
                if(Role::where('id', $request->role)->first())
                {
                    UserRole::where('user_id', $request->id)->update([
                        'role_id' => $request->role
                    ]);
                }
                $token = Token::where('user_id', $user->id)->where('revoked', 0)->orderBy('created_at', 'desc')->first();
                if($token) $token->update(['revoked' => 1, 'updated_at' => now()]);
            }
            return ["$user->name Profile Updated Successfully", 200];
        }else{
            return ['Cannot Update User Profile Off Your Authorization', 400];
        }
    }
    
    static public function updatePassword($request)
    {
        $user = Self::find($request->id);
        if($user && self::checkAuthorization($user))
        {
            $user->password = bcrypt($request->password);
            $user->updated_at = now();
            $user->updated_by = Auth::user()->id;
            $user->save();

            DB::table('oauth_access_tokens')->where('user_id', $request->id)->update(['revoked' => 1]);

            return ["$user->name Password Updated Successfully", 200];
        }else{
            return ['Cannot Update User Password Off Your Authorization', 400];
        }
    }

    static public function updateStatus($request)
    {
        $user = Self::find($request->id);
        if(self::checkAuthorization($user))
        {
            $role = Auth::user()->role->id;
            // if($role != 'admin' && $role == $user->role->id) return ['Cannot Update A User Profile Off Your Authorization', 400];

            $user->status = $request->status;
            $user->updated_at = now();
            $user->updated_by = Auth::user()->id;
            $user->save();

            if($request->status == 0) DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();
            return ['User Status Updated Successfully', 200];
        }else{
            return ['Cannot Update The User Status Off Your Authorization', 400];
        }
    }

    static public function createUser($request)
    {
        $creator = Auth::user();

        // Check if the user profile is valid
        if($creator->role->role != 'admin')
        {
            if(!in_array($request->role, [58, 67]))
            {
                return ['You Are Able To Create Only Teacher Or Head Master Users', 400]; 
            }
        } 
        $user = new Self;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->status = $request->status == true ? 1 : 0;
        $user->created_by = $creator->id;
        $user->save();

        if($user)
        {
            UserRole::new($user->id, $request->role);
            $request->id = $user->id;
            if(in_array($request->role, [58, 67]) && isset($request->classes)) UserClass::updateMultipleClasses($request);
            return ['User Created Successfully', 201];
        }
        else
        {
            return ['Sorry, Unabled To Create A New User, Please Let Us Know', 400];
        }
    }

    static function fullDelete($id) // Delete User by the GDPR
    {
        DB::delete("DELETE u.*, al.*, c.*, gh.*, sp.*, ts.*, a.*, ul.*, pr.* FROM users AS u "
                    . "LEFT JOIN attempts AS a ON a.user_id = u.id "
                    . "LEFT JOIN user_roles AS ur ON ur.user_id = u.id "
                    . "LEFT JOIN attempt_logs AS al ON al.user_id = u.id "
                    . "LEFT JOIN containers AS c ON c.user_id = u.id "
                    . "LEFT JOIN game_hints AS gh ON gh.user_id = u.id "
                    . "LEFT JOIN supports AS sp ON sp.user_id = u.id "
                    . "LEFT JOIN total_scores AS ts ON ts.user_id = u.id "
                    . "LEFT JOIN user_logs AS ul ON ul.user_id = u.id "
                    . "LEFT JOIN password_resets AS pr ON pr.email = u.email "
                    . "WHERE u.id = ?", [$id]);
    }

    private static function level() // Returns user current level 
    {
        $points = TotalScore::where('user_id', Auth::user()->id)->first(['total_points']);
        $points = $points ? $points->total_points : 0;  // If user have not played yet

        $level = Level::where('points', '<=', $points)->orderBy('points', 'desc')->first();

        return $level->level;
    }

    static public function details($id) // Return user current Level, Points & Next Level Points
    {
        $data = [];
        $points = TotalScore::where('user_id', $id)->first(['total_points']);
        $data['points'] = $points ? $points->total_points : 0;  // If user have not played yet
        $data['level'] = Level::where('points', '<=', $data['points'])->orderBy('points', 'desc')->first(['level', 'points', 'next_level']);
        return $data;
    }

    static public function practice_arena_details($id) // Return the user practice arena progress, and lessons status
    {
        $data = [];
        $points = PracticeArenaTotalScore::where('user_id', $id)->first(['total_points']);
        $data['points'] = $points ? $points->total_points : 0;  // If user have not played yet
        $data['lessons'] = Lesson::where('status', 1)->count(); 
        $data['lessons_complete'] = LessonAttempt::where('user_id', $id)->where('status', 1)->count(); 
        $data['lessons_failed'] = LessonAttempt::where('user_id', $id)->where('status', 0)->count(); 
        $data['hints'] = MissionTaskHint::where('user_id', $id)->count(); 
        $data['tasks'] = MissionTask::count(); 
        $data['tasks_complete'] = MissionTaskAttempt::where('status', 1)->where('user_id', $id)->count(); 
        return $data;
    }

    static public function checkAuthorization($user) // Check if the user is under the Authorization of the user
    {
        $class = $user->class ? $user->class->id : false;
        $role = Auth::user()->role->role;
        // if admin can have access to all
        if($role == 'admin') 
        {
            return true;
        }
        // the user cannot update another user the same role as his
        else if($role == $user->role->role) return false; 
        // role master can update all the students & teacher in his colleges / classes 
        elseif($role == 'master') 
        {
            if($user->role->role == $role && $user->id != Auth::user()->id) return false;
            $data = self::IdOfClasses();
            if(in_array($class, $data)) return true;
        }
        // role teacher can update only the students in his classes
        elseif($role == 'teacher') 
        {
            if($user->role->role == $role && $user->id != Auth::user()->id) return false;
            $data = self::IdOfClasses();
            if(in_array($class, $data)) return true;
        }

            return false;
    }

    static public function IdOfClasses() // return all the user classes in array of id => Example: [7,13,45,84]
    {
        $classes = Auth::user()->classes;
        $data = [];
        foreach($classes AS $item){
            $data[] = $item['id'];
        }
        return $data;
    }
    
    static public function getLiveDockers($id, &$data)
    {
        $date = new \DateTime();
        $date->modify('-3 hours');
        $formatted_date = $date->format('Y-m-d H:i:s');
        $data['challenges'] = Container::where('containers.user_id', $id)
                                       ->join('games AS g', 'g.id', 'containers.game_id')
                                       ->join('categories AS c', 'c.id', 'g.category_id')
                                       ->where('containers.created_at', '>', $formatted_date)
                                       ->get(['containers.id','containers.game_id', 'containers.port', 'containers.ip', 'containers.created_at', 'g.name AS game', 'c.name AS category',]);

        $data['lessons'] = LessonContainer::where('lesson_containers.user_id', $id)
                                       ->join('lessons AS l', 'l.id', 'lesson_containers.lesson_id')
                                       ->join('modules AS m', 'm.id', 'l.module_id')
                                       ->where('lesson_containers.created_at', '>', $formatted_date)
                                       ->get(['lesson_containers.id', 'lesson_containers.lesson_id', 'lesson_containers.container_id', 'lesson_containers.port', 'lesson_containers.ip', 'lesson_containers.created_at', 'l.name AS lesson', 'm.name AS module']);
    }

    static public function getStatistics($id, &$data)
    {
        $data['categories'] = Attempt::where('user_id', $id)
                                     ->join('games AS g', 'g.id', 'attempts.game_id')
                                     ->join('categories AS c', 'c.id', 'g.category_id')
                                     ->groupBy('g.category_id')
                                     ->orderBy('total', 'desc')
                                     ->select('c.name', DB::raw('count(*) as total'))
                                     ->get();
    }
    
    static public function closeDocker($request, $id)
    {
        if( self::checkAuthorization(Self::find($id)) )
        {
            DB::table($request->table)->where('id', $request->id)->delete();
            return ['Container Has Been Deleted', 200];
        }
        return ['The Student is not under your Authorization', 400];
    }
    
    static public function resetActivities($request)
    {
        if( self::checkAuthorization(Self::find($request->id)) )
        {
            foreach($request->tables AS $table)
            {
                if($request->data == 'all') DB::table($table)->where('user_id', $request->id)->delete();
                else
                {
                    if($table == 'lesson_attempts') DB::table($table)->where('user_id', $request->id)->whereIn('id', $request->data)->delete();
                    else if(in_array($table, ['mission_task_attempts', 'mission_task_hints'])) DB::table($table)->where('user_id', $request->id)->whereIn('lesson_id', $request->data)->delete();
                    else if($table == 'attempts') DB::table($table)->where('user_id', $request->id)->whereIn('game_id', $request->data)->delete();
                }
            }
            return ['User activities has been deleted', 200];
        }
        return ['The User is not under your Authorization', 400];
    }
    
    static public function createUserFromCampus($student)
    {
        $user = Self::where('email', $student['semail'])->first(['id']);
        if(!$user){
            $user = new Self;
            $user->email = $student['semail'];
            $user->name = $student['sname'];
            $user->password = $student['spassword'];
            $user->created_at = $student['created_at'];
            $user->updated_at = $student['updated_at'];
            $user->status = $student['active'];
            $user->save();

            UserRole::insert([
                'user_id' => $user->id,
                'role_id' => 33,
            ]);

            if($student['campname'] == 'קמפוס מרכז'){
                $college = 1;
            }elseif ($student['campname'] == 'קמפוס צפון') {
                $college = 6;
            }elseif($student['campname'] == 'קמפוס ירושלים'){
                $college = 10;
            }elseif($student['campname'] == 'קמפוס אילת'){
                $college = 11;
            }else{
                $college = 5;
            }

            \preg_match('/\S*\d+\S*/',$student['gname'], $class_name);
            
            $class = Classes::firstOrCreate(
                ['name' => $class_name[0]],
                ['status' => 1, 'created_by' => 1, 'college_id' => $college, 'created_at' => now(), 'updated_at' => now(), 'status' => 1]
            );
            
            UserClass::create([
                'user_id' => $user->id,
                'class_id' => $class->id,
            ]);
            
            UserClassesLog::new($user->id, $class->id);

        }else{
            $user->status = $student['active'];
            $user->save();
        }
        return 'OKAY';
    }

    static public function updateStatusFromCampus($request)
    {
        $user = User::where('email', $request->email)->first();
        return $user ? true : false;
        $user->status = $request->active;
        $user->save();
        return true;
    }

    static function createJcCandidate($request) {
        
        $userExists = Self::where('email',$request->candidate_email)->first();
        if($userExists) {

            $user = self::find($userExists->id);
            $user->updated_at = now();
            $user->save();

           return self::encrypt($userExists, $request);
        }
        $user = new Self;
        $user->name = 'JC-'.$request->justcode_user_id;
        $user->email = $request->candidate_email;
        $user->password = bcrypt('jc4life');
        $user->status = 1;
        $user->created_by = 0;
        $user->save();
        if($user)
        {
            UserRole::new($user->id, 149);
            $data = [
                
            ];
            UserAllowedGame::insert($data);
            return self::encrypt($user, $request);
        }
        else
        {
            return ['Sorry, Unable To Create A New User, Please Let Us Know', 400];
        }
    }
    
    static function encrypt($user, $request) {
        $userJc = $user->email.'|Galex|'.$request->hackeru_exam.'|Galex|'.now().'|Galex|'.$request->duration_in_mins;
        unset($request['token']);
        unset($request['duration_in_mins']);
        $userJcAndRequestArr = $userJc.'|Galex|'.Crypt::encrypt($request->toArray());
        //list(,$link,) = explode('|Galex|',$userJc);
        // $gameData = GamesController::GetGameStructure('0Q8k3wKBJR6dTcqtq5Bp6f5sSNKg0FFG',1,1000, true);
        // if(!isset($gameData[$link])) {
        //    return response()->json(['error' => 'Game does not exists'],404);
        // }
        // // We bring all game with thier url and id
        // setcookie('jc',Crypt::encrypt($request->toArray()),time() + 60 * 60 * 24,'/api/games/'.$gameData[$link]); //We create a new cookie with info for the gam
        $token = APIStudentController::hashDecrypted($userJcAndRequestArr,'e'); // we create login token for the candidate
        return ['candidate_token' => $token];
        //$host = request()->getSchemeAndHttpHost();
        //header("Location: $host/login-jc?token=".$token);
        //exit;
    }

}
