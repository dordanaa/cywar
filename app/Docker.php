<?php

namespace App;

use Exception;
use App\Container;
use App\GameDetail;
use App\LessonContainer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Docker extends Model
{
    static public function run($id)
    {
        $game = GameDetail::where('game_id', $id)->first();
        if($game)
        {
            $ip = $game->docker;
            if(!$ip) return ['Challenge has no docker', 400];
        }else
        {
            return ['Sorry, haven\'t found the challenge', 400]; 
        }

        $check = self::check($game); // Checks if there is a container available for this game
        if($check)
        {
            return [$check, 200];
        }else{
            if($id == 151)
            {
                $docker = self::activateDocker($ip, $game, 'client'); // Send a request to open a new container
                $docker->time = '02:59:59';
                $docker2 = self::activateDocker($ip, $game, 'server'); // Send a request to open a new container
                $docker2->time = '02:59:59';
            }else
            {
                $docker = self::activateDocker($ip, $game); // Send a request to open a new container
            }
        }


        if(isset($docker->error)){
            return [$docker, 400];
        }else{
            if($docker)
            {
                Container::newContainer($docker, $game->game_id); // Creates a new container record
                if($id == 151) return [[$docker, $docker2], 200];
                // $docker->model = 'Container';
                // $pulse = self::checkPulse($docker);
                // if($pulse[0] != 'Success') return ["Sorry Challenge Is Unavailable For Now, Please Try Again $pulse[0]", 400];
                $docker->time = self::secToHR($game->play_time);
                return [$docker, 200];
            }else{
                return ['Sorry Challenge Is Unavailable For Now, Please Try Again Soon', 400];
            }
        }
    }

    static public function runPractice($id)
    {
        $lesson = Lesson::find($id);
        if($lesson)
        {
            $ip = $lesson->docker;
            if(!$ip) return ['COMING SOON', 400];
        }else
        {   
            return ['Sorry, haven\'t found the lesson', 400]; 
        }

        $check = self::checkPractice($lesson); // Checks if there is a container available for this lesson
        if($check)
        {
            return [$check, 200];
        }else{
            if($id == 236)
            {
                $docker = self::activateDocker($ip, $lesson, 'cisco'); // Send a request to open a new container
            }else
            {
                $docker = self::activateDocker($ip, $lesson); // Send a request to open a new container
            }
        }

        if(isset($docker->error)){
            return [$docker, 400];
        }else{
            if($docker)
            {
                LessonContainer::newContainer($docker, $lesson->id); // Creates a new container record
                $docker->time = self::secToHR($lesson->play_time);
                return [$docker, 200];
            }else{
                return ['Sorry Lesson Is Unavailable For Now, Please Try Again Soon', 400];
            }
        }
    }

    static private function activateDocker($ip, $game, $type = '')
    {
        $playtime = $game->play_time;
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://crang.hackeruarena.com/start-container/?ip=' . urlencode("$ip&$type&playtime=$playtime"));
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, 
                [
                    'Authorization: Token Qdr6e8OsEbQQDRYQozmzvpLXCPjmyboQ'
                ]                                                                         
            ); 
            $result = curl_exec($curl);
            $result = json_decode($result);
            // if (curl_errno($curl)) {
            // $error_msg = curl_error($curl);
            //     dd($error_msg, 'https://crang.hackeruarena.com/start-container/?ip=' . $ip . '&playtime=' . $playtime);
            // }
            curl_close($curl);
            
            return $result;
            
        }catch(Exception $ex){
         
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $ip . "&$type" . '&playtime=' . $playtime);
            curl_setopt($curl, CURLOPT_PORT, 8181);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, 
                [
                    'Authorization: Token Qdr6e8OsEbQQDRYQozmzvpLXCPjmyboQ'
                ]                                                                         
            ); 
            $result = curl_exec($curl);
            $result = json_decode($result);
            curl_close($curl);
            
            return $result;
            
        }
    }

    static private function check($game)
    {

        $play_time = $game->play_time;
        $games = Container::where('game_id', $game->game_id)->groupBy('port')->get();
        if(count($games)){ // If container exists
            $deleted = false;
            foreach($games AS $game){
                $old = new Carbon($game->created_at);
                $currentTime = Carbon::now();
                if($currentTime->diffInMinutes($old) > $play_time){ // If docker expired
                    Container::where('port', $game->port)->delete();
                    $deleted = true;
                }
            }
            if($deleted) return false;
        }else{
            return false; // If no containers opened, function stopped 
        }

        if( $container = Container::where('user_id', Auth::user()->id)->where('game_id', $game->game_id)->first() ) 
        {
            $currentTime = Carbon::now();
            $firstContainerTime = Container::where('game_id', $game->game_id)->where('port', $container->port)->first(['created_at'])->created_at;
            $checkTime = $play_time - $currentTime->diffInMinutes($firstContainerTime);
          
            if($checkTime > 0) {
                $res = self::secToHR($checkTime);
                return [
                    'time' => $res,
                    'ip' => $container->ip,
                    'port' => $container->port
                ];
            }
        }

        $docker = $games->last();
        if(Container::where([['game_id', $game->game_id], ['port', $docker->port]])->count() == 10) // Maximum of 10 players per container
        {
            return false;
        }
        else
        {
            Container::newContainer($docker, $game->game_id); // Creates a new container record

            $currentTime = Carbon::now();
            $checkTime = $play_time - $currentTime->diffInMinutes($docker->created_at);
            if($checkTime > 0) {
                $res = self::secToHR($checkTime);
                return [
                    'time' => $res,
                    'ip' => $docker->ip,
                    'port' => $docker->port
                ];
            }
        }
    }

    static private function checkPractice($lesson)
    {
        $play_time = $lesson->play_time;
        $lessons = LessonContainer::where('lesson_id', $lesson->id)->groupBy('port')->get();
        if(count($lessons)){
            $deleted = false;
            foreach($lessons AS $lesson){
                $old = new Carbon($lesson->created_at);
                $currentTime = Carbon::now();
                if($currentTime->diffInMinutes($old) > $play_time){
                    LessonContainer::where('port', $lesson->port)->delete();
                    $deleted = true;
                }
           }
           if($deleted) return false;
        }
        else{
            return false;
        }

        $docker = $lessons->last();
        if($container = LessonContainer::where([['lesson_id', $docker->lesson_id], ['user_id', Auth::user()->id]])->first()) // if already has an open container
        {
            $container->time = self::secToHR($play_time);
            return $container;
        }
        else
        {
            return false;
        }
    }
    
    static private function secToHR($time) {
        $hours = floor(($time / 60) % 60);
        $minutes = $time % 60;
        $minutes = strlen($minutes) > 1 ? $minutes : "0$minutes";
        $seconds = $hours == 3 ? 0 : 59;
        $time = "0$hours:$minutes:$seconds";
        return $time;
    }

    static public function testTask($request)
    {
        $ip = $request['docker']['ip'];
        $ip = "https://$ip/lessonCheck";
        $container = LessonContainer::where('lesson_id', $request['docker']['id'])->where('user_id', Auth::user()->id)->first();
        if(!$container) return [
            'content' => 'Sorry, Container not found',
            'status' => 0
        ];
        $data = [
            'id' => $container->container_id,
            'mission' => $request['docker']['missionNum'],
            'check' => $request['docker']['checkNum'],
            'token' => 'Qdr6e8OsEbQQDRYQozmzvpLXCPjmyboQ'
        ];
        $data = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $ip);
        curl_setopt($curl, CURLOPT_PORT, 8181);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20);
        curl_setopt($curl, CURLOPT_HTTPHEADER, [                                                                         
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($data),
            'Authorization: Token Qdr6e8OsEbQQDRYQozmzvpLXCPjmyboQ'
            ]
        );    
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec($curl);
        // if (curl_errno($curl)) {
        //     $error_msg = curl_error($curl);
        //     dd($error_msg);
        // }
        $result = json_decode($result);
        curl_close($curl);
        return $result;
    }

    static public function checkPulse($r)
    {
        $http = $r->model == 'LessonContainer' ? 'https' : 'http';

        // $ip = "$http://13.59.192.100:50020";
        $ip = "$http://$r->ip:$r->port";
        for($i = 0; $i < 6; $i++){
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $ip);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            curl_exec($curl);
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
    
            if($statusCode != 0) return [true, 200];
            sleep(1);
        }
            return [false, 404];
    }

    static public function createRDPFile($request)
    {
        $ip = $request->ip;

        $content = "auto connect:i:1
full address:s:$ip
username:s:Student
password 51:b:01000000d08c9ddf0115d1118c7a00c04fc297eb01000000ea3c0fe42a05044c84e04f9c93daf5b700000000020000000000106600000001000020000000dfd304939556df6319090310f438fb988cf51b8bafb49f8f5215c7616141b17c000000000e800000000200002000000017945eaf99236cc73f57cafcadbc724308b9a3535300e023fdc3386c63158a3a80000000fedf556213308a4932242a0dc3b8860c5b8f122351a6f1e2ad500b3436714e89370daec995bbebcd52c3e340a72a53df06810e2b77f14718f33129f80eef42814be42b7cc5117628c18df7f30a58fb47062f4e6bc8a19afc25949952af3f8327b8efc1c1f18894482628c2524aed2a2afc93e441ce0b841bf1b51952940cd6294000000079349f8ccbf7541bf8b4c817fa92c6a973409ed1a538007684cac78583674223b59df8b4bed48f980fdcf8f3702208741cb54bd3c7f57f409c33dc95901768db
        ";

        $path = 'lesson_files\\' . str_replace(' ', '_', Auth::user()->name) . '_' . str_replace(' ', '_', $request->name) . '_' . $ip . "_Connection.rdp";
        Storage::disk('pub')->put( $path, $content);
        return $path;
    }
}
