<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;

class ImageLoader extends Model
{
    static public function new($request, $file_name, $path)
    {
        try{
            $image_name = null;
            if( $request->hasFile($file_name) && $request->file($file_name)->isValid() ){
                
                $file = $request->file($file_name);
                if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return false;
                $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
                $request->file($file_name)->move(public_path() . $path, $image_name);
                return $image_name;
            }
        }catch(Exception $ex){
            return false;
        }
    }
}
