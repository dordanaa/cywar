<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class PracticeArenaTotalScore extends Model
{
    static public function set($points)
    {
        $id = Auth::user()->id;
        $user = Self::where('user_id', $id)->first();
        if($user)
        {
            $user->total_points = $user->total_points + $points;
            $user->updated_at = now();
        }
        else
        {
            $new = new Self;
            $new->total_points = $points;
            $new->user_id = $id;
            $new->lessons = 0;
            $new->save();
        }
    }
}
