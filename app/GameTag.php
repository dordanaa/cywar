<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameTag extends Model
{
    public $timestamps = false;

    static public function newGame($data, $id)
    {
        $tags = [];
        foreach(json_decode($data) AS $tag)
        {
            $tags[] = [
                'game_id' => $id,
                'tag_id' => $tag
            ];
        }

        Self::insert($tags);
    }
}
