<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $casts = [
        // 'created_at' => 'datetime:Y-m-d',
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id')->select('id', 'name');
    }

    public function subscription(){
        return $this->hasOne(Subscription::class, 'plan_id', 'subscription_id')->select('name', 'id', 'duration', 'price');
    }

    static public function getAll($request, &$data)
    {
        $data = Order::where('status', $request->status)
                     ->with('user', 'subscription')   
                     ->orderBy('created_at', 'desc')
                     ->paginate($request->pagination);
    }

    static public function new($data)
    {
        $user = Auth::user();
        $order = new Self;
        $order->user_id = $user->id;
        $order->subscription_id = $user->subscription_id;
        $order->order_id = $data->id;
        $order->payer_id = $data->payer->payer_info->payer_id;
        $order->price = $data->plan->merchant_preferences->setup_fee->value;
        $order->status = 1;
        $order->save();

        // User is now an active member after he finishes his payment
        if($order)
        {
            $user->update(['status' => 1, 'updated_at' => now()]);
        }
    }
}
