<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    static public function createNew($request)
    {
        $type = new Self;
        $type->name = $request->name;
        $type->color = $request->color;
        $type->save();
    }
    
    static public function updateType($request)
    {
        $type = Self::find($request->id);
        $type->name = $request->name;
        $type->color = $request->color;
        $type->save();
    }

    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($id);
            }
            return ['Notification Types have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Notification Types status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function remove($id)
    {
        $sup = Self::find($id);
        if($sup){
            $sup->delete();
            return ['Notification Type Deleted Successfully', 200];
        }
        return ['Notification Type Not Found', 404];
        
    }
}
