<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    
    public function colleges(){
        return $this->hasMany(College::class, 'country_id', 'id');
    }
    
    public function classes(){
        return $this->hasMany(College::class, 'country_id', 'id')
                    ->rightjoin('classes AS c', 'c.college_id', 'colleges.id')
                    ->select('country_id');
    }
    
    public function students(){
        return $this->hasMany(College::class, 'country_id', 'id')
                    ->rightjoin('classes AS c', 'c.college_id', 'colleges.id')
                    ->rightjoin('user_classes AS uc', 'uc.class_id', 'c.id')
                    ->rightjoin('users AS u', 'u.id', 'uc.user_id')
                    ->rightjoin('user_roles AS ur', 'ur.user_id', 'uc.user_id')
                    ->rightjoin('roles AS r', 'r.id', 'ur.role_id')
                    ->where('r.role', 'student')
                    ->select('country_id');
    }
    
    public function users(){
        return $this->hasMany(College::class, 'country_id', 'id')
                    ->join('classes AS c', 'colleges.id', 'c.college_id')
                    ->join('user_classes AS uc', 'uc.class_id', 'c.id')
                    ->join('users AS u', 'u.id', 'uc.user_id')
                    ->join('user_roles AS ur', 'ur.user_id', 'uc.user_id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->select('country_id','role', 'u.status');
    }

    static public function createNew($request)
    {
        $country = new Self;
        $country->country = $request->name;
        $country->status = $request->status ? 1 : 0;
        $country->save();
    }

    static public function updateCountry($request)
    {
        $country = Self::find($request->id);
        $country->country = $request->country;
        $country->save();
    }

    static public function getColleges($id, $pagination, &$data)
    {
        $data = Self::where('countries.id', $id)
                    ->join('colleges AS c', 'c.country_id', 'countries.id')
                    ->paginate($pagination);
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['Country Cannot Be Deleted While Having Games', 400]; 
            }
            return ['Countries have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Countries status has been changed successfully to $request->mode", 200];
        }
    }

    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }
    
    static public function remove($id)
    {
        // If Country has colleges cannot be deleted
        $country = Self::where('id', $id)->with('colleges')->first();
        if(count($country->colleges) > 0) return ['Cannot Delete Country With Colleges, Please Remove All Classes First', 400];

        DB::delete("DELETE c.* FROM countries AS c "
                    . "WHERE c.id = ?", [$id]);

        return ['Country Deleted Successfully', 200];
    }
}
