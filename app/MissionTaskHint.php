<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class MissionTaskHint extends Model
{
    public $timestamps = false;

    static public function new($task)
    {
        $id = Auth::user()->id;
        $hint = Self::where([['user_id', $id], ['mission_task_id', $task]])->first();
        if(!$hint)
        {
            $hint = new Self;
            $hint->user_id = $id;
            $hint->mission_task_id = $task;
            $hint->created_at = now();
            $hint->save();
        }
    }
}
