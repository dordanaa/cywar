<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAllowedGame extends Model // User Unavailable Games!!!
{
    public $timestamps = false;

    protected $table = "user_unavailable_games";

    static public function new($request)
    {
        $user = User::find($request->id);
        if(in_array($user->role->role, ['admin', 'master'])) return ['Cannot Update The User Games Allowing Off Your Authorization',400];
        
        if(User::checkAuthorization($user))
        {
            Self::where('user_id', $request->id)->delete();

            $data = [];
            foreach($request->games AS $game)
            {
                if(isset($game['id']) && is_numeric($game['id']))
                {
                    $data[] = ['user_id' => $request->id, 'game_id' => $game['id']];
                }
            }
            Self::insert($data);
            return ['User Games Allowing Updated Successfully',200];
        }else{
            return ['Cannot Update The User Games Allowing Off Your Authorization',400];
        }
    }
}
