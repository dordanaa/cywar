<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameInput extends Model
{
    static public function newGameInputs($id, $request)
    {
        $data = [];
        if(!is_array($request->inputs)) return;
        foreach($request->inputs AS $i){
            $input = explode(',', $i);
            $data[] = [
                'game_id' => $id,
                'name' => $input[0],
                'value' => $input[1],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        Self::insert($data);
    }
}
