<?php

namespace App;

use App\Lesson;
use App\ClassAvailableLesson;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $guarded = [];

    public function lessons(){
        return $this->hasMany(Lesson::class, 'module_id', 'id')
                    ->where('status', 1)
                    ->with('tasks_count', 'tasks_finished_count', 'available')
                    ->select('id', 'module_id', 'name', 'docker', 'play_time', 'objectives');
    }

    public function lessons_finished(){
        return $this->hasMany(Lesson::class, 'module_id', 'id')
                    ->where('lessons.status', 1)
                    ->join('lesson_attempts AS la', 'la.lesson_id', 'lessons.id')
                    ->where('la.user_id', Auth::user()->id)
                    ->where('la.status', 1)
                    ->select('lessons.module_id', 'lessons.id', 'la.status');
    }
    
    public function tasks_finished(){
        return $this->hasMany(Lesson::class, 'module_id', 'id')
                    ->where('lessons.status', 1)
                    ->join('missions AS m', 'm.lesson_id', 'lessons.id')
                    ->join('mission_tasks AS mt', 'mt.mission_id', 'm.id')
                    ->join('mission_task_attempts AS mta', 'mta.mission_task_id', 'mt.id')
                    ->where('mta.user_id', Auth::user()->id)
                    ->where('mta.status', 1)
                    ->select('lessons.module_id', 'lessons.id');
    }

    public function tasks(){
        return $this->hasMany(Lesson::class, 'module_id', 'id')
                    ->where('lessons.status', 1)
                    ->join('missions AS m', 'm.lesson_id', 'lessons.id')
                    ->join('mission_tasks AS mt', 'mt.mission_id', 'm.id')
                    ->select('lessons.module_id', 'lessons.id');
    }

    static public function getAllCMS($pagination, $request, &$data)
    {
        $data = Self::withCount('lessons');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->paginate($pagination);
    }
    
    static public function getAll(&$data)
    {
        $modules = Self::where('status', 1)->with('lessons', 'lessons_finished', 'tasks_finished', 'tasks')->get();
        if(Auth::user()->role->role == 'student')
        {
            $user = User::where('id', Auth::user()->id)->with('class')->first();
            $user_allowed = UserAvailableLesson::where('user_id', $user->id)->get();
            if($user->class) $allowed = ClassAvailableLesson::where('class_id', $user->class->id)->get();

            foreach($modules AS $mod_key => $module)
            {
                foreach($module->lessons AS $less_key => $lesson)
                {
                    $lesson->valid = false;
                    $arr = [];

                    if($user_allowed)
                    {
                        foreach($user_allowed AS $allow)
                        {
                            $arr[] = $allow->lesson_id;
                        }
                    }

                    if($user->class)
                    {
                        foreach($allowed AS $allow)
                        {
                            if(!in_array($allow->lesson_id, $arr)) $arr[] = $allow->lesson_id;
                        }
                    }

                    if(in_array($lesson->id, $arr))
                    {
                        $lesson->valid = true;
                    }
                }
            }
        }
        $data = $modules;
    }
    
    static public function createNew($request)
    {
        $image_name = null;
        if( $request->hasFile('file') && $request->file('file')->isValid() )
        {
            $file = $request->file('file');
            
            if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return ['Module Created Succesfully', 201];

            $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
            $request->file('file')->move(public_path() . '/images/modules/', $image_name);
            // $img = Image::make(public_path() . '/images/modules/' . $image_name);
            // $img->resize(1250, 700, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
        }
        $module = new Self;
        $module->name = $request->name;
        in_array($request->type, ['Red', 'Blue']) ? $module->type = $request->type : $module->type = 'Red';
        $module->image = $image_name ? $image_name : 'construction.jpg';
        $module->status = $request->status ? 1 : 0;
        $module->created_at = now();
        $module->created_by = Auth::user()->id;
        $module->save();
        return ['Module Created Succesfully', 201];
    }
    
    static public function updateMod($request)
    {
        $image_name = null;
        if( $request->hasFile('file') && $request->file('file')->isValid() )
        {
            $file = $request->file('file');
            
            if(!preg_match('/^.*\.(jpe?g|gif|webp|bmp|png)$/i', $file->getClientOriginalName())) return ['Module Updated Succesfully', 201];

            $image_name = date('Y.m.d.H.i.s') . '-' . $file->getClientOriginalName();
            $request->file('file')->move(public_path() . '/images/modules/', $image_name);
            // $img = Image::make(public_path() . '/images/modules/' . $image_name);
            // $img->resize(700, 700, function ($constraint) {
            //     $constraint->aspectRatio();
            // });
            // $img->save();
        }
        $module = Self::find($request->id);
        if(!$module) return ['Module Not Found', 400];
        
        $module->name = $request->name;
        if(in_array($request->type, ['Red', 'Blue'])) $module->type = $request->type;
        $module->image = $image_name ? $image_name : 'construction.jpg';
        $module->created_at = now();
        $module->created_by = Auth::user()->id;
        $module->save();
        return ['Module Updated Succesfully', 201];
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['Module Cannot Be Deleted While Having Lessons', 400]; 
            }
            return ['Modules have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Modules status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }
    
    static public function remove($id)
    {
        // If a module has lessons, cannot be deleted
        if( count(Lesson::where('module_id', $id)->get()) ) return false;
        Self::find($id)->delete();
        return true;
    }

}
