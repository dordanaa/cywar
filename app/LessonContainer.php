<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class LessonContainer extends Model
{
    public $timestamps = false;
    
    protected $hidden = [
        'container_id',
    ]; 

    public function lesson(){
        return $this->hasOne(Lesson::class, 'id', 'lesson_id');
    }

    static public function newContainer($data, $id)
    {
        $con = new Self;
        $con->user_id = Auth::user()->id;
        $con->container_id = isset($data->dockerID) ? $data->dockerID : NULL;
        $con->ip = $data->ip;
        $con->port = $data->port;
        $con->lesson_id = $id;
        $con->created_at = now();
        $con->save();
    }

    static public function getLessonContainers($id, &$data)
    {
        $date = new \DateTime();
        $date->modify('-3 hours');
        $formatted_date = $date->format('Y-m-d H:i:s');
        $data = Self::where('lesson_id', $id)
                    ->where('lesson_containers.created_at', '>', $formatted_date)
                    ->join('users AS u', 'u.id', 'lesson_containers.user_id')
                    ->join('user_roles AS ur', 'ur.user_id', 'lesson_containers.user_id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->get(['lesson_containers.*', 'u.name', 'r.role']);
    }
}
