<?php

namespace App;

use App\Classes;
use App\PaypalPlan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $hidden = [
        'plan_id'
    ];

    public function classes(){
        return $this->hasMany(Classes::class, 'subscription_id', 'id')->select('subscription_id', 'name', 'id');
    }
    
    public function creator(){
        return $this->hasOne(User::class, 'id', 'created_by')->select('id', 'name');
    }

    static public function getAll(&$plans)
    {
        $plans = Self::with('creator')->get();
    }

    static public function getAllActive(&$plans)
    {
        $plans = Self::where('status', 1)->get();
    }

    static public function createPlan($plan, $id)
    {
        $new = new Self;
        $new->plan_id = $id;
        $new->name = $plan->name;
        $new->description = $plan->description;
        $new->price = $plan->price;
        $new->duration = $plan->duration;
        $new->status = 2;
        $new->created_by = Auth::user()->id;
        $new->save();
    }

    static public function updatePlan($plan)
    {
        $new = Self::find($plan->id);
        $new->name = $plan->name;
        $new->description = $plan->description;
        $new->price = $plan->price;
        $new->duration = $plan->duration;
        $new->created_by = Auth::user()->id;
        $new->save();

        return ['Plan Updated Successfully', 200];
    }
    
    static public function updateStatus($request)
    {
        $plan = Self::find($request->id);
        $plan->status = $request->status;
        $plan->updated_at = now();
        $plan->updated_by = Auth::user()->id;
        $plan->save();
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                $res = self::remove($id);
                if(!$res) return ['Category Cannot Be Deleted While Having Games', 400]; 
            }
            return ['Categories have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Categories status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function remove($id)
    {
        // If a Subscription is being used, cannot be deleted
        if( count(Classes::where('subscription_id', $id)->where('status', 1)->get()) ) return ['Cannot delete plan while being used', 403];

        $sub = Self::find($id);
        if($sub) $sub->delete();

        PaypalPlan::deletePlan($sub->plan_id);

        return ['Plan deleted successfully', 200];
    }
}
