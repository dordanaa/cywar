<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class UserClassesLog extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];
    public $timestamps = false;
    protected $guarded = [];

    static public function new($user_id, $class_id)
    {
        $user = Auth::user();
        $log = new Self;
        $log->user_id = $user_id;
        $log->class_id = $class_id;
        $log->created_at = now();
        $log->created_by = $user ? $user->id : NULL;
        $log->save();
    }

    static public function getClassesHistory($id, &$data)
    {
        $data = Self::where('user_classes_logs.user_id', $id)
                    ->join('classes AS c', 'c.id', 'user_classes_logs.class_id')
                    ->join('colleges AS cl', 'cl.id', 'c.college_id')
                    ->join('countries AS cn', 'cn.id', 'cl.country_id')
                    ->select('user_classes_logs.*', 'c.name AS class', 'cl.name AS college', 'cn.country')
                    ->get();
    }
}
