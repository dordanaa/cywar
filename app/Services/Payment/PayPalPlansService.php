<?php

namespace App\Services\Payment;

use PayPal\Api\Plan;
use App\Subscription;
use PayPal\Api\Patch;
use PayPal\Api\Currency;
use PayPal\Rest\ApiContext;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Auth\OAuthTokenCredential;

class PaypalPlanService
{
    static private $plan;
    static private $patch;
    static private $status;
    static private $paymentDefinition;
    static private $merchantPreferences;
    static private $apiContext;

    public function __construct()
    {
        self::$apiContext = new ApiContext(
            new OAuthTokenCredential(
                'ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V', // Client ID
                'ELqyHPegFdgPXnIawK8oKrK0MjzcobLYUWPfcKYDjSj_pBfXayWRaUmCdVmIIQelf0waFJ9YtepsI4a9' // Client Sercret
            )
        );
    }

    static public function showPlan($id)
    {
        return Plan::get($id, self::$apiContext);
    }
    
    static public function getPlans()
    {
        $params = array('page_size' => '5');
        return Plan::all($params, self::$apiContext);
    }

    static public function createPlan($id)
    {
        self::setPlanHeader();
        self::setPaymentDefinition();
        self::setMerchantPreferences();

        self::$plan->setPaymentDefinitions(array(self::$paymentDefinition));
        self::$plan->setMerchantPreferences(self::$merchantPreferences);

        return self::sendNewPlan();
    }

    static public function updatePlan()
    {
        $sub = Subscription::find(request()->id);
        if($sub->status != 2) return true;

        self::$plan = self::showPlan(request()->plan_id);
        $data = self::updatePlanDetails(request());

        return self::sendUpdatedPlan($data);
    }
    
    static public function deletePlan($id)
    {
        self::$plan = self::showPlan($id);
        self::removePlan();
    }
    
    static public function updatePlanStatus($id, $status)
    {
        self::$plan = self::showPlan($id);
        $value = self::setStatus($status);

        try {
            $patch = new Patch();
            
            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
        
            self::$plan->update($patchRequest, self::$apiContext);
            return Plan::get(self::$plan->getId(), self::$apiContext);
            
        } catch (Exception $ex) {
            return false;
        }
    }

    static public function setPlanHeader()
    {
        self::$plan = new Plan();
        self::$plan->setName(request()->name)
                   ->setDescription(request()->description)
                   ->setType('fixed');
    }
    
    protected static function setPaymentDefinition()
    {
        self::$paymentDefinition = new PaymentDefinition();
        self::$paymentDefinition->setName('Regular Payments')
                                ->setType('REGULAR')
                                ->setFrequency('Month')
                                ->setFrequencyInterval("1")
                                ->setCycles(request()->duration)
                                ->setAmount(new Currency(array('value' => request()->price, 'currency' => 'USD')));
    }
    
    protected static function setMerchantPreferences()
    {
        self::$merchantPreferences = new MerchantPreferences();
        self::$merchantPreferences->setReturnUrl("https://cywar.hackeru.com/agreement?status=true")
                                  ->setCancelUrl("https://cywar.hackeru.com/agreement?status=false")
                                  ->setAutoBillAmount("yes")
                                  ->setInitialFailAmountAction("CONTINUE")
                                  ->setMaxFailAttempts("0")
                                  ->setSetupFee(new Currency(array('value' => 0, 'currency' => 'USD')));
        return self::$merchantPreferences;
    }

    static public function sendNewPlan()
    {
        try {
            return self::$plan->create(self::$apiContext);
        } catch (Exception $ex) {
            return false;
        }
    }

    static public function updatePlanDetails($request)
    {
        self::$patch = new Patch();
        return [
            "name" => "$request->name",
            "frequency" => "Month",
            "amount" => [
                "currency" => "USD",
                "value" => "$request->price"
            ]
        ];
    }

    static public function sendUpdatedPlan($data)
    {
        try {
            $paymentDefinitions = self::$plan->getPaymentDefinitions();
            $paymentDefinitionId = $paymentDefinitions[0]->getId();
            self::$patch->setOp('replace')
                ->setPath('/payment-definitions/' . $paymentDefinitionId)
                ->setValue($data);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch(self::$patch);
        
            self::$plan->update($patchRequest, self::apiContext());
            $plan = Plan::get(self::$plan->getId(), self::apiContext());
        } catch (Exception $ex) {
            return false;
        }
            return $plan;
    }

    static public function setStatus()
    {
        if(self::$status == 1)
        {
            return new PayPalModel('{
                "state":"ACTIVE"
            }');
        }
        else if(self::$status == 0)
        {
            return new PayPalModel('{
                "state":"INACTIVE"
            }');
        }
    }

    static public function removePlan()
    {
        try {
            self::$plan->delete(self::$apiContext);
            return true;
        }catch(Exception $ex){
            return false;
        }
    }
}