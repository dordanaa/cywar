<?php

namespace App\Services\Payment;

class PaypalService
{
    static public function createAgreement($id)
    {
        $createdPlan = Self::showPlan($id);
        $agreement = self::agreementDetails();
        $plan = new Plan();
        $plan->setId($createdPlan->getId());
        $agreement->setPlan($plan);
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);
        
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setLine1('111 First Street')
                        ->setCity('Saratoga')
                        ->setState('CA')
                        ->setPostalCode('95070')
                        ->setCountryCode('US');
        $agreement->setShippingAddress($shippingAddress);
        
        try {
            $agreement = $agreement->create(self::apiContext());
            $approvalUrl = $agreement->getApprovalLink();

        }catch (Exception $ex){
            return false;
        }
        
        $user = Auth::user();
        $user->subscription_id = $agreement->plan->id;
        $user->save();

        return $approvalUrl;
    }
}