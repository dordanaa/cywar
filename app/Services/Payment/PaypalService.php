<?php

namespace App\Services\Payment;

use App\Subscription;
use PayPal\Api\Plan;
use PayPal\Api\Payer;
use PayPal\Api\Agreement;
use PayPal\Rest\ApiContext;
use PayPal\Api\ShippingAddress;
use Illuminate\Support\Facades\Auth;
use PayPal\Auth\OAuthTokenCredential;

class PaypalService
{
    protected static function apiContext()
    {
        return new ApiContext(
            new OAuthTokenCredential(
                'ATc89fyIJAo5qCGYgB0YDsVHoGi3AQZ_JOa5y9phcfd_pkUhRXFWT1LjV6v-wATv9nxzgBlUhrq7DG7V', // Client ID
                'ELqyHPegFdgPXnIawK8oKrK0MjzcobLYUWPfcKYDjSj_pBfXayWRaUmCdVmIIQelf0waFJ9YtepsI4a9' // Client Sercret
            )
        );
    } 

    static public function createAgreement($products)
    {
        $plan_id = self::getPlan($products);
        $createdPlan = Plan::get($plan_id, self::apiContext());
        $agreement = self::agreementDetails();
        $plan = new Plan();
        $plan->setId($createdPlan->getId());
        $agreement->setPlan($plan);
        
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);
        
        $shippingAddress = new ShippingAddress();
        $shippingAddress->setLine1('111 First Street')
                        ->setCity('Saratoga')
                        ->setState('CA')
                        ->setPostalCode('95070')
                        ->setCountryCode('US');
        $agreement->setShippingAddress($shippingAddress);
        
        try {
            
            $agreement = $agreement->create(self::apiContext());
            $approvalUrl = $agreement->getApprovalLink();

        }catch (Exception $ex){
            return false;
        }
        
        $user = Auth::user();
        $user->subscription_id = $agreement->plan->id;
        $user->save();

        return $approvalUrl;
    }

    static public function getPlan($products)
    {
        return Subscription::find($products[0]['product_id'])->plan_id;
    }
    
    static protected function agreementDetails()
    {
        $agreement = new Agreement();
        $agreement->setName('Base Agreement2')
                ->setDescription('Basic Agreement2')
                ->setStartDate(now()->addMinute()->format('c'));
        return $agreement;
    }
}