<?php

namespace App\Services\Payment;

use App\Services\Payment\PaypalService;
use App\Services\Payment\TranzilaService;

class OrderService
{
    static public $products;
    static public $service;
    static public $agreementToken;
    static public $response;

    static public function create()
    {
        self::$products = request()->products;
        self::$service = request()->service;

        self::setProducts();
        self::serviceRouter();

        return self::$response;
    }

    static public function setProducts()
    {
        // get the products details by the id's received from the browser
    }

    static public function serviceRouter()
    {
        if( self::$service == 'PayPal' ) self::$response = PaypalService::createAgreement(self::$products);
        if( self::$service == 'Tranzila' ) self::$response = TranzilaService::createAgreement(self::$products);
    }
}