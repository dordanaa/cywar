<?php

namespace App\Services\Logger;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class LoggerService
{
    static public $content = '';

    /**
     * creates a new log
     * @param string $type
     * @param string $content
     * @param string $channel
     * @param string $token
     */
    static public function init($type, $content, $channel = 'custom', $token = null)
    {
        self::setContent($type, $content);
        self::setBrowserDetails();
        self::setUser();
        self::setIncomingPath();
        self::createLog($type, $channel, $token);
    }

    static private function setContent($type, $content)
    {
        self::$content = "ACTION: $content, ";
    }

    static private function setBrowserDetails()
    {
        self::$content .= 'BROWSER: ' . request()->header('user-agent') . ', ';
        self::$content .= 'IP: ' . request()->ip() . ', ';
    }

    static private function setUser()
    {
        if($user = Auth::user()) self::$content .= 'USER: ' . $user->id . ', ';
    }

    static private function setIncomingPath()
    {
        self::$content .= 'URL: ' . request()->server('HTTP_REFERER');
    }

    static private function createLog($type, $channel, $token)
    {
        $log = Log::channel($channel);

        switch ($type) {
            case 'ERROR':
                $log->error(self::$content);
                break;
            case 'INFO':
                $log->info(self::$content . $token);
                break;
            case 'CRITICAL':
                $log->critical(self::$content);
                break;
            default:
                $log->error(self::$content);
                break;
        }
    }
}
