<?php

namespace App\Services\Pods;

use App\GameDetail;
use App\Lesson;
use App\Services\Logger\LoggerService;

class PodsService
{
    static private $secret_key = '5eC(T0)B3-4Sp3C!aLUR';

    static public function getAllContainers($token)
    {
        self::log('INFO', 'Attempting to get all containers from PodsService');
        
        if( self::$secret_key !== $token) 
        {
            self::log('ERROR', 'Failed getting all containers from PodsService');
            return response()->json('Unauthorized', 401);
        }
        
        $challenges = collect(self::getChallengesContainers());
        $lessons = collect(self::getLessonsContainers());
        $data = self::setData( $challenges->merge($lessons) );
        return response()->json($data);
    }
    
    static public function getChallengesContainers()
    {
        $data = GameDetail::whereNotNull('docker')->get(['docker']);
        return self::getOnlyDocker($data);
    }
    
    static public function getLessonsContainers()
    {
        $data = Lesson::whereNotNull('docker')->get(['docker']);
        return self::getOnlyDocker($data);
    }
    
    static public function getOnlyDocker($data)
    {
        $response = [];
        foreach($data AS $pod) $response[] = $pod->docker;
        return $response;
    }

    static public function setData($data)
    {
        return [
            'total' => count($data),
            'data' => $data
        ];
    }

    static public function log($info, $content)
    {
        LoggerService::init($info, $content);
    }
}

