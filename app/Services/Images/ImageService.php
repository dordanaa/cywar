<?php

namespace App\Services\Images;

class ImageService
{
    static public function remove($file)
    {
        $full_path = public_path($file);
        if(file_exists($full_path)) unlink($full_path);
    }
}