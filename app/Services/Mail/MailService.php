<?php

namespace App\Services\Mail;

use Exception;
use Illuminate\Support\Facades\Mail;
use App\Services\Logger\LoggerService;

class MailService
{
    static public $emails = [];
    static public $fails = [];

    static public function support($content)
    {
        if( !env('MAIL_SERVICE') ) return self::logger('INFO', 'MAIL SERVICE IS OFF. Attempted to send a support mail');

        Mail::raw($content, function ($message) {
            $message->to('gala@hackerupro.co.il')
              ->subject('Support Issue');
        });
    }

    static public function send($emails, $mail_format, $data)
    {
        if( !env('MAIL_SERVICE') ) return self::logger('INFO', 'MAIL SERVICE IS OFF. Attempted to send ' . $mail_format);

        self::setEmails($emails);
        self::logger('INFO', 'Attempting to send ' . $mail_format);

        try{
            $mail = 'App\\Mail\\' . $mail_format;
            Mail::to($emails)->send(new $mail($data));
            self::logger('INFO', 'Successfully sent ' . $mail_format);
        }
        catch(Exception $ex)
        {
            self::logger('FAILED', 'Sending ' . $mail_format  . ' Failed. Error content: ' . $ex->getMessage() . '.');
        }
    }

    static public function setEmails($emails)
    {
        if(is_array($emails)) self::$emails = $emails;
        else self::$emails[] = $emails; 
    }

    static public function logger($type, $content)
    {
        $mailto = ' mail to: ' . json_encode(self::$emails);
        LoggerService::init($type, $content . $mailto , 'mail');
    }
}

