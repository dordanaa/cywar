<?php

namespace App\Services\MondayAPI;

use Exception;
use App\Services\Logger\LoggerService;

class CreateMondayTicket
{
    static private $secret_key = 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjM3MTIyMzUxLCJ1aWQiOjExNjI5NjY0LCJpYWQiOiIyMDIwLTAyLTA5IDEzOjAyOjQwIFVUQyIsInBlciI6Im1lOndyaXRlIn0.7iIPQVNWyap6HEAht-KdJwt_f0qgzuPKJiAlsgX2O80';
    static private $board = 413061844;
    static private $group;
    static private $response;

    public function __construct($inputs)
    {
        self::setGroup($inputs['category']);
        self::sendRequest();
        dd(self::$response);
    }    

    static private function setGroup($category)
    {
        switch ($category) {
            case 6:
                self::$group = 'new_group24893';
                break;
            case 7:
                self::$group = 'new_group67324';
                break;
            default:
                self::$group = 'CyWar - Infrastructure';
                break;
        }
    }

    static private function sendRequest()
    {
        try{
            $ip = 'https://api.monday.com/v2';
            // Create Item
            // $body = "
            //     mutation ($name: String!, $board: Int!, $group: String!, $columns: JSON!) {
            //         create_item (board_id: $board, group_id: $group, item_name: $name, column_values: $columns) {
            //             id
            //         }
            //     }
            // ";
            $body = '{"mutation":"{create_item(board_id:' . self::$board . ', group_id: '. self::$group . ', item_name: "neitem"){id}"}';
            // $body = 'mutation {create_item (board_id: 413061844, group_id: new_group24893, item_name: "new item") { id }}';
            // Get Groups
            // $body = '{"query":"{boards(ids:' . self::$board . '){groups{id title}}}"}';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $ip);
            curl_setopt($curl, CURLOPT_TIMEOUT, 60);
            // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
            curl_setopt($curl, CURLOPT_HTTPHEADER, 
                [
                    'Authorization:' . self::$secret_key,
                    'Content-Type: application/json'
                ]                                                                         
            ); 
            $result = curl_exec($curl);
            if (curl_errno($curl)) {
                $error_msg = curl_error($curl);
                if($error_msg) dd($error_msg);
            }
            self::$response = json_decode($result);
            curl_close($curl);
        }catch(Exception $ex){
            dd($ex);
            LoggerService::init('ERROR', 'Failed to create a support ticket in monday');
        }
    }
};
