<?php

namespace App\Services\Auth;

use App\Token;
use App\UserLog;
use Illuminate\Support\Facades\Auth;
use App\Services\Logger\LoggerService;
use App\Services\Auth\Google2FAService;
use App\Services\Auth\DecryptionService;

class LoginService extends DecryptionService
{
    static private $user;
    static private $email;
    static private $password;
    static private $mfa_key;
    static private $time;
    static private $token;
    static private $api_token;
    static private $type;
    static private $redirectURL;
    static private $data;
    static private $attempt;

    /**
     * Set dynamic variables
     * 
     * @param int $time
     * @param string $type
    */
    public function __construct($time = 180, $type)
    {
        self::$time = $time;
        self::$type = $type;
        self::$api_token = request()->token;
        self::$email = request()->email;
        self::$password = request()->password;
        self::$mfa_key = request()->key;
        self::$attempt = request()->attempt;
    }

    /**
     * Initial the login proccess
     * Control all the functions
     * 
     * @return array
    */
    static public function init($user = null)
    {
        if($user)
        {
            self::createToken();
            self::setDataProps();
            self::createLog('INFO', 'User: ' . $user->email . ' reseted token');
            return [self::$data, 200];
        }

        // VERIFYING USER
        $error = self::loginRouter();
        if(self::$user) $error = self::checkStatus();

        if($error) 
        {
            $content = $error[0] . ', User Request: ' . self::$email . ', Attempt: ' . self::$attempt;
            self::createLog('ERROR', $content);
            return $error;
        }

        // USER LOGGED SUCCESSFULLY

        self::createToken();
        self::setDataProps();

        /**
         * TODO: Remove Token after debugging : issue where users logged as alex
         */
        $content = 'User Request: ' . self::$email . ', User Recieved: ' . self::$user['email'];
        self::createLog('INFO', $content);
        UserLog::new(request(), self::$user);

        return [self::$data, 200];
    }

    /**
     * Routing which login attempt is required 
    */
    static private function loginRouter()
    {
        if(self::$api_token) $error = self::attemptAPILogin();
        else $error = self::attemptLogin();

        return $error;
    }

    /**
     * Attempting to login the user from inside Cywar 
    */
    static private function attemptLogin()
    {
        $attempt = Auth::attempt(['email' => self::$email, 'password' => self::$password]);
        if($attempt)
        {
            self::$user = Auth::user();
            if( $error = Google2FAService::authenticate(self::$mfa_key, self::$user) ) return $error;
        }
        else return ['Email or Password is incorrect', 401];
    }
    
    /**
     * Attempting to login the user with API 
    */
    static private function attemptAPILogin()
    {
        $output = self::decrypt(self::$api_token, 'd');

        if(self::$type == 'campus') self::$user = CampusService::extractUser($output);
        if(self::$type == 'jc') {
            if( $res = JustCodeService::extractUser($output) )
            {
                self::$user = $res['user'];
                self::$redirectURL = $res['redirectURL'];
            }
        };
        self::$email = self::$user['email'];
        if( !self::$user ) return ['Unabled to login', 400];
    }
    
    
    /**
     * Checking if the user is valid by his status
    */
    static private function checkStatus()
    {
        if(self::$user->status == 2) return ['Waiting for approvement', 401];
        if(self::$user->status == 0) return ['User is unactive', 401];
    } 
    
    /**
     * Creating new token & deleting old one
    */
    static private function createToken()
    {
        // remove old token if exists
        // only 1 token per user
        Token::where('user_id', self::$user->id)->delete();
        self::$token = self::$user->createToken('Cywar')->accessToken;
    } 
    
    /**
     * Sets all the information that is needed for the front
    */
    static private function setDataProps()
    {
        self::$data = [
            'token' => self::$token,
            'name' => self::$user->name,
            'role' => self::$user->role->role,
            'time' => self::$time,
            'url' => self::$redirectURL
        ];
    } 
    
    /**
     * Create a log file for security
    */
    static private function createLog($type, $content)
    {
        LoggerService::init($type, $content, 'security', ', Token: ' . self::$data['token']);
    } 

}