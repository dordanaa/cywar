<?php

namespace App\Services\Auth;

class DecryptionService
{
    static public $key = 'ABy56e$RlL#';
    static public $secret_key = 'sag8_ckm#h3!apr_secret_key';
    static public $secret_iv = 'sag8_ckm#h3!apr_secret_iv';

    /**
     * Decrypt the api_token
     * 
     * @return user_details   
     */
    static public function decrypt($token, $action = 'e')
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', self::$secret_key);
        $iv = substr(hash('sha256', self::$secret_iv), 0, 16);
        if ($action == 'e') {
            $output = base64_encode(openssl_encrypt($token, $encrypt_method, $key, 0, $iv));
        } else if ($action == 'd') {
            $output = openssl_decrypt(base64_decode($token), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
}