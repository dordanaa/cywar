<?php

namespace App\Services\Auth;

use App\UserLog;
use Illuminate\Support\Carbon;

class Google2FAService
{
    /**
     * Verifying the google key
     * 
     * @param string $key
     * @param collection $user
     */
    static public function verifyKey($key, $user)
    {
        $google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA());
        $isValid = $google2fa->verifyKey($user->google2fa_secret, $key, 0);
        return $isValid ? true : false;
    }

    /**
     * Check & verifies the mfa if needed
     * 
     * @param string $mfa_key
     * @param collection $user
     * 
     * @return $error if exists
     */
    static public function authenticate($mfa_key, $user)
    {
        if($error = self::checkIfUserRequiresMFA($mfa_key, $user)) return $error;
        if($error = self::verifyMFA($mfa_key, $user)) return $error;
    }

    /**
     * Check if the user requires MFA
     * If so, will pop up the dialog for the key  
     */
    static private function checkIfUserRequiresMFA($mfa_key, $user)
    {
        // if user already filled the key does not require validation
        if( $mfa_key ) return;

        // if the user logged in the last 12 hours, the user does not need to signin with the 2fa again
        if( UserLog::where([['user_id', $user->id], ['created_at', '>', Carbon::now()->subMinutes(720)->toDateTimeString()]])->first() ) return;

        // check by the user
        if($user->active_2fa) return ['Activate 2FA', 200];

        // only if the user is a student to check by the class
        if($user->role->role != 'student') return false;

        // check by the user classes
        $user->load('classes');
        if($user->classes)
        {
            foreach($user->classes AS $class) if($class->active_2fa) return ['Activate 2FA', 200];
        }
    }

    /**
     * Only if the user needs MFA
     * Verifies the key
     */
    static private function verifyMFA($mfa_key, $user)
    {
        // if did not need MFA
        if( !$mfa_key ) return;

        if( !Google2FAService::verifyKey($mfa_key, $user) ) return ['2 FA code is invalid', 422];
    }
}