<?php

namespace App\Services\Auth;

use App\User;
use Illuminate\Support\Carbon;
use App\Http\Controllers\GamesController;

class JustCodeService
{
    static private $token = '0Q8k3wKBJR6dTcqtq5Bp6f5sSNKg0FFG';

    /**
     * Extract the JC user details
     * 
     * @param string $data
     * @return collection
     */
    static public function extractUser($data)
    {
        list($email, $redirectURL, $createdTime, $durationMin ,$requestArr) = explode('|Galex|',$data);
        $durationHours = ($durationMin ?? 1440) / 60;

        if( self::checkCreationTime($createdTime, $durationHours) ) return;

        self::createJCookie($redirectURL, $requestArr, $durationHours);

        return [
            'user' => User::where('email', $email)->first(),
            'redirectURL' => $redirectURL
        ];
    }

    /**
     * Checks if candidate created less than 24 hours
     */
    static private function checkCreationTime($createdTime, $durationHours)
    {
        $now = Carbon::now();
        $candidateCreated = Carbon::parse($createdTime)->addHours($durationHours); 
        if($now->greaterThan($candidateCreated)) return 'ERROR';
    }

    /**
     * Create a cookie for JC candidate
     * The cookie stores the challenge info, 1 hour
     */
    static private function createJCookie($redirectURL, $requestArr, $durationHours)
    {
        $gameData = GamesController::GetGameStructure(self::$token, 1, 1000, true);
        if(!isset($gameData[$redirectURL])) {
            return response()->json(['error' => 'Game does not exists'],404);
        }

        setcookie('jc', $requestArr, time() + 60 * 60 * $durationHours, '/api/games/'.$gameData[$redirectURL]);
    }
}