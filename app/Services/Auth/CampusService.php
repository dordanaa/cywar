<?php

namespace App\Services\Auth;

use App\User;

class CampusService
{
    static private $divider = '-HackampusToCywar-';

    /**
     * Extract the JC user details
     * 
     * @param string $data
     * @return collection
     */
    static public function extractUser($data)
    {
        $parts = explode(self::$divider, $data);
        if (isset($parts[0]) && isset($parts[1])) 
        {
            if (filter_var($parts[0], FILTER_VALIDATE_EMAIL) && is_numeric($parts[1])) 
            {
                $diff = time() - $parts[1];
                if (true) 
                // if ($diff < 60) 
                {
                    return User::where('email', $parts[0])->where('status', 1)->first();
                }
            }
        }
    }
}