<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttemptLog extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

    public $timestamps = false;

    static public function new($game, $user, $points, $status)
    {
        $attempt = new Self;
        $attempt->game_id = $game;
        $attempt->user_id = $user;
        $attempt->points = $points;
        $attempt->status = $status;
        $attempt->created_at = now();
        $attempt->save();
    }
}
