<?php

namespace App;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    static public function fullSearch($cat, $search, &$data)
    {
        $valids = ['games', 'categories', 'students', 'users', 'modules', 'lessons'];
        if(!in_array($cat, $valids)) return;

        if(in_array($cat, ['students', 'users']))
        {
            $result = User::join('user_roles AS ur', 'users.id', 'ur.user_id');
            if($cat == 'students')
            {
                $result = $result->where('ur.role_id', 33);
            }
            else
            {
                $result = $result->where('ur.role_id', '!=', 33);
            }
            $result = $result->where(function($q) use($search) {
                           $q->Where('name', 'LIKE', "%$search%")
                             ->orWhere('email', 'LIKE', "%$search%")
                             ->orWhere('phone', 'LIKE', "%$search%");
                           })->limit(10)->get(['users.id', 'users.name']);
        }
        else
        {
            $result = DB::table($cat)->where('name', 'LIKE', "%$search%")->limit(10)->get(['id', 'name']);
        }

        $data = $result;
    }
}
