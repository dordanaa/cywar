<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class MissionTask extends Model
{
    public function userCompleted(){
        return $this->hasOne(MissionTaskAttempt::class, 'mission_task_id', 'id')->where('mission_task_attempts.user_id', Auth::user()->id)->select('mission_task_id', 'status');
    }

    static public function new($mission, $new, $id)
    {
        $data = [];
        foreach($mission['cms_tasks'] AS $task){
            $data[] = [
                'mission_id' => $new->id,
                'content' => $task['content'],
                'solution' => $task['solution'],
                'explanation' => isset($task['explanation']) ? $task['explanation'] : null,
                'status' => $task['status'],
                'created_by' => $id,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }
        Self::insert($data);
    }

    static public function updateTask($tasks, $id, $mid)
    {
        if(isset($tasks['cms_tasks_del']))
        {
            foreach($tasks['cms_tasks_del'] AS $item){
                if(isset($item['id'])) Self::where('id', $item['id'])->delete();
            }
        }

        foreach($tasks['cms_tasks'] AS $item){
            if(isset($item['id']))
            {
                $task = Self::find($item['id']);
                $task->content = $item['content'];
                $task->explanation = isset($item['explanation']) ? $item['explanation'] : null;
                $task->status = $item['status'];
                $task->solution = $item['solution'];
                $task->updated_at = now();
                $task->updated_by = $id;
                $task->save();
            }
            else
            {
                $task = new Self;
                $task->mission_id = $mid;
                $task->content = $item['content'];
                $task->explanation = isset($item['explanation']) ? $item['explanation'] : null;
                $task->status = $item['status'];
                $task->solution = $item['solution'];
                $task->created_at = now();
                $task->created_by = $id;
                $task->save();
            }
        }
    }

    static public function getSolution($id)
    {
        $task = Self::where('id', $id)->first(['solution']);
        return $task->solution;
    }
}
