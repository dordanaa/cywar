<?php

namespace App;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class TotalScore extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];
    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    static public function HOF(&$data, $limit) // ONLY FOR STUDENTS
    {
        if(! in_array($limit, [10, 50, 100, 'class'])) $limit = 10;
        
        $user = Auth::user();

        // $users = Self::join('users AS u', 'u.id', 'total_scores.user_id')
        //                  ->join('user_roles AS ur', 'ur.user_id', 'u.id')
        //                  ->join('roles AS r', 'r.id', 'ur.role_id')
        //                  ->leftJoin('user_classes AS ua', 'ua.user_id', 'u.id')
        //                  ->leftJoin('classes AS c', 'c.id', 'ua.class_id')
        //                  ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
        //                  ->leftJoin('countries AS cr', 'cr.id', 'cg.country_id')
        //                  ->where('r.role', 'student');
        $users = User::join('user_roles AS ur', 'ur.user_id', 'users.id')
                    ->join('roles AS r', 'r.id', 'ur.role_id')
                    ->leftJoin('user_classes AS ua', 'ua.user_id', 'users.id')
                    ->leftJoin('classes AS c', 'c.id', 'ua.class_id')
                    ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                    ->leftJoin('countries AS cr', 'cr.id', 'cg.country_id')
                    ->leftJoin('total_scores', 'total_scores.user_id', 'users.id')
                    ->where('r.role', 'student');

        if($limit == 'class')
        {
            return $data = $users->where('c.id', $user->class->id)
                                 ->select('total_scores.total_points', 'total_scores.plays', 'total_scores.games', 'users.name', 'users.image', 'total_scores.user_id', 'cr.country')
                                 ->get();
        }
        else
        {
            
        $users = $users->orderBy('total_points', 'desc')
                       ->select('total_scores.total_points', 'total_scores.plays', 'total_scores.games', 'users.name', 'users.id', 'users.image', 'total_scores.user_id', 'cr.country')
                       ->limit($limit)->get();
    
            if($user->role->role != 'student') return $data = $users; // If user is not a student, what comes after is not relavent
    
            // Checks if student in TOP 10
            $check = false; 
            foreach($users AS $item)
            {
                if($item->user_id == $user->id){
                    $check = true;
                    break;
                }
            }
    
            if(!$check) // If student is not in TOP 10
            {
                $rank = 0;
                $all = Self::get(['user_id','total_points']);
                foreach($all AS $i => $user){
                    if($user->user_id == $user->id){
                        $rank = $i + 1; // Student Rank ( $i starts with 0 )
                        break;
                    }
                }

                $ranked = User::where('users.id', Auth::user()->id)
                            ->leftJoin('user_classes AS ua', 'ua.user_id', 'users.id')
                            ->leftJoin('classes AS c', 'c.id', 'ua.class_id')
                            ->leftJoin('colleges AS cg', 'cg.id', 'c.college_id')
                            ->leftJoin('countries AS cr', 'cr.id', 'cg.country_id')
                            ->join('total_scores AS ts', 'ts.user_id', 'users.id')
                            ->select('ts.total_points', 'ts.plays', 'ts.games', 'users.name', 'users.image', 'ts.user_id', 'cr.country')
                            ->first();

                $ranked['rank'] = $rank;
                $users[] = $ranked;
                
            }

        }
        
        $data = $users;
    }

    static public function new($uid, $points, $game)
    {
        $validate = Self::where('user_id', $uid)
                        ->increment('total_points', $points,['plays' => DB::raw("plays + 1"), 'games' => DB::raw("games + $game")]);

        if(!$validate) // User first game TotalScore
        {
            $total = new Self;
            $total->user_id = $uid;
            $total->total_points =  $points;
            $total->plays = 1;
            $total->games = 1;
            $total->save();
        }
    }
}
