<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CyberpediaCategory extends Model
{
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    public function terms(){
        return $this->hasMany(CyberpediaTerm::class, 'category_id', 'id');
    }

    public function validTerms(){
        return $this->hasMany(CyberpediaTerm::class, 'category_id', 'id')->where('status', 1)->select('id', 'category_id', 'image', 'name', 'content', 'link');
    }

    static public function getAll($pagination, $request, &$data)
    {
        $data = Self::withCount('terms');

        if( isset($request->sortBy) && $request->sortBy && isset($request->sortOrder) && $request->sortOrder )
        {
            $order = $request->sortOrder == 'true' ? 'asc' : 'desc';
            $data = $data->orderBy($request->sortBy, $order);
        }

        $data = $data->paginate($pagination);
    }

    static public function createNew($request)
    {
        $image_name = ImageLoader::new($request, 'file', '/images/cyberpedia/cats/');
        $category = new Self;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->image = $image_name ? $image_name : 'default.jpg';
        $category->status = $request->status ? 1 : 0;
        $category->created_at = now();
        $category->created_by = Auth::user()->id;
        $category->save();
        return ['Category Created Succesfully', 201];
    }

    static public function updateCategory($request)
    {
        $image_name = ImageLoader::new($request, 'file', '/images/cyberpedia/cats/');
        $category = Self::find($request->id);
        $category->name = $request->name;
        $category->description = $request->description;
        if($image_name) $category->image = $image_name;
        $category->status = $request->status ? 1 : 0;
        $category->created_at = now();
        $category->created_by = Auth::user()->id;
        $category->save();
    }

    static public function checkNumber($mid, $number, $id)
    {
        if( $lesson = Self::where([['module_id', $mid],['number', $number],['id', '!=', $id]])->first() )
        {
            $lesson->number = $number + 1;
            $lesson->save();
            self::checkNumber($mid, $lesson->number, $lesson->id);
        }
    }
    
    static public function MultipleAction($request)
    {
        if($request->mode == 'Delete')
        {
            foreach($request->data AS $id)
            {
                self::remove($id);
            }
            return ['Lessons have been deleted successfully', 200];
        }else{
            $status = $request->mode == 'Activate' ? 1 : 0;
            Self::whereIn('id', $request->data)->update([
                'status' => $status,
                'updated_at' => now(),
                'updated_by' => Auth::user()->id
            ]);
            return ["Lessons status has been changed successfully to $request->mode", 200];
        }
    }
    
    static public function updateStatus($request)
    {
        $cat = Self::find($request->id);
        $cat->status = $request->status;
        $cat->updated_at = now();
        $cat->updated_by = Auth::user()->id;
        $cat->save();
    }

    static public function remove($id)
    {
        // If a category has terms, cannot be deleted
        if( count(CyberpediaTerm::where('category_id', $id)->get()) ) return false;

        DB::delete("DELETE c.*, ct.* FROM cyberpedia_categories AS c "
                    . "LEFT JOIN cyberpedia_terms AS ct ON ct.category_id = c.id "
                    . "WHERE c.id = ?", [$id]);

        return true;
    }
}
