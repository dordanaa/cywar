<?php

namespace App\Console\Commands;

use App\Classes;
use App\Mail\ClassExpired;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use App\Mail\ClassAboutToExpire;
use Illuminate\Support\Facades\Mail;

class ClassExp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'class_exp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Expirations of classes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $class = Classes::where('name', 'galex')->first();
        Mail::to('gala@hackerupro.co.il')->send(new ClassAboutToExpire($class));
        return;
        $emails = ['gala@hackerupro.co.il', 'alexb@hackerupro.co.il', 'ilanm@hackeru.co.il', 'roman@hackerupro.co.il'];

        $classesEndsInOneWeek = Classes::where('end_at', Carbon::now()->addDays(7)->format('Y-m-d'))->with('master')->get(['id', 'name']);
        foreach($classesEndsInOneWeek AS $class){
            $mails = $emails;
            $mails[] = $class->master->email;
            Mail::to($mails)->send(new ClassAboutToExpire($class));
        }
        
        $classesEnded = Classes::where('end_at', '>',  Carbon::today())->with('master')->get();
        if($classesEnded) Classes::where('end_at', '>',  Carbon::today())->update([
            'status' => 0,
            'notes' => 'Class has been expired',
            'updated_at' => now(),
            'updated_by' => '0'
        ]);
        foreach($classesEnded AS $class){
            $mails = $emails;
            $mails[] = $class->master->email;
            Mail::to($mails)->send(new ClassExpired($class));
        }
    }
}
