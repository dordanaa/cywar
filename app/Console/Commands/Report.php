<?php

namespace App\Console\Commands;

use App\Game;
use App\Attempt;
use App\Mail\MonthlyReport;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\CMS\DashboardController;

class Report extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monthly Cywar General Report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = ['gala@hackerupro.co.il', 'ilanm@hackeru.co.il', 'lion@hackerupro.co.il', 'alexb@hackerupro.co.il'];
        $data = [];
        $now = Carbon::now();
        $data['date'] = "$now->month/$now->year";
        $data['students'] = DashboardController::students();
        $data['cats'] = DashboardController::games();
        $data['games'] = Game::count();
        $data['games_added'] = Game::where('created_at', '>', Carbon::now()->startOfMonth())->count();
        $data['games_played'] = Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count();
        $data['games_finished'] = Attempt::join('users', 'users.id', 'attempts.user_id')->join('user_roles AS ur', 'ur.user_id', 'users.id')->join('roles AS r', 'r.id', 'ur.role_id')->where('r.role', 'student')->where('attempts.status', 1)->where('attempts.created_at', '>=', Carbon::now()->startOfMonth())->count();

        Mail::to($emails)->send(new MonthlyReport($data));
    }
}
