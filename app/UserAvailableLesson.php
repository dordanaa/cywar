<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserAvailableLesson extends Model
{
    static public function new($request)
    {
        $user = User::find($request->id);
        if(User::checkAuthorization($user))
        {
            Self::where('user_id', $user->id)->delete();

            $data = [];
            foreach($request->lessons AS $lesson)
            {
                if(isset($lesson['id']) && is_numeric($lesson['id']))
                {
                    $data[] = ['user_id' => $request->id, 'lesson_id' => $lesson['id']];
                }
            }
            Self::insert($data);
            return ['User Lessons Available Updated Successfully',200];
        }else{
            return ['Cannot Update, The User Is Off Your Authorization',400];
        }
    }
}
